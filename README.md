# Curveball Algorithm

This algorithm is used to generate uniformly at random graphs (nodes + links) with a fixed degree distribution

## Goal

The purpose of this project is to find the more efficient implementations of the curveball algorithm
depending on the type of hardware available (single vs multicore CPU, GPU, dedicated chips)


## Implementations

As defined by C. J. Carstens, A. Berger, and G. Strona in [Curveball: a new generation of sampling
algorithms for graphs with fixed degree sequence](https://arxiv.org/abs/1609.05137)

Different languages are used:

 - python https://git.cs.uni-kl.de/siebert/curveball/tree/master/code/fdsmCurveballPython
 - java  https://git.cs.uni-kl.de/siebert/curveball/tree/master/code/fdsmCurveballJava

# Wiki

Have a look at the [wiki](https://git.cs.uni-kl.de/siebert/curveball/wikis/home)

