
These are the java implementations of the global curveball algorithm.

It uses [Maven](http://maven.apache.org/guides/) (see pom.xml)

If you are using an IDE, please be sure to import the project as a maven project.

Some code examples are located in the package "experiments"

After packaging (`mvn package`) one can run the code through the command line

```bash
java -cp fdsmCurveballJava-1.0-SNAPSHOT.jar:OTHER_EXTERNAL_JARS_HERE.jar de.kl.uni.cs.aalab.experiments.profiling.ProfilingLoadAdjacencyList ~/PATH_TO_THE_CODE/fdsmCurveballJava/src/main/resources/brunson_revolution/adj.txt 136 1
>>> Time elapsed: 203 ms
```
