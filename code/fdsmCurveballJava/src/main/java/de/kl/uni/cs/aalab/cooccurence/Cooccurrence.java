package de.kl.uni.cs.aalab.cooccurence;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Interface for all curveballAndCooccurrence algorithms implementations
 */
public interface Cooccurrence {


    /**
     * Converts an adjacency list of a bipartite graph L->R to R->L
     *
     *
     * @param adjacencyList a reference to the adjacency list
     * @param nl the number of nodes on the left side
     * @param nr the number of nodes on the right side
     * @return
     */
    static HashSet<Integer>[] convert(HashSet<Integer>[] adjacencyList, int nl, int nr){
        HashSet<Integer>[] converted = new HashSet[nr];
        for (int i = 0; i < nr; i++) {
            converted[i] = new HashSet<Integer>(); // TODO initial capacity an load factor?
        }
        for (int i = 0; i < nl; i++) {
            HashSet<Integer> neighborhood = adjacencyList[i];
            for (Integer node: neighborhood){
                converted[node].add(i);
            }
        }
        return converted;
    }
}
