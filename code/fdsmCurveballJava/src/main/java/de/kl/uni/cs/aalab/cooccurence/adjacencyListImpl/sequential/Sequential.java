package de.kl.uni.cs.aalab.cooccurence.adjacencyListImpl.sequential;

import de.kl.uni.cs.aalab.cooccurence.Cooccurrence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * A sequential implementation of the curveballAndCooccurrence computation algorithm
 */
public class Sequential implements Cooccurrence {

    /**
     * Computes the full symmetric curveballAndCooccurrence matrix of a bipartite graph
     * Using the Right side of the adjacency list
     * <p>
     * <b>Assumes that adjacency list stores indices of nodes on the left side</b>
     *
     * @param rightSideAdjacencyList a reference to the list of neighborhood sets of the right side nodes of a bipartite graphs
     * @param nl                     the number of nodes on the left side of the bipartite graph
     * @return the full symmetric curveballAndCooccurrence matrix
     */
    public static int[][] cooccRight(HashSet<Integer>[] rightSideAdjacencyList, int nl) {
        int[][] cooccurrence = new int[nl][nl];
        for (HashSet<Integer> neighborsLeftSide : rightSideAdjacencyList) {
            for (int i : neighborsLeftSide) {
                for (int j : neighborsLeftSide) {
                    if (i != j) {
                        cooccurrence[i][j] += 1;
                    }
                }
            }
        }
        return cooccurrence;
    }


    /**
     * Computes the full symmetric curveballAndCooccurrence matrix of a bipartite graph
     * Using the Left side of the adjacency list
     * <p>
     * <b>Assumes that adjacency list stores indices of nodes on the right side</b>
     *
     * @param adjacencyList a reference to the list of neighborhood sets of the right side nodes of a bipartite graphs
     * @param nl            the number of nodes on the left side of the bipartite graph
     * @param nr            the number of nodes on the right side of the bipartite graph
     * @return the full symmetric curveballAndCooccurrence matrix
     */
    public static int[][] cooccLeft(HashSet<Integer>[] adjacencyList, int nl, int nr) {
        // initialization of the full symmetric matrix
        int[][] cooccurrence = new int[nl][nl];
        // a set of all nodes from the right side that have been processed so far
        HashSet<Integer> processed = new HashSet<>(nr, 1);
        // loop through all left side nodes
        for (int i = 0; i < nl; i++) {
            // loop through all the neighbouring nodes
            for (Integer neighbor : adjacencyList[i]) {
                // check whether the neighbor has been processed already
                if (!processed.contains(neighbor)) {
                    // a list of nodes that will see their curveballAndCooccurrence incremented
                    ArrayList<Integer> toBeIncremented = new ArrayList<>();
                    toBeIncremented.add(i);
                    // loop through all the nodes on the left side > i
                    for (int j = i + 1; j < nl; j++) {
                        // if their neighborhood contains the current 'neighbor', then it is a common neighbor and curveballAndCooccurrence must be incremented
                        if (adjacencyList[j].contains(neighbor)) {
                            toBeIncremented.add(j);
                        }
                    }
                    // increment curveballAndCooccurrence of the nodes saved so far
                    for (int k = 0; k < toBeIncremented.size() - 1; k++) {
                        for (int l = k + 1; l < toBeIncremented.size(); l++) {
                            cooccurrence[toBeIncremented.get(k)][toBeIncremented.get(l)] += 1;
                            cooccurrence[toBeIncremented.get(l)][toBeIncremented.get(k)] += 1;
                        }
                    }
                    // mark the node on the right side as processed
                    processed.add(neighbor);
                }
            }
        }

        return cooccurrence;
    }

    /**
     * Computes the full symmetric curveballAndCooccurrence matrix of a bipartite graph
     * Using the Left side of the adjacency list
     * <p>
     * <b>Assumes that adjacency list stores indices of nodes on the right side</b>
     *
     * @param adjacencyList a reference to the list of neighborhood sets of the right side nodes of a bipartite graphs
     * @param nl            the number of nodes on the left side of the bipartite graph
     * @param nr            the number of nodes on the right side of the bipartite graph
     * @return a hash map with all curveballAndCooccurrence != 0 (key = "node_i node_j", value = coocc(node_i,node_j)"
     */
    public static HashMap<String, Integer> coocc(HashSet<Integer>[] adjacencyList, int nl, int nr) {
        // initialization of the full symmetric matrix
        HashMap<String, Integer> cooccurrence = new HashMap<String, Integer>();
        // a set of all nodes from the right side that have been processed so far
        HashSet<Integer> processed = new HashSet<>(nr, 1);
        // loop through all left side nodes
        for (int i = 0; i < nl; i++) {
            // loop through all the neighbouring nodes
            for (Integer neighbor : adjacencyList[i]) {
                // check whether the neighbor has been processed already
                if (!processed.contains(neighbor)) {
                    // a list of nodes that will see their curveballAndCooccurrence incremented
                    ArrayList<Integer> toBeIncremented = new ArrayList<>();
                    toBeIncremented.add(i);
                    // loop through all the nodes on the left side > i
                    for (int j = i + 1; j < nl; j++) {
                        // if their neighborhood contains the current 'neighbor', then it is a common neighbor and curveballAndCooccurrence must be incremented
                        if (adjacencyList[j].contains(neighbor)) {
                            toBeIncremented.add(j);
                        }
                    }
                    // increment curveballAndCooccurrence of the nodes saved so far
                    for (int k = 0; k < toBeIncremented.size() - 1; k++) {
                        for (int l = k + 1; l < toBeIncremented.size(); l++) {
                            String key = String.format("%d %d",toBeIncremented.get(k),toBeIncremented.get(l));
                            cooccurrence.merge(key, 1, (oldValue, one) -> oldValue + one);
                        }
                    }
                    // mark the node on the right side as processed
                    processed.add(neighbor);
                }
            }
        }

        return cooccurrence;
    }


    /**
     * Naive implementation of the curveballAndCooccurrence using hashmap for storing the results
     *
     * @param adjacencyList
     * @return
     */
    public static HashMap<String, Integer> cooccNaive(HashSet<Integer>[] adjacencyList) {
        HashMap<String, Integer> cooccurrence = new HashMap<String, Integer>();
        for (int i = 0; i < adjacencyList.length - 1; i++) {
            for (int j = i + 1; j < adjacencyList.length; j++) {
                HashSet<Integer> commons = new HashSet<>(adjacencyList[i]);
                commons.retainAll(adjacencyList[j]);
                if (!commons.isEmpty()) {
                    cooccurrence.put(String.format("%d %d", i, j), commons.size());
                }
            }
        }
        return cooccurrence;
    }




}
