package de.kl.uni.cs.aalab.cooccurence.adjacencyListImpl.thread;


import de.kl.uni.cs.aalab.datastructure.CooccHashMap;

import java.util.HashSet;
import java.util.concurrent.Callable;

/**
 * Cooccurrence computation done in parallel by threads
 */
public class ParallelCooccurrenceTask implements Callable<CooccHashMap> {

    private int startIndex;
    private int endIndex;
    private HashSet<Integer>[] adjacencyList;
    private CooccHashMap coocc;

    /**
     * Constructor
     *
     * @param adjacencyList : a reference to the list of all RIGHT SIDE neighborhood sets (RIGHT SIDE ADJACENCY LIST)
     * @param startIndex    : the RIGHT SIDE node index from which to start computing the cooccurrence
     * @param endIndex      : the RIGHT SIDE node index before which we stop computing cooccurrence
     */
    public ParallelCooccurrenceTask(HashSet<Integer>[] adjacencyList, int startIndex, int endIndex) {
        this.coocc = new CooccHashMap();
        this.adjacencyList = adjacencyList;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }


    /**
     * computes cooccurrence coocc(u,v) where u in [startX, endX[ and v in [startY, endY[
     */
    @Override
    public CooccHashMap call() {
        for (int i = this.startIndex; i < this.endIndex; i++) {
            HashSet<Integer> neighborsLeftSide = this.adjacencyList[i];
            for (int u : neighborsLeftSide) {
                for (int v : neighborsLeftSide) {
                    if (u > v) {
                        this.coocc.increment(u,v,1);
                    }
                }
            }
        }
        return this.coocc;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public int getStartIndex() {
        return startIndex;
    }
}
