package de.kl.uni.cs.aalab.cooccurence.adjacencyListImpl.thread;

import de.kl.uni.cs.aalab.datastructure.CooccHashMap;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * A thread implementation of the Curveball algorithm using Threads,
 * and using adjacency list
 * where self-loops are allowed
 */
public class ThreadImpl {

    private ExecutorService pool;
    private ArrayList<ParallelCooccurrenceTask> tasks;


    /**
     * Constructor
     * <p>
     * Creates:
     * <ul>
     * <li>A pool of threads (fixed number of threads)</li>
     * <li>A list of task to be executed by the threads.
     * Where:
     * Each task consists computing cooccurrence coocc(u,v).
     * All task are independent from each other.
     * I.e., they work on disjoint subsets of the adjacency list.
     * This is important to be sure that no two threads modify the same elements in the adjacency list
     * </li>
     * </ul>
     *
     * @param nbThreads:     the number of threads that will share the task of computing one step of the Global Curveball algorithm
     * @param adjacencyList: a reference to the list of neighborhood sets
     */
    public ThreadImpl(int nbThreads, HashSet<Integer>[] adjacencyList) {
        // creating a pool of threads
        this.pool = Executors.newFixedThreadPool(nbThreads);

        // create a list of tasks to be executed in thread by the threads
        this.tasks = new ArrayList<ParallelCooccurrenceTask>(nbThreads);

        // number of elements to be treated by a single thread
        int delta = adjacencyList.length / nbThreads;

        // the first nbThreads - 1 task will work on exactly delta elements
        for (int i = 0; i < nbThreads - 1; i++) {
            tasks.add(new ParallelCooccurrenceTask( adjacencyList, delta * i, delta * (i + 1)));
        }
        // the last one works on the remaining elements (can be more than delta)
        tasks.add(new ParallelCooccurrenceTask(adjacencyList, delta * (nbThreads - 1), adjacencyList.length));

    }

    protected ArrayList<ParallelCooccurrenceTask> getTasks() {
        return this.tasks;
    }


    public CooccHashMap coocc() throws InterruptedException {
        CooccHashMap coocc = new CooccHashMap();
        // submit the tasks to the pool for them to be executed
        List<Future<CooccHashMap>> futures = pool.invokeAll(this.tasks);
        // aggregating results (reduce)
        for(Future<CooccHashMap> future : futures){
            try {
                for(Map.Entry<CooccHashMap.Key, Integer> entry :future.get().entrySet()){
                    coocc.increment(entry.getKey(), entry.getValue());
                }
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        return coocc;
    }

    /**
     * Shutdown the pool of threads
     */
    public void shutdown() {
        this.pool.shutdown();
    }

}
