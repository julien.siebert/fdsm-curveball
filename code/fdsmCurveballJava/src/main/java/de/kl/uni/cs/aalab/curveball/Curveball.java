package de.kl.uni.cs.aalab.curveball;

/**
 * Interface for every implementation of the Global Curveball Algorithm
 */
public interface Curveball {

    /**
     * Runs one step of the Global Curveball
     * Modifies the inner data structure (i.e., in-place method)
     */
    void step() throws Exception;
}