package de.kl.uni.cs.aalab.curveball.adjacencyListImpl;

import java.util.*;

/**
 * Parent Class for all Adjacency List based implementations of the Global Curveball algorithm
 */
public class AdjacencyListImpl {

    protected int[] nodesIndices;
    protected HashSet<Integer>[] adjacencyList;
    protected Random rnd;

    /**
     * Constructor
     *
     * @param nodesIndices  : a reference to the list of nodes indices
     * @param adjacencyList : a reference to the list of neighborhood sets
     * @param rnd           : a reference to the random generator
     */
    public AdjacencyListImpl(int[] nodesIndices, HashSet<Integer>[] adjacencyList, Random rnd) {
        this.nodesIndices = nodesIndices;
        this.adjacencyList = adjacencyList;
        this.rnd = rnd;
    }

    /**
     * In place method to trade neighbors from node i and node j
     * Can create self-loops!
     * <ul>
     * <li>bipartite graphs (will never create self-loop)</li>
     * <li>directed graphs (can create self-loops)</li>
     * </ul>
     *
     * @param i: the index of the first node involved in the trading
     * @param j: the index of the second node involved in the trading
     */
    public void tradeWithSelfLoop(int i, int j) {
        // allocation of 2 new sets: Pi and Pj, the sets of possible trades for both nodes i and j
        HashSet<Integer> possibleTrades_i = new HashSet<>(this.adjacencyList[i].size(), 1);
        HashSet<Integer> possibleTrades_j = new HashSet<>(this.adjacencyList[j].size(), 1);
        getPossibleTradesWithSelfLoop(i, j, possibleTrades_i, possibleTrades_j);
        // only performs trade when there is something to trade
        if (!possibleTrades_i.isEmpty() && !possibleTrades_j.isEmpty()) {
            trade(i, j, possibleTrades_i, possibleTrades_j);
        }
    }


    /**
     * In place method to trade neighbors from node i and node j
     * Cannot create self-loops
     * (but if self-loops are included in the graph from the start, then they will remain in the graph)
     * <p>
     * - bipartite graphs (will never create self-loop)
     * - directed graphs (will never create self-loop)
     *
     * @param i: the index of the first node involved in the trading
     * @param j: the index of the second node involved in the trading
     */
    public void tradeWithoutSelfLoop(int i, int j) {
        // allocation of 2 new sets: Pi and Pj, the sets of possible trades for both nodes i and j
        HashSet<Integer> possibleTrades_i = new HashSet<>(this.adjacencyList[i].size(), 1);
        HashSet<Integer> possibleTrades_j = new HashSet<>(this.adjacencyList[j].size(), 1);
        getPossibleTradesWithoutSelfLoop(i, j, possibleTrades_i, possibleTrades_j);
        // only performs trade when there is something to trade
        if (!possibleTrades_i.isEmpty() && !possibleTrades_j.isEmpty()) {
            trade(i, j, possibleTrades_i, possibleTrades_j);
        }
    }


    /**
     * In place method to trade neighbors from node i and node j
     * Can create self-loops!
     * <ul>
     * <li>bipartite graphs (will never create self-loop)</li>
     * <li>directed graphs (can create self-loops)</li>
     * </ul>
     *
     * @param i: the index of the first node involved in the trading
     * @param j: the index of the second node involved in the trading
     * @return curveballAndCooccurrence between nodes i and j
     */
    public int cooccurenceWhileTradeWithSelfLoop(int i, int j) {
        // allocation of 2 new sets: Pi and Pj, the sets of possible trades for both nodes i and j
        HashSet<Integer> possibleTrades_i = new HashSet<>(this.adjacencyList[i].size(), 1);
        HashSet<Integer> possibleTrades_j = new HashSet<>(this.adjacencyList[j].size(), 1);
        this.getPossibleTradesWithSelfLoop(i, j, possibleTrades_i, possibleTrades_j);
        // computes curveballAndCooccurrence between nodes i and j
        int cooccurrence = adjacencyList[i].size() - possibleTrades_i.size();
        // only performs trade when there is something to trade
        if (!possibleTrades_i.isEmpty() && !possibleTrades_j.isEmpty()) {
            this.trade(i, j, possibleTrades_i, possibleTrades_j);
        }
        return cooccurrence;
    }

    /**
     * In place method to trade neighbors from node i and node j
     * Cannot create self-loops
     * (but if self-loops are included in the graph from the start, then they will remain in the graph)
     * <p>
     * - bipartite graphs (will never create self-loop)
     * - directed graphs (will never create self-loop)
     *
     * @param i: the index of the first node involved in the trading
     * @param j: the index of the second node involved in the trading
     * @return curveballAndCooccurrence between nodes i and j
     */
    public int cooccurrenceWhileTradeWithoutSelfLoop(int i, int j) {
        // allocation of 2 new sets: Pi and Pj, the sets of possible trades for both nodes i and j
        HashSet<Integer> possibleTrades_i = new HashSet<>(this.adjacencyList[i].size(), 1);
        HashSet<Integer> possibleTrades_j = new HashSet<>(this.adjacencyList[j].size(), 1);
        getPossibleTradesWithoutSelfLoop(i, j, possibleTrades_i, possibleTrades_j);
        // computes curveballAndCooccurrence between nodes i and j
        int cooccurrence = adjacencyList[i].size() - possibleTrades_i.size();
        if (adjacencyList[i].contains(j)){
            cooccurrence --;
        }
        // only performs trade when there is something to trade
        if (!possibleTrades_i.isEmpty() && !possibleTrades_j.isEmpty()) {
            trade(i, j, possibleTrades_i, possibleTrades_j);
        }
        return cooccurrence;
    }

    /**
     * In place method for preparing the set of neighbors that can be traded for both nodes i and j
     * Allowing self loops: possibleTrade_i can contain j, possibleTrade_j can contain i
     *
     * @param i:                index of node i
     * @param j:                index of node j
     * @param possibleTrades_i: a reference to the set of neighbors of node i that can be traded
     * @param possibleTrades_j: a reference to the set of neighbors of node j that can be traded
     */
    protected void getPossibleTradesWithSelfLoop(int i, int j, HashSet<Integer> possibleTrades_i, HashSet<Integer> possibleTrades_j) {
        possibleTrades_i.addAll(this.adjacencyList[i]);
        possibleTrades_j.addAll(this.adjacencyList[j]);
        // get elements available for trading: set difference Pi = Ni \ Nj and Pj = Nj \ Ni
        possibleTrades_i.removeAll(this.adjacencyList[j]);
        possibleTrades_j.removeAll(this.adjacencyList[i]);
    }


    /**
     * In place method for preparing the set of neighbors that can be traded for both nodes i and j
     * Not allowing self loops: possibleTrade_i should not contain j, possibleTrade_j should not contain i
     *
     * @param i:                index of node i
     * @param j:                index of node j
     * @param possibleTrades_i: a reference to the set of neighbors of node i that can be traded
     * @param possibleTrades_j: a reference to the set of neighbors of node j that can be traded
     */
    protected void getPossibleTradesWithoutSelfLoop(int i, int j, HashSet<Integer> possibleTrades_i, HashSet<Integer> possibleTrades_j) {
        getPossibleTradesWithSelfLoop(i, j, possibleTrades_i, possibleTrades_j);
        possibleTrades_i.remove(j);
        possibleTrades_j.remove(i);
    }

    /**
     * In place method for trading neighbors between node i and node j
     *
     * @param i                index of node i
     * @param j                index of node j
     * @param possibleTrades_i a set of neighbors that node i can possibly trade with node j
     * @param possibleTrades_j a set of neighbors that node j can possibly trade with node i
     */
    public void trade(int i, int j, HashSet<Integer> possibleTrades_i, HashSet<Integer> possibleTrades_j) {
        // compute how many elements node i can tradeWithSelfLoop
        int nbTrades_i = possibleTrades_i.size();
        // remove the elements available for trading from the original sets
        this.adjacencyList[i].removeAll(possibleTrades_i);
        this.adjacencyList[j].removeAll(possibleTrades_j);
        // get all possible trades in a single set: in place union: P = Pi U Pj


        // do the union (addAll) using the smallest HashSet as argument
        ArrayList<Integer> possibleTrades = new ArrayList<>(getAllPossibleTrades(possibleTrades_i, possibleTrades_j));
        // shuffle the possible trades
        Collections.shuffle(possibleTrades, this.rnd);
        // add traded elements to original neighborhood sets Ni and Nj
        this.adjacencyList[i].addAll(possibleTrades.subList(0, nbTrades_i));
        this.adjacencyList[j].addAll(possibleTrades.subList(nbTrades_i, possibleTrades.size()));
    }

    /**
     * Return union of both set of neighbors that i and j can possibly trade
     *
     * @param possibleTrades_i
     * @param possibleTrades_j
     * @return
     */
    protected HashSet<Integer> getAllPossibleTrades(HashSet<Integer> possibleTrades_i, HashSet<Integer> possibleTrades_j) {
        HashSet<Integer> possibleTrades = new HashSet<>();
        possibleTrades.addAll(possibleTrades_i);
        possibleTrades.addAll(possibleTrades_j);
        return possibleTrades;
    }

    public HashSet<Integer>[] getAdjacencyList() {
        return adjacencyList;
    }

    public int[] getNodesIndices() {
        return nodesIndices;
    }

    public Random getRnd() {
        return rnd;
    }
}
