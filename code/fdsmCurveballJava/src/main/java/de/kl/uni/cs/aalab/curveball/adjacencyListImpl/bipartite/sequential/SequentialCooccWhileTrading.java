package de.kl.uni.cs.aalab.curveball.adjacencyListImpl.bipartite.sequential;

import de.kl.uni.cs.aalab.curveball.Curveball;
import de.kl.uni.cs.aalab.curveball.adjacencyListImpl.AdjacencyListImpl;
import de.kl.uni.cs.aalab.utilities.Shuffle;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;
import java.util.Random;


/**
 * Sequential Implementation of the Global Curveball Algorithm
 * Using Adjacency List
 * Allowing Self-Loops (e.g., for bipartite graphs or directed graphs with self-loops)
 * Samples Cooccurrence while trading (i.e., for each trade(i,j), computes coocc(i,j) and saves it to a file)
 */
public class SequentialCooccWhileTrading extends AdjacencyListImpl implements Curveball {

    private BufferedWriter writer;

    /**
     * Constructor
     *
     * @param nodesIndices  : a reference to the list of nodes indices
     * @param adjacencyList : a reference to the list of neighborhood sets
     * @param rnd           : a reference to the random generator
     */
    public SequentialCooccWhileTrading(int[] nodesIndices, HashSet<Integer>[] adjacencyList, Random rnd, Path cooccFilename) {
        super(nodesIndices, adjacencyList, rnd);

        try {
            this.writer = Files.newBufferedWriter(cooccFilename, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Runs one step of the Global Curveball algorithm
     * Self-Loops are allowed!
     * Adjacency List implementation
     */
    @Override
    public void step() throws IOException {
        Shuffle.shuffleArray(this.nodesIndices, this.rnd);
        for (int i = 0; i < this.nodesIndices.length - 1; i = i + 2) {
            this.writer.write(this.nodesIndices[i] + " " + this.nodesIndices[i + 1] + " " + this.cooccurenceWhileTradeWithSelfLoop(this.nodesIndices[i], this.nodesIndices[i + 1]) + "\n");
        }
    }

    public void close() throws IOException {
        this.writer.close();
    }
}
