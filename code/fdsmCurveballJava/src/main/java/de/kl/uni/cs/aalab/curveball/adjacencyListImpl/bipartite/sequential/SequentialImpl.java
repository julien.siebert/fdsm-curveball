package de.kl.uni.cs.aalab.curveball.adjacencyListImpl.bipartite.sequential;

import de.kl.uni.cs.aalab.curveball.Curveball;
import de.kl.uni.cs.aalab.curveball.adjacencyListImpl.AdjacencyListImpl;
import de.kl.uni.cs.aalab.utilities.Shuffle;

import java.util.HashSet;
import java.util.Random;


/**
 * Sequential Implementation of the Global Curveball Algorithm
 * Using Adjacency List
 * Allowing Self-Loops (e.g., for bipartite graphs or directed graphs with self-loops)
 */
public class SequentialImpl extends AdjacencyListImpl implements Curveball {


    /**
     * Constructor
     *  @param nodesIndices  : a reference to the list of nodes indices
     * @param adjacencyList : a reference to the list of neighborhood sets
     * @param rnd           : a reference to the random generator
     */
    public SequentialImpl(int[] nodesIndices, HashSet<Integer>[] adjacencyList, Random rnd) {
        super(nodesIndices, adjacencyList, rnd);
    }

    /**
     * Runs one step of the Global Curveball algorithm
     * Self-Loops are allowed!
     * Adjacency List implementation
     */
    @Override
    public void step() {
        Shuffle.shuffleArray(this.nodesIndices, this.rnd);
        for (int i = 0; i < this.nodesIndices.length - 1; i = i + 2) {
            this.tradeWithSelfLoop(this.nodesIndices[i], this.nodesIndices[i + 1]);
        }
    }
}
