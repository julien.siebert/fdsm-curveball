package de.kl.uni.cs.aalab.curveball.adjacencyListImpl.bipartite.thread;

import de.kl.uni.cs.aalab.curveball.adjacencyListImpl.AdjacencyListImpl;

import java.util.HashSet;
import java.util.Random;
import java.util.concurrent.Callable;

/**
 * Trading methods that can be called in thread by threads
 */
public class ParallelTradingTask extends AdjacencyListImpl implements Callable<Void> {

    private int startIndex;
    private int endIndex;

    /**
     * Constructor
     *  @param nodesIndices :  a reference to the list of all nodes indices
     * @param adjacencyList : a reference to the list of all neighborhood sets
     * @param rnd :           a reference to an instance of a random generator
     * @param startIndex :    the index where to start the trading (inclusive)
     * @param endIndex :      the index where to stop the trading (exclusive)
     */
    public ParallelTradingTask(int[] nodesIndices, HashSet<Integer>[] adjacencyList, Random rnd, int startIndex, int endIndex) {
        super(nodesIndices, adjacencyList, rnd);
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }

    protected int getStartIndex(){
        return this.startIndex;
    }

    protected int getEndIndex(){
        return this.endIndex;
    }


    /**
     * Runs one step of the Global Curveball Algorithm on a subset of the adjacency list
     *
     * @throws Exception if unable to compute a result
     */
    @Override
    public Void call() {
        for (int i = this.startIndex; i < this.endIndex - 1; i = i + 2) {
            this.tradeWithSelfLoop(this.nodesIndices[i], this.nodesIndices[i + 1]);
        }
        return null;
    }
}
