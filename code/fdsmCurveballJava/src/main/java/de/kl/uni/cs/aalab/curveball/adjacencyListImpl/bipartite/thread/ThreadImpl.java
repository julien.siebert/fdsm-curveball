package de.kl.uni.cs.aalab.curveball.adjacencyListImpl.bipartite.thread;

import de.kl.uni.cs.aalab.curveball.Curveball;
import de.kl.uni.cs.aalab.curveball.adjacencyListImpl.AdjacencyListImpl;
import de.kl.uni.cs.aalab.utilities.Shuffle;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * A thread implementation of the Curveball algorithm using Threads,
 * and using adjacency list
 * where self-loops are allowed
 */
public class ThreadImpl extends AdjacencyListImpl implements Curveball {

    private ExecutorService pool;
    private ArrayList<ParallelTradingTask> tasks;


    /**
     * Constructor
     * <p>
     * Creates:
     * <ul>
     * <li>A pool of threads (fixed number of threads)</li>
     * <li>A list of task to be executed by the threads.
     * Where:
     * Each task consists of the in place trading of neighbors.
     * All task are independent from each other.
     * I.e., they work on disjoint subsets of the adjacency list.
     * This is important to be sure that no two threads modify the same elements in the adjacency list
     * </li>
     * </ul>
     *
     * @param nbThreads:     the number of threads that will share the task of computing one step of the Global Curveball algorithm
     * @param nodesIndices:  a reference to the list of nodes indices
     * @param adjacencyList: a reference to the list of neighborhood sets
     * @param rnd:           a reference to the random generator
     */
    public ThreadImpl(int nbThreads, int[] nodesIndices, HashSet<Integer>[] adjacencyList, Random rnd) {
        super(nodesIndices, adjacencyList, rnd);
        // creating a pool of threads
        this.pool = Executors.newFixedThreadPool(nbThreads);
        // create a list of tasks to be executed in thread by the threads
        // number of elements to be treated by a single thread
        int delta = nodesIndices.length / nbThreads;
        this.tasks = new ArrayList<ParallelTradingTask>(nbThreads);
        // the first nbThreads - 1 task will work on exactly delta elements
        for (int i = 0; i < nbThreads - 1; i++) {
            tasks.add(new ParallelTradingTask(nodesIndices, adjacencyList, rnd, delta * i, delta * (i + 1)));
        }
        // the last one works on the remaining elements (can be more than delta)
        tasks.add(new ParallelTradingTask(nodesIndices, adjacencyList, rnd, delta * (nbThreads - 1), nodesIndices.length));
    }

    protected ArrayList<ParallelTradingTask> getTasks() {
        return this.tasks;
    }

    /**
     * Runs one step of the Global Curveball algorithm
     * <p>
     * Can timeout! Then will throw exception
     */
    @Override
    public void step() throws InterruptedException {
        Shuffle.shuffleArray(this.nodesIndices, this.rnd);
        // submit the tasks to the pool for them to be executed
        pool.invokeAll(this.tasks);
    }

    /**
     * Shutdown the pool of threads
     */
    public void shutdown() {
        this.pool.shutdown();
    }
}
