package de.kl.uni.cs.aalab.curveball.adjacencyMatrixImpl;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Random;

/**
 * Parent Class for all Adjacency Matrix based implementations of the Global Curveball algorithm
 */
public class AdjacencyMatrixImpl {

    protected int[] nodesIndices;
    protected ArrayList<BitSet> adjacencyMatrix;
    protected Random rnd;

    /**
     * Constructor
     *  @param nodesIndices :    a list of nodes indices
     * @param adjacencyMatrix : a list of bitset representing rows of the adjacency matrix
     * @param rnd :             a random generator
     */
    public AdjacencyMatrixImpl(int[] nodesIndices, ArrayList<BitSet> adjacencyMatrix, Random rnd) {
        this.adjacencyMatrix = adjacencyMatrix;
        this.nodesIndices = nodesIndices;
        this.rnd = rnd;
    }

    /**
     * Splits randomly a given BitSet B0 in two different BitSets: B1 and B2.
     * <p>
     * The first BitSet B1 contains `nbOnesInFirst` ones, picked uniformly at random from the given one B0.
     *
     * @param toSplit:       the BitSet to split (aka B0)
     * @param nbOnesInFirst: the number of ones that the first BitSet B1 needs to contain
     */
    protected BitSet[] randomSplit(BitSet toSplit, int nbOnesInFirst) {

        BitSet first = new BitSet(toSplit.length());
        BitSet second = new BitSet(toSplit.length());

        int nbOnesTotal = toSplit.cardinality();

        for (int i = toSplit.length(); (i = toSplit.previousSetBit(i-1)) >= 0; ) {
            int k = this.rnd.nextInt(nbOnesTotal);
            if (k < nbOnesInFirst){
                first.set(i);
                nbOnesInFirst -= 1;
            } else {
                second.set(i);
            }
            nbOnesTotal -= 1;
        }

        BitSet[] res = new BitSet[2];
        res[0] = first;
        res[1] = second;
        return res;
    }

    /**
     * In place method to trade neighbors from node i and node j
     * Can create self-loops!
     * <ul>
     *     <li>bipartite graphs (will never create self-loop)</li>
     *     <li>directed graphs (can create self-loops)</li>
     * </ul>
     *
     * @param indexFirstRow:             the index of the first node involved in the trading
     * @param indexSecondRow:            the index of the second node involved in the trading
     */
    public void tradeWithSelfLoop(int indexFirstRow, int indexSecondRow) {
        BitSet matrixRowI = this.adjacencyMatrix.get(indexFirstRow);
        BitSet matrixRowJ = this.adjacencyMatrix.get(indexSecondRow);
        // create a mask
        BitSet mask = (BitSet) matrixRowI.clone();
        mask.xor(matrixRowJ);
        // count the number of elements that can be trades for both nodes and set the trading elements to zero
        int nbTradesFirst = 0;
        for (int i = mask.length(); (i = mask.previousSetBit(i-1)) >= 0; ) {
            if (matrixRowI.get(i)){
                nbTradesFirst += 1;
                matrixRowI.clear(i);
            } else {
                matrixRowJ.clear(i);
            }
        }

        BitSet[] res = this.randomSplit(mask, nbTradesFirst);

        // update both matrix row
        matrixRowI.or(res[0]);
        matrixRowJ.or(res[1]);
    }

    public int[] getNodesIndices() {
        return nodesIndices;
    }

    public ArrayList<BitSet> getAdjacencyMatrix() {
        return adjacencyMatrix;
    }

    public Random getRnd() {
        return rnd;
    }
}
