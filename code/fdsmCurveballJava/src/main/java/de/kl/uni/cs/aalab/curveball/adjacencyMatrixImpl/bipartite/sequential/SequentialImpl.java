package de.kl.uni.cs.aalab.curveball.adjacencyMatrixImpl.bipartite.sequential;

import de.kl.uni.cs.aalab.curveball.Curveball;
import de.kl.uni.cs.aalab.curveball.adjacencyMatrixImpl.AdjacencyMatrixImpl;
import de.kl.uni.cs.aalab.utilities.Shuffle;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Random;

/**
 * Sequential Implementation of the Global Curveball Algorithm
 * Using Adjacency Matrix
 * Allowing Self-Loops (e.g., for bipartite graphs or directed graphs with self-loops)
 */
public class SequentialImpl extends AdjacencyMatrixImpl implements Curveball {

    /**
     * Constructor
     *  @param nodesIndices    : a reference to the list of nodes indices
     * @param adjacencyMatrix : a reference to the  list of bitset representing rows of the adjacency matrix
     * @param rnd             : a reference to the random generator
     */
    public SequentialImpl(int[] nodesIndices, ArrayList<BitSet> adjacencyMatrix, Random rnd) {
        super(nodesIndices, adjacencyMatrix, rnd);
    }

    /**
     * Runs one step of the Global Curveball algorithm
     * Self-Loops are allowed!
     * Adjacency Matrix implementation
     */
    @Override
    public void step() {
        Shuffle.shuffleArray(this.nodesIndices, this.rnd);
        for (int i = 0; i < this.nodesIndices.length -1; i = i + 2) {
            this.tradeWithSelfLoop(this.nodesIndices[i], this.nodesIndices[i+1]);
        }
    }
}
