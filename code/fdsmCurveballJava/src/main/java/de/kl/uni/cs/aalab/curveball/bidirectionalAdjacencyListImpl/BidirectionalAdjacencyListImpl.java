package de.kl.uni.cs.aalab.curveball.bidirectionalAdjacencyListImpl;

import de.kl.uni.cs.aalab.datastructure.BidirectionalAdjacencyList;

import java.util.*;

public class BidirectionalAdjacencyListImpl {

    protected BidirectionalAdjacencyList bidirectionalAdjacencyList;
    protected Random rnd;

    /**
     * Constructor
     *
     * @param bidirectionalAdjacencyList : a reference to the bidirectional list of neighborhood sets
     * @param rnd                        : a reference to the random generator
     */
    public BidirectionalAdjacencyListImpl(BidirectionalAdjacencyList bidirectionalAdjacencyList, Random rnd) {
        this.bidirectionalAdjacencyList = bidirectionalAdjacencyList;
        this.rnd = rnd;
    }

    /**
     * In place method to trade neighbors from node i and node j
     * Can create self-loops!
     * <ul>
     * <li>bipartite graphs (will never create self-loop)</li>
     * <li>directed graphs (can create self-loops)</li>
     * </ul>
     *
     * @param i: the index of the first node involved in the trading
     * @param j: the index of the second node involved in the trading
     */
    public void tradeWithSelfLoop(int i, int j) {
        // allocation of 2 new sets: Pi and Pj, the sets of possible trades for both nodes i and j
        HashSet<Integer> possibleTrades_i = new HashSet<>(this.bidirectionalAdjacencyList.getAdjacencyListLeft()[i].size(), 1);
        HashSet<Integer> possibleTrades_j = new HashSet<>(this.bidirectionalAdjacencyList.getAdjacencyListLeft()[j].size(), 1);
        getPossibleTradesWithSelfLoop(i, j, possibleTrades_i, possibleTrades_j);
        // only performs trade when there is something to trade
        if (!possibleTrades_i.isEmpty() && !possibleTrades_j.isEmpty()) {
            trade(i, j, possibleTrades_i, possibleTrades_j);
        }
    }


    /**
     * In place method to trade neighbors from node i and node j (both nodes belonging to the right side of the bipartite graph)
     * Can create self-loops!
     * <ul>
     * <li>bipartite graphs (will never create self-loop)</li>
     * <li>directed graphs (can create self-loops)</li>
     * </ul>
     *
     * @param i: the index of the first node involved in the trading
     * @param j: the index of the second node involved in the trading
     */
    public void tradeRightWithSelfLoop(int i, int j) {
        // allocation of 2 new sets: Pi and Pj, the sets of possible trades for both nodes i and j
        HashSet<Integer> possibleTrades_i = new HashSet<>(this.bidirectionalAdjacencyList.getAdjacencyListRight()[i].size(), 1);
        HashSet<Integer> possibleTrades_j = new HashSet<>(this.bidirectionalAdjacencyList.getAdjacencyListRight()[j].size(), 1);
        getPossibleTradesRightWithSelfLoop(i, j, possibleTrades_i, possibleTrades_j);
        // only performs trade when there is something to trade
        if (!possibleTrades_i.isEmpty() && !possibleTrades_j.isEmpty()) {
            tradeRight(i, j, possibleTrades_i, possibleTrades_j);
        }
    }


    /**
     * In place method to trade neighbors from node i and node j
     * Cannot create self-loops
     * (but if self-loops are included in the graph from the start, then they will remain in the graph)
     * <p>
     * - bipartite graphs (will never create self-loop)
     * - directed graphs (will never create self-loop)
     *
     * @param i: the index of the first node involved in the trading
     * @param j: the index of the second node involved in the trading
     */
    public void tradeWithoutSelfLoop(int i, int j) {
        // allocation of 2 new sets: Pi and Pj, the sets of possible trades for both nodes i and j
        HashSet<Integer> possibleTrades_i = new HashSet<>(this.bidirectionalAdjacencyList.getAdjacencyListLeft()[i].size(), 1);
        HashSet<Integer> possibleTrades_j = new HashSet<>(this.bidirectionalAdjacencyList.getAdjacencyListLeft()[j].size(), 1);
        getPossibleTradesWithoutSelfLoop(i, j, possibleTrades_i, possibleTrades_j);
        // only performs trade when there is something to trade
        if (!possibleTrades_i.isEmpty() && !possibleTrades_j.isEmpty()) {
            trade(i, j, possibleTrades_i, possibleTrades_j);
        }
    }


    /**
     * In place method to trade neighbors from node i and node j
     * Can create self-loops!
     * <ul>
     * <li>bipartite graphs (will never create self-loop)</li>
     * <li>directed graphs (can create self-loops)</li>
     * </ul>
     *
     * @param i: the index of the first node involved in the trading
     * @param j: the index of the second node involved in the trading
     * @return curveballAndCooccurrence between nodes i and j
     */
    public int cooccurenceWhileTradeWithSelfLoop(int i, int j) {
        // allocation of 2 new sets: Pi and Pj, the sets of possible trades for both nodes i and j
        HashSet<Integer> possibleTrades_i = new HashSet<>(this.bidirectionalAdjacencyList.getAdjacencyListLeft()[i].size(), 1);
        HashSet<Integer> possibleTrades_j = new HashSet<>(this.bidirectionalAdjacencyList.getAdjacencyListLeft()[j].size(), 1);
        getPossibleTradesWithSelfLoop(i, j, possibleTrades_i, possibleTrades_j);
        // computes curveballAndCooccurrence between nodes i and j
        int cooccurrence = this.bidirectionalAdjacencyList.getAdjacencyListLeft()[i].size() - possibleTrades_i.size();
        // only performs trade when there is something to trade
        if (!possibleTrades_i.isEmpty() && !possibleTrades_j.isEmpty()) {
            trade(i, j, possibleTrades_i, possibleTrades_j);
        }
        return cooccurrence;
    }

    /**
     * In place method to trade neighbors from node i and node j
     * Cannot create self-loops
     * (but if self-loops are included in the graph from the start, then they will remain in the graph)
     * <p>
     * - bipartite graphs (will never create self-loop)
     * - directed graphs (will never create self-loop)
     *
     * @param i: the index of the first node involved in the trading
     * @param j: the index of the second node involved in the trading
     * @return curveballAndCooccurrence between nodes i and j
     */
    public int cooccurrenceWhileTradeWithoutSelfLoop(int i, int j) {
        // allocation of 2 new sets: Pi and Pj, the sets of possible trades for both nodes i and j
        HashSet<Integer> possibleTrades_i = new HashSet<>(this.bidirectionalAdjacencyList.getAdjacencyListLeft()[i].size(), 1);
        HashSet<Integer> possibleTrades_j = new HashSet<>(this.bidirectionalAdjacencyList.getAdjacencyListLeft()[j].size(), 1);
        getPossibleTradesWithoutSelfLoop(i, j, possibleTrades_i, possibleTrades_j);
        // computes curveballAndCooccurrence between nodes i and j
        int cooccurrence = bidirectionalAdjacencyList.getAdjacencyListLeft()[i].size() - possibleTrades_i.size();
        if (bidirectionalAdjacencyList.getAdjacencyListLeft()[i].contains(j)) {
            cooccurrence--;
        }
        // only performs trade when there is something to trade
        if (!possibleTrades_i.isEmpty() && !possibleTrades_j.isEmpty()) {
            trade(i, j, possibleTrades_i, possibleTrades_j);
        }
        return cooccurrence;
    }

    /**
     * In place method for preparing the set of neighbors that can be traded for both nodes i and j
     * Allowing self loops: possibleTrade_i can contain j, possibleTrade_j can contain i
     *
     * @param i:                index of node i
     * @param j:                index of node j
     * @param possibleTrades_i: a reference to the set of neighbors of node i that can be traded
     * @param possibleTrades_j: a reference to the set of neighbors of node j that can be traded
     */
    protected void getPossibleTradesWithSelfLoop(int i, int j, HashSet<Integer> possibleTrades_i, HashSet<Integer> possibleTrades_j) {
        possibleTrades_i.addAll(this.bidirectionalAdjacencyList.getAdjacencyListLeft()[i]);
        possibleTrades_j.addAll(this.bidirectionalAdjacencyList.getAdjacencyListLeft()[j]);
        // get elements available for trading: set difference Pi = Ni \ Nj and Pj = Nj \ Ni
        possibleTrades_i.removeAll(this.bidirectionalAdjacencyList.getAdjacencyListLeft()[j]);
        possibleTrades_j.removeAll(this.bidirectionalAdjacencyList.getAdjacencyListLeft()[i]);
    }


    /**
     * In place method for preparing the set of neighbors that can be traded for both nodes i and j (both nodes belonging to the right side)
     * Allowing self loops: possibleTrade_i can contain j, possibleTrade_j can contain i
     *
     * @param i:                index of node i
     * @param j:                index of node j
     * @param possibleTrades_i: a reference to the set of neighbors of node i that can be traded
     * @param possibleTrades_j: a reference to the set of neighbors of node j that can be traded
     */
    protected void getPossibleTradesRightWithSelfLoop(int i, int j, HashSet<Integer> possibleTrades_i, HashSet<Integer> possibleTrades_j) {
        possibleTrades_i.addAll(this.bidirectionalAdjacencyList.getAdjacencyListRight()[i]);
        possibleTrades_j.addAll(this.bidirectionalAdjacencyList.getAdjacencyListRight()[j]);
        // get elements available for trading: set difference Pi = Ni \ Nj and Pj = Nj \ Ni
        possibleTrades_i.removeAll(this.bidirectionalAdjacencyList.getAdjacencyListRight()[j]);
        possibleTrades_j.removeAll(this.bidirectionalAdjacencyList.getAdjacencyListRight()[i]);
    }

    /**
     * In place method for preparing the set of neighbors that can be traded for both nodes i and j
     * Not allowing self loops: possibleTrade_i should not contain j, possibleTrade_j should not contain i
     *
     * @param i:                index of node i
     * @param j:                index of node j
     * @param possibleTrades_i: a reference to the set of neighbors of node i that can be traded
     * @param possibleTrades_j: a reference to the set of neighbors of node j that can be traded
     */
    protected void getPossibleTradesWithoutSelfLoop(int i, int j, HashSet<Integer> possibleTrades_i, HashSet<Integer> possibleTrades_j) {
        getPossibleTradesWithSelfLoop(i, j, possibleTrades_i, possibleTrades_j);
        possibleTrades_i.remove(j);
        possibleTrades_j.remove(i);
    }

    /**
     * In place method for trading neighbors between node i and node j
     *
     * @param i                index of node i
     * @param j                index of node j
     * @param possibleTrades_i a set of neighbors that node i can possibly trade with node j
     * @param possibleTrades_j a set of neighbors that node j can possibly trade with node i
     */
    public void trade(int i, int j, HashSet<Integer> possibleTrades_i, HashSet<Integer> possibleTrades_j) {
        // compute how many elements node i can tradeWithSelfLoop
        int nbTrades_i = possibleTrades_i.size();
        // remove the elements available for trading from the original sets
        for (int n : possibleTrades_i) {
            this.bidirectionalAdjacencyList.removeEdge(i, n);
        }
        for (int m : possibleTrades_j) {
            this.bidirectionalAdjacencyList.removeEdge(j, m);
        }
        // get all possible trades in a single set: in place union: P = Pi U Pj
        ArrayList<Integer> possibleTrades = new ArrayList<>(getAllPossibleTrades(possibleTrades_i, possibleTrades_j));
        // shuffle the possible trades
        Collections.shuffle(possibleTrades, this.rnd);
        // add traded elements to original neighborhood sets Ni and Nj
        for (int k = 0; k < nbTrades_i; k++) {
            this.bidirectionalAdjacencyList.addEdge(i, possibleTrades.get(k));
        }
        for (int k = nbTrades_i; k < possibleTrades.size(); k++) {
            this.bidirectionalAdjacencyList.addEdge(j, possibleTrades.get(k));
        }
    }


    /**
     * In place method for trading neighbors between node i and node j both belonging to the right side of the bipartite graph
     *
     * @param i                index of node i (i belonging to the right side)
     * @param j                index of node j (j belonging to the right side)
     * @param possibleTrades_i a set of neighbors that node i can possibly trade with node j
     * @param possibleTrades_j a set of neighbors that node j can possibly trade with node i
     */
    public void tradeRight(int i, int j, HashSet<Integer> possibleTrades_i, HashSet<Integer> possibleTrades_j) {
        // compute how many elements node i can tradeWithSelfLoop
        int nbTrades_i = possibleTrades_i.size();
        // remove the elements available for trading from the original sets
        for (int n : possibleTrades_i) {
            this.bidirectionalAdjacencyList.removeEdge(n, i);
        }
        for (int m : possibleTrades_j) {
            this.bidirectionalAdjacencyList.removeEdge(m, j);
        }
        // get all possible trades in a single set: in place union: P = Pi U Pj
        ArrayList<Integer> possibleTrades = new ArrayList<>(getAllPossibleTrades(possibleTrades_i, possibleTrades_j));
        // shuffle the possible trades
        Collections.shuffle(possibleTrades, this.rnd);
        // add traded elements to original neighborhood sets Ni and Nj
        for (int k = 0; k < nbTrades_i; k++) {
            this.bidirectionalAdjacencyList.addEdge(possibleTrades.get(k), i);
        }
        for (int k = nbTrades_i; k < possibleTrades.size(); k++) {
            this.bidirectionalAdjacencyList.addEdge(possibleTrades.get(k), j);
        }
    }



    /**
     * Return union of both set of neighbors that i and j can possibly trade
     *
     * @param possibleTrades_i
     * @param possibleTrades_j
     * @return
     */
    protected HashSet<Integer> getAllPossibleTrades(HashSet<Integer> possibleTrades_i, HashSet<Integer> possibleTrades_j) {
        HashSet<Integer> possibleTrades = new HashSet<>();
        possibleTrades.addAll(possibleTrades_i);
        possibleTrades.addAll(possibleTrades_j);
        return possibleTrades;
    }


    public HashMap<String, Integer> tradeUpdateCoocc(int i, int j, HashSet<Integer> possibleTrades_i, HashSet<Integer> possibleTrades_j) {
        HashMap<String, Integer> cooccUpdate = new HashMap<>();
        // compute how many elements node i can tradeWithSelfLoop
        int nbTrades_i = possibleTrades_i.size();
        // get all possible trades in a single set: in place union: P = Pi U Pj
        ArrayList<Integer> possibleTrades = new ArrayList<>(getAllPossibleTrades(possibleTrades_i, possibleTrades_j));

        // shuffle the possible trades
        Collections.shuffle(Arrays.asList(possibleTrades), this.rnd);
        // update links and corresponding curveballAndCooccurrence
        for (int k = 0; k < nbTrades_i; k++) {
            // keep the same number of neighbors for i (e.g. keep the same degree)
            int node = possibleTrades.get(k);
            if (possibleTrades_j.contains(node)) {
                // node was a neighbor of j and now becomes a neighbor of i (traded)
                this.bidirectionalAdjacencyList.removeEdge(j, node);
                this.bidirectionalAdjacencyList.addEdge(i, node);
                update(j, i, node, cooccUpdate);
            }
        }
        for (int k = nbTrades_i; k < possibleTrades.size(); k++) {
            // keep the same number of neighbors for j (e.g. keep the same degree)
            int node = possibleTrades.get(k);
            if (possibleTrades_i.contains(node)) {
                // node was a neighbor of i and now becomes a neighbor of j (traded)
                this.bidirectionalAdjacencyList.removeEdge(i, node);
                this.bidirectionalAdjacencyList.addEdge(j, node);
                update(i, j, node, cooccUpdate);
            }
        }

        return cooccUpdate;
    }

    /**
     * Updating curveballAndCooccurrence.
     *
     * Consider a bipartite graphs with nodes i and j on the left side and k on the right,
     * where k is being traded from i to j.
     *
     * This means that edge (i,k) becomes now (j,k).
     *
     * Cooccurrence needs to be updated like this.
     *
     * For all neighbors n of k (on the left side) that are different from i:
     *   decrease coocc(i,n)
     *   increase coocc(j,n)
     *
     *
     *
     * @param i: index of first node on the left side
     * @param j: index of second node on the right side
     * @param k: index of the traded node of the right side
     * @param cooccUpdate: the datastructure to keep track of the cooccurrences updates
     */
    protected void update(int i, int j, int k, HashMap<String, Integer> cooccUpdate) {
        for (int n : this.bidirectionalAdjacencyList.getAdjacencyListRight()[k]) {
            if (n != i) {
                increaseCoocc(i, n, cooccUpdate);
                decreaseCoocc(j, n, cooccUpdate);
            }
        }
    }

    /**
     * Increase curveballAndCooccurrence coocc(i,j)
     *
     * @param i
     * @param j
     * @param cooccUpdate
     */
    protected void increaseCoocc(int i, int j, HashMap<String, Integer> cooccUpdate) {
        // keep key in increasing order
        String key = (i < j) ? i + " " + j : j + " " + i;
        // increase coocc()
        cooccUpdate.putIfAbsent(key, 0);
        cooccUpdate.put(key, cooccUpdate.get(key) + 1);
    }

    /**
     * Decrease curveballAndCooccurrence coocc(i,j)
     *
     * @param i
     * @param j
     * @param cooccUpdate
     */
    protected void decreaseCoocc(int i, int j, HashMap<String, Integer> cooccUpdate) {
        // keep key in increasing order
        String key = (i < j) ? i + " " + j : j + " " + i;
        // increase coocc()
        cooccUpdate.putIfAbsent(key, 0);
        cooccUpdate.put(key, cooccUpdate.get(key) - 1);
    }

    public BidirectionalAdjacencyList getBidirectionalAdjacencyList() {
        return bidirectionalAdjacencyList;
    }

    public Random getRnd() {
        return rnd;
    }
}
