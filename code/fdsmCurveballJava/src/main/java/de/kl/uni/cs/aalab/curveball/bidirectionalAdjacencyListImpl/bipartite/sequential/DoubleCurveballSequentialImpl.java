package de.kl.uni.cs.aalab.curveball.bidirectionalAdjacencyListImpl.bipartite.sequential;

import de.kl.uni.cs.aalab.curveball.Curveball;
import de.kl.uni.cs.aalab.curveball.bidirectionalAdjacencyListImpl.BidirectionalAdjacencyListImpl;
import de.kl.uni.cs.aalab.datastructure.BidirectionalAdjacencyList;
import de.kl.uni.cs.aalab.utilities.Shuffle;

import java.util.Random;

public class DoubleCurveballSequentialImpl extends BidirectionalAdjacencyListImpl implements Curveball {

    private int[] nodesIndicesLeft;
    private int[] nodesIndicesRight;

    /**
     * Constructor
     *
     * @param nodesIndicesLeft           : a reference to the list of nodes indices from the left side
     * @param nodesIndicesRight          : a reference to the list of nodes indices from the right side
     * @param bidirectionalAdjacencyList : a reference to the bidirectional list of neighborhood sets
     * @param rnd                        : a reference to the random generator
     */
    public DoubleCurveballSequentialImpl(int[] nodesIndicesLeft, int[] nodesIndicesRight, BidirectionalAdjacencyList bidirectionalAdjacencyList, Random rnd) {
        super(bidirectionalAdjacencyList, rnd);
        this.nodesIndicesLeft = nodesIndicesLeft;
        this.nodesIndicesRight = nodesIndicesRight;
    }

    @Override
    public void step() {
        // classical step using nodes from the left side
        Shuffle.shuffleArray(this.nodesIndicesLeft, this.rnd);
        for (int i = 0; i < this.nodesIndicesLeft.length - 1; i = i + 2) {
            this.tradeWithSelfLoop(this.nodesIndicesLeft[i], this.nodesIndicesLeft[i + 1]);
        }
        // step using nodes from the right side
        Shuffle.shuffleArray(this.nodesIndicesRight, this.rnd);
        for (int i = 0; i < this.nodesIndicesRight.length - 1; i = i + 2) {
            this.tradeRightWithSelfLoop(this.nodesIndicesRight[i], this.nodesIndicesRight[i + 1]);
        }
    }
}
