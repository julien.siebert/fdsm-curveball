package de.kl.uni.cs.aalab.curveball.sparseAdjacencyMatrixImpl;

import com.zaxxer.sparsebits.SparseBitSet;

import java.util.ArrayList;
import java.util.Random;

/**
 * Parent class the for all SParse Adjacency Matrix based implementations of the Global Curveball algorithm
 */
public class SparseAdjacencyMatrixImpl {


    protected int[] nodesIndices;
    protected ArrayList<SparseBitSet> adjacencyMatrix;
    protected Random rnd;

    /**
     * Constructor
     *
     * @param nodesIndices:    a list of nodes indices
     * @param adjacencyMatrix: a list of bitset representing rows of the adjacency matrix
     * @param rnd:             a random generator
     */
    public SparseAdjacencyMatrixImpl(int[] nodesIndices, ArrayList<SparseBitSet> adjacencyMatrix, Random rnd) {
        this.adjacencyMatrix = adjacencyMatrix;
        this.nodesIndices = nodesIndices;
        this.rnd = rnd;
    }

    /**
     * Splits randomly a given SparseBitSet B0 in two different SparseBitSets: B1 and B2.
     * <p>
     * The first SparseBitSet B1 contains `nbOnesInFirst` ones, picked uniformly at random from the given one B0.
     *
     * @param toSplit:       the SparseBitSet to split (aka B0)
     * @param nbOnesInFirst: the number of ones that the first SparseBitSet B1 needs to contain
     */
    protected SparseBitSet[] randomSplit(SparseBitSet toSplit, int nbOnesInFirst) {

        SparseBitSet first = new SparseBitSet(toSplit.length());
        SparseBitSet second = new SparseBitSet(toSplit.length());

        int nbOnesTotal = toSplit.cardinality();

        for (int i = toSplit.nextSetBit(0); i >= 0; i = toSplit.nextSetBit(i + 1)) {
            int k = this.rnd.nextInt(nbOnesTotal);
            if (k < nbOnesInFirst) {
                first.set(i);
                nbOnesInFirst -= 1;
            } else {
                second.set(i);
            }
            nbOnesTotal -= 1;
        }

        SparseBitSet[] res = new SparseBitSet[2];
        res[0] = first;
        res[1] = second;
        return res;
    }

    /**
     * In place method to trade neighbors from node i and node j
     * Can create self-loops!
     * <ul>
     * <li>bipartite graphs (will never create self-loop)</li>
     * <li>directed graphs (can create self-loops)</li>
     * </ul>
     *
     * @param indexFirstRow:  the index of the first node involved in the trading
     * @param indexSecondRow: the index of the second node involved in the trading
     */
    public void tradeWithSelfLoop(int indexFirstRow, int indexSecondRow) {
        SparseBitSet matrixRowI = this.adjacencyMatrix.get(indexFirstRow);
        SparseBitSet matrixRowJ = this.adjacencyMatrix.get(indexSecondRow);
        // create a mask
        SparseBitSet mask = matrixRowI.clone();
        mask.xor(matrixRowJ);
        // count the number of elements that can be trades for both nodes and set the trading elements to zero
        int nbTradesFirst = 0;
        for (int i = mask.nextSetBit(0); i >= 0; i = mask.nextSetBit(i + 1)) {
            if (matrixRowI.get(i)) {
                nbTradesFirst += 1;
                matrixRowI.clear(i);
            } else {
                matrixRowJ.clear(i);
            }
        }

        SparseBitSet[] res = this.randomSplit(mask, nbTradesFirst);

        // update both matrix row
        matrixRowI.or(res[0]);
        matrixRowJ.or(res[1]);
    }

    public int[] getNodesIndices() {
        return nodesIndices;
    }

    public ArrayList<SparseBitSet> getAdjacencyMatrix() {
        return adjacencyMatrix;
    }

    public Random getRnd() {
        return rnd;
    }
}
