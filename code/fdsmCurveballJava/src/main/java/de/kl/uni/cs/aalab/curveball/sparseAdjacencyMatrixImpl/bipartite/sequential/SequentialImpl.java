package de.kl.uni.cs.aalab.curveball.sparseAdjacencyMatrixImpl.bipartite.sequential;

import com.zaxxer.sparsebits.SparseBitSet;
import de.kl.uni.cs.aalab.curveball.Curveball;
import de.kl.uni.cs.aalab.curveball.sparseAdjacencyMatrixImpl.SparseAdjacencyMatrixImpl;
import de.kl.uni.cs.aalab.utilities.Shuffle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Sequential Implementation of the Global Curveball Algorithm
 * Using Sparse Adjacency Matrix
 * Allowing Self-Loops (e.g., for bipartite graphs or directed graphs with self-loops)
 */
public class SequentialImpl extends SparseAdjacencyMatrixImpl implements Curveball {
    /**
     * Constructor
     *
     * @param nodesIndices    : a reference to the list of nodes indices
     * @param adjacencyMatrix : a reference to the  list of bitset representing rows of the adjacency matrix
     * @param rnd             : a reference to the random generator
     */
    public SequentialImpl(int[] nodesIndices, ArrayList<SparseBitSet> adjacencyMatrix, Random rnd) {
        super(nodesIndices, adjacencyMatrix, rnd);
    }

    @Override
    public void step() {
        Shuffle.shuffleArray(this.nodesIndices, this.rnd);
        for (int i = 0; i < this.nodesIndices.length -1; i = i + 2) {
            this.tradeWithSelfLoop(this.nodesIndices[i], this.nodesIndices[i+1]);
        }
    }
}
