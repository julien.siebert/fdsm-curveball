package de.kl.uni.cs.aalab.curveball.sparseAdjacencyMatrixImpl.bipartite.thread;

import com.zaxxer.sparsebits.SparseBitSet;
import de.kl.uni.cs.aalab.curveball.sparseAdjacencyMatrixImpl.SparseAdjacencyMatrixImpl;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Callable;

/**
 * Trading methods that can be called in thread by threads
 */
public class ParallelTradingTask extends SparseAdjacencyMatrixImpl implements Callable<Void> {

    private int startIndex;
    private int endIndex;

    /**
     * Constructor
     *  @param nodesIndices :    a reference to the list of all nodes indices
     * @param adjacencyMatrix : a reference to the list of all matrix rows
     * @param rnd :             a reference to an instance of a random generator
     * @param startIndex :      the index where to start the trading (inclusive)
     * @param endIndex :        the index where to stop the trading (exclusive)
     */
    public ParallelTradingTask(int[] nodesIndices, ArrayList<SparseBitSet> adjacencyMatrix, Random rnd, int startIndex, int endIndex) {
        super(nodesIndices, adjacencyMatrix, rnd);
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }

    protected int getStartIndex() {
        return this.startIndex;
    }

    protected int getEndIndex() {
        return this.endIndex;
    }


    /**
     * Runs one step of the Global Curveball algorithm on a subset of the adjacency list
     *
     * @throws Exception if unable to compute a result
     */
    @Override
    public Void call() {
        for (int i = this.startIndex; i < this.endIndex - 1; i = i + 2) {
            this.tradeWithSelfLoop(this.nodesIndices[i], this.nodesIndices[i + 1]);
        }
        return null;
    }
}
