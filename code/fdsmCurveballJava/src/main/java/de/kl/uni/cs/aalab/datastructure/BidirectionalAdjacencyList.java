package de.kl.uni.cs.aalab.datastructure;

import java.util.HashSet;

public class BidirectionalAdjacencyList {

    private HashSet<Integer>[] adjacencyListLeft;
    private HashSet<Integer>[] adjacencyListRight;

    /**
     * Creates an empty bidirectional adjacency list
     * Allocates two lists of sets (adjacency list left and adjacency list right)
     * Alocates all empty sets
     *
     * @param nLeft:  number of nodes on the left side
     * @param nRight: number of nodes on the right side
     */
    public BidirectionalAdjacencyList(int nLeft, int nRight) {
        this.adjacencyListLeft = new HashSet[nLeft];
        this.adjacencyListRight = new HashSet[nRight];
        for (int i = 0; i < nLeft; i++) {
            this.adjacencyListLeft[i] = new HashSet<Integer>();
        }
        for (int i = 0; i < nRight; i++) {
            this.adjacencyListRight[i] = new HashSet<Integer>();
        }
    }

    /**
     * Creates a bidirectional adjacency list from an existing adjacency list (! swallow copy !)
     * Allocates two lists of sets (adjacency list left and adjacency list right)
     * Alocates all empty sets
     *
     * @param adjacencyListLeft : the existing adjacency list (left side)
     * @param nRight            :            number of nodes on the right side
     */
    public BidirectionalAdjacencyList(HashSet<Integer>[] adjacencyListLeft, int nRight) {
        this.adjacencyListLeft = adjacencyListLeft;
        this.adjacencyListRight = new HashSet[nRight];
        for (int i = 0; i < nRight; i++) {
            this.adjacencyListRight[i] = new HashSet<Integer>();
        }
        // init adjacency list right side using the existing adjacency list of the left side
        for (int i = 0; i < this.adjacencyListLeft.length; i++) {
            for (int j : this.adjacencyListLeft[i]) {
                this.adjacencyListRight[j].add(i);
            }
        }
    }

    public void addEdge(int indexNodeLeft, int indexNodeRight) {
        this.adjacencyListLeft[indexNodeLeft].add(indexNodeRight);
        this.adjacencyListRight[indexNodeRight].add(indexNodeLeft);
    }

    public void removeEdge(int indexNodeLeft, int indexNodeRight) {
        this.adjacencyListLeft[indexNodeLeft].remove(indexNodeRight);
        this.adjacencyListRight[indexNodeRight].remove(indexNodeLeft);
    }

    /**
     * Given two edges: e1 = (l1, r1) and e2 = (l2, r2)
     * A swap means that:
     * e1 = (l1, r2)
     * e2 = (l2, r1)
     *
     * @param indexNodeLeft1:  index of node l1
     * @param indexNodeRight1: index of node r1
     * @param indexNodeLeft2:  index of node l2
     * @param indexNodeRight2: index of node r2
     * @TODO Synchronized?
     */
    public void swapEdges(int indexNodeLeft1, int indexNodeRight1, int indexNodeLeft2, int indexNodeRight2) {
        if (this.adjacencyListLeft[indexNodeLeft1].contains(indexNodeRight1) &&
                this.adjacencyListLeft[indexNodeLeft2].contains(indexNodeRight2) &&
                this.adjacencyListRight[indexNodeRight1].contains(indexNodeLeft1) &&
                this.adjacencyListRight[indexNodeRight2].contains(indexNodeLeft2)) {
            // swapping in adjacency list left
            this.adjacencyListLeft[indexNodeLeft1].remove(indexNodeRight1);
            this.adjacencyListLeft[indexNodeLeft1].add(indexNodeRight2);

            this.adjacencyListLeft[indexNodeLeft2].remove(indexNodeRight2);
            this.adjacencyListLeft[indexNodeLeft2].add(indexNodeRight1);

            // swapping in adjacency list right
            this.adjacencyListRight[indexNodeRight1].remove(indexNodeLeft1);
            this.adjacencyListRight[indexNodeRight1].add(indexNodeLeft2);

            this.adjacencyListRight[indexNodeRight2].remove(indexNodeLeft2);
            this.adjacencyListRight[indexNodeRight2].add(indexNodeLeft1);
        }
    }

    public HashSet<Integer>[] getAdjacencyListRight() {
        return adjacencyListRight;
    }


    public HashSet<Integer>[] getAdjacencyListLeft() {
        return adjacencyListLeft;
    }


}
