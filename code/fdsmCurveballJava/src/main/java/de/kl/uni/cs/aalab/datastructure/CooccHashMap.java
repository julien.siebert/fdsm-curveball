package de.kl.uni.cs.aalab.datastructure;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Represents a cooccurrence matrix where only non zero values of coocc(u,v) are stored
 * That is coocc(u,v) = 0 are not stored
 */
public class CooccHashMap {


    private HashMap<Key, Integer> coocc;

    public CooccHashMap() {
        coocc = new HashMap<>();
    }

    public Set<Map.Entry<Key, Integer>> entrySet(){
        return coocc.entrySet();
    }


    /**
     * Increments coocc(U,V)
     * <p>
     * Takes care of index order (i < j)
     *
     * @param i: index of first node U
     * @param j: index of second node V
     * @param inc: the increment
     */
    public void increment(int i, int j, int inc) {
        Key key = new Key(i, j);
        increment(key, inc);
    }

    public void increment(Key key, Integer inc) {
        int value = this.coocc.getOrDefault(key, 0);
        this.coocc.put(key, value + inc);
    }


    public int get(int i, int j) {
        return this.coocc.get(new Key(i, j));
    }

    public class Key {

        private final int x;
        private final int y;

        public Key(int x, int y) {
            if (x < y) {
                this.x = x;
                this.y = y;
            } else {
                this.x = y;
                this.y = x;
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Key)) return false;
            Key key = (Key) o;
            return this.x == key.x && this.y == key.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.x, this.y);
        }

        @Override
        public String toString(){
            return this.x + " " + this.y;
        }
    }
}
