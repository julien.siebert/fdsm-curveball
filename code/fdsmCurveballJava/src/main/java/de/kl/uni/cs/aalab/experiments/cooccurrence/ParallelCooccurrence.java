package de.kl.uni.cs.aalab.experiments.cooccurrence;

import de.kl.uni.cs.aalab.cooccurence.Cooccurrence;
import de.kl.uni.cs.aalab.cooccurence.adjacencyListImpl.thread.ThreadImpl;
import de.kl.uni.cs.aalab.utilities.LoadAdjacencyListFile;

import java.io.IOException;
import java.util.HashSet;

import static de.kl.uni.cs.aalab.utilities.SaveMatrixToFile.save;

public class ParallelCooccurrence {

    private static String usage = "This programs computes the cooccurrence in parallel using threads\n" +
            "It first loads an adjacency list of a bipartite graph from a file\n" +
            "then conputes the coocc(u,v) for u and v belonging to the left side of the\n" +
            "finally saves the cooccurrence to a file\n" +
            "\nUSAGE:\n" +
            "This program requires 5 command line arguments, namely\n" +
            "the name of the file to load (adjacency list)\n" +
            "the number of rows in the adjacency list\n" +
            "the number of nodes on the right side\n" +
            "the minimum index (usually 1)\n" +
            "the name of the file to save the cooccurrence\n" +
            "the number of threads\n";


    public static void main(String[] args) {

        if (args.length != 6) {
            System.err.println(usage);
            System.exit(1);
        }

        String fileName = args[0];
        int nl = Integer.parseInt(args[1]);
        int nr = Integer.parseInt(args[2]);
        int indexMinimum = Integer.parseInt(args[3]);
        String outputFilename = args[4];
        int nbThreads = Integer.parseInt(args[5]);

        LoadAdjacencyListFile loader = new LoadAdjacencyListFile();


        long start = System.currentTimeMillis();
        try {
            HashSet<Integer>[] adjacencyList = loader.load(fileName, nl, indexMinimum);
            ThreadImpl cooccurrence = new ThreadImpl(nbThreads, Cooccurrence.convert(adjacencyList, nl, nr));
            save(cooccurrence.coocc(), outputFilename);
            cooccurrence.shutdown();
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }


        long end = System.currentTimeMillis();

        System.out.println("Time elapsed: " + (end - start) + " ms");
    }


}
