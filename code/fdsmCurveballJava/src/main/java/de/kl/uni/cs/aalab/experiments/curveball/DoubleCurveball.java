package de.kl.uni.cs.aalab.experiments.curveball;

import de.kl.uni.cs.aalab.curveball.bidirectionalAdjacencyListImpl.bipartite.sequential.DoubleCurveballSequentialImpl;
import de.kl.uni.cs.aalab.datastructure.BidirectionalAdjacencyList;
import de.kl.uni.cs.aalab.utilities.LoadAdjacencyListFile;

import java.io.IOException;
import java.util.HashSet;
import java.util.Random;

import static de.kl.uni.cs.aalab.similarity.adjacencyListImpl.PerturbationScore.computeScore;

public class DoubleCurveball {

    private static String usage = "This program runs s steps of the double global curveball and for each computes the perturbation score with the original graph\n" +
            "\nUSAGE:\n" +
            "This program requires the following command line arguments, namely\n" +
            "the name of the file to load\n" +
            "the number of nodes in the left side of the bipartite graph\n" +
            "the number of nodes in the right side of the bipartite graph\n" +
            "the minimum index (usually 1)\n" +
            "the number of steps to run \n" +
            "the random seed";

    public static void main(String[] args) {
        if (args.length != 6) {
            System.err.println(usage);
            System.exit(1);
        }
        String fileName = args[0];
        int nbNodesLeft = Integer.parseInt(args[1]);
        int nbNodesRight = Integer.parseInt(args[2]);
        int indexMinimum = Integer.parseInt(args[3]);
        int nbSteps = Integer.parseInt(args[4]);
        long seed = Long.parseLong(args[5]);
        long start = System.currentTimeMillis();
        LoadAdjacencyListFile loader = new LoadAdjacencyListFile();


        try {
            HashSet<Integer>[] adjacencyList = loader.load(fileName, nbNodesLeft, indexMinimum);
            // copying the adjacency list
            HashSet<Integer>[] originalAdjacencyList = new HashSet[adjacencyList.length];
            for (int i = 0; i < adjacencyList.length; i++) {
                originalAdjacencyList[i] = new HashSet<Integer>(adjacencyList[i]);
            }
            int[] nodesIndicesLeft = new int[nbNodesLeft];
            for (int i = 0; i < nbNodesLeft; i++) {
                nodesIndicesLeft[i] = i;
            }
            int[] nodesIndicesRight = new int[nbNodesRight];
            for (int i = 0; i < nbNodesRight; i++) {
                nodesIndicesRight[i] = i;
            }
            Random rnd = new Random(seed);
            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, nbNodesRight);
            DoubleCurveballSequentialImpl curveball = new DoubleCurveballSequentialImpl(nodesIndicesLeft, nodesIndicesRight, bidirectionalAdjacencyList, rnd);

            float perturbationScore = 0.f;
            // running Double Global Curveball algorithm
            for (int i = 0; i < nbSteps; i++) {
                curveball.step();
                perturbationScore = computeScore(originalAdjacencyList, curveball.getBidirectionalAdjacencyList().getAdjacencyListLeft());
                System.out.println(i + " " + perturbationScore);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        System.exit(0);
    }
}
