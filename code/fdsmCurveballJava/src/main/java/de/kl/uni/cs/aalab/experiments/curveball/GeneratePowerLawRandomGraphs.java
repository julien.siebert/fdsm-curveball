package de.kl.uni.cs.aalab.experiments.curveball;


import de.kl.uni.cs.aalab.curveball.adjacencyListImpl.bipartite.thread.ThreadImpl;
import de.kl.uni.cs.aalab.utilities.LoadAdjacencyListFile;

import java.io.IOException;
import java.util.HashSet;
import java.util.Random;

import static de.kl.uni.cs.aalab.utilities.GraphGenerator.powerLaw;

/**
 * Generates random bipartite graphs following a powerlaw distribution
 */
public class GeneratePowerLawRandomGraphs {

    private static String usage = "This program first generate a dummy bipartite graph following with a power law degree distribution, then calls the global curveball algorithm to generate random graphs from that degree distribution \n" +
            "USAGE:\n" +
            "This program requires 6 command line arguments, namely\n" +
            "base\n" +
            "exponent\n" +
            "the random seed\n" +
            "number of threads\n" +
            "number of random graphs to generate\n" +
            "number of steps of the curveball algorithm before generating a new random graph\n";


    public static void main(String[] args){
        // checks arguments
        if (args.length != 6) {
            System.err.println(usage);
            System.exit(1);
        }

        // parse arguments
        int base = Integer.parseInt(args[0]);
        int exponent = Integer.parseInt(args[1]);
        long seed = Long.parseLong(args[2]);
        int nbThreads = Integer.parseInt(args[3]);
        int nbGraphs = Integer.parseInt(args[4]);
        int nbSteps = Integer.parseInt(args[5]);

        // generate adjacency matrix
        HashSet<Integer>[] adjacencyList = powerLaw(base,exponent);
        int[] nodesIndices = new int[adjacencyList.length];
        for (int i = 0; i < adjacencyList.length; i++) {
            nodesIndices[i] = i;
        }
        Random rnd = new Random(seed);

        ThreadImpl curveball = new ThreadImpl(nbThreads, nodesIndices, adjacencyList, rnd);

        for (int g = 0; g < nbGraphs; g++) {
            for (int s = 0; s < nbSteps; s++) {
                try {
                    curveball.step();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    curveball.shutdown();
                    System.exit(1);
                }

                // save
                try {
                    LoadAdjacencyListFile.save(adjacencyList, String.format("adj_list_graph%d.csv", g), ' ');
                } catch (IOException e) {
                    e.printStackTrace();
                    curveball.shutdown();
                    System.exit(1);
                }
            }
        }

        curveball.shutdown();
    }
}
