package de.kl.uni.cs.aalab.experiments.curveball;

import de.kl.uni.cs.aalab.curveball.adjacencyListImpl.bipartite.sequential.SequentialImpl;
import de.kl.uni.cs.aalab.utilities.LoadAdjacencyListFile;

import java.io.IOException;
import java.util.HashSet;
import java.util.Random;

import static de.kl.uni.cs.aalab.similarity.adjacencyListImpl.PerturbationScore.computeScore;

public class PerturbationScore {

    private static String usage = "This program runs s steps of the global curveball and for each computes the perturbation score with the original graph\n" +
            "\nUSAGE:\n" +
            "This program requires the following command line arguments, namely\n" +
            "the name of the file to load\n" +
            "the number of nodes in the left side of the bipartite graph\n" +
            "the minimum index (usually 1)\n" +
            "the number of steps to run \n" +
            "the random seed";

    public static void main(String[] args) {
        if (args.length != 5) {
            System.err.println(usage);
            System.exit(1);
        }
        String fileName = args[0];
        int nbNodesLeft = Integer.parseInt(args[1]);
        int indexMinimum = Integer.parseInt(args[2]);
        int nbSteps = Integer.parseInt(args[3]);
        long seed = Long.parseLong(args[4]);
        long start = System.currentTimeMillis();
        LoadAdjacencyListFile loader = new LoadAdjacencyListFile();


        try {
            HashSet<Integer>[] adjacencyList = loader.load(fileName, nbNodesLeft, indexMinimum);
            // copying the adjacency list
            HashSet<Integer>[] originalAdjacencyList = new HashSet[adjacencyList.length];
            for (int i = 0; i < adjacencyList.length; i++) {
                originalAdjacencyList[i] = new HashSet<Integer>(adjacencyList[i]);
            }
            int[] nodesIndicesLeft = new int[nbNodesLeft];
            for (int i = 0; i < nbNodesLeft; i++) {
                nodesIndicesLeft[i] = i;
            }
            Random rnd = new Random(seed);
            SequentialImpl curveball = new SequentialImpl(nodesIndicesLeft, adjacencyList, rnd);

            float perturbationScore = 0.f;
            // running Double Global Curveball algorithm
            for (int i = 0; i < nbSteps; i++) {
                curveball.step();
                perturbationScore = computeScore(originalAdjacencyList, curveball.getAdjacencyList());
                System.out.println(i + " " + perturbationScore);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        System.exit(0);
    }
}
