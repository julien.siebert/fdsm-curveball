package de.kl.uni.cs.aalab.experiments.curveballAndCooccurrence;

import de.kl.uni.cs.aalab.cooccurence.adjacencyListImpl.sequential.Sequential;
import de.kl.uni.cs.aalab.curveball.adjacencyListImpl.bipartite.thread.ThreadImpl;
import de.kl.uni.cs.aalab.utilities.LoadAdjacencyListFile;
import de.kl.uni.cs.aalab.utilities.SaveMatrixToFile;

import java.io.IOException;
import java.util.HashSet;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This program loads a graph from an adjacency file,
 * creates an adjacency list,
 * generates a certain number of random graphs,
 * that is, for each graph, it runs the curveball algorithm for a certain number of steps
 * for each generated graph, it computes the curveballAndCooccurrence matrix (! O(n^3)),
 * saves the curveballAndCooccurrence matrix,
 * and saves the adjacency list.
 * <p>
 * Along the way it logs the time taken (for profiling purposes)
 */
public class StatsCooccurrence {

    private final static Logger LOGGER = Logger.getLogger(StatsCooccurrence.class.getName());

    private static String usage = "This program computes the curveballAndCooccurrence matrices for randomly generated bipartite graphs\n" +
            "!!! WARNING !!!\n" +
            "This program computes the curveballAndCooccurrence of several random graphs\n" +
            "The complexity is > O(n^3)\n" +
            "This can REALLY takes a long time\n" +
            "!!! WARNING !!!\n\nUSAGE:\n" +
            "This program requires 7 command line arguments, namely\n" +
            "the name of the file to load\n" +
            "the number of nodes in the left side of the bipartite graph\n" +
            "the number of nodes in the right side of the bipartite graph\n" +
            "the minimum index (usually 1)\n" +
            "the number of steps to run before computing curveballAndCooccurrence (usually 20)\n" +
            "the number of graphs to generate (usually 100)\n" +
            "the random seed";


    public static void main(String[] args) {
        if (args.length != 7) {
            System.err.println(usage);
            System.exit(1);
        }
        String fileName = args[0];
        int nbNodesLeft = Integer.parseInt(args[1]);
        int nbNodesRight = Integer.parseInt(args[2]);
        int indexMinimum = Integer.parseInt(args[3]);
        int nbSteps = Integer.parseInt(args[4]);
        int nbGraphs = Integer.parseInt(args[5]);
        long seed = Long.parseLong(args[6]);
        long start = System.currentTimeMillis();
        LoadAdjacencyListFile loader = new LoadAdjacencyListFile();

        LOGGER.setLevel(Level.OFF);

        LOGGER.info(String.format("FILE: %s", fileName));
        LOGGER.info(String.format("BIPARTITE GRAPH (nl, nr) = (%d %d)", nbNodesLeft, nbNodesRight));
        LOGGER.info(String.format("MINIMUM INDEX %d", indexMinimum));
        LOGGER.info(String.format("GENERATING %d Graphs EACH AFTER %d Steps", nbGraphs, nbSteps));
        LOGGER.info(String.format("START TIME: %d", start));

        try {
            HashSet<Integer>[] adjacencyList = loader.load(fileName, nbNodesLeft, indexMinimum);
            LOGGER.info(String.format("LOADING ADJACENCY FILE, Time %d", System.currentTimeMillis() - start));
            int[] nodesIndices = new int[nbNodesLeft];
            for (int i = 0; i < nbNodesLeft; i++) {
                nodesIndices[i] = i;
            }
            Random rnd = new Random(seed);
            ThreadImpl curveball = new ThreadImpl(2, nodesIndices, adjacencyList, rnd);

            for (int g = 0; g < nbGraphs; g++) {
                // running Global Curveball algorithm
                for (int i = 0; i < nbSteps; i++) {
                    curveball.step();
                }
                LOGGER.info(String.format("RUNNING CURVEBALL: Graph %d: Time: %d", g, System.currentTimeMillis() - start));
                // computing curveballAndCooccurrence matrix
                int[][] cooccurrence = Sequential.cooccLeft(adjacencyList, nbNodesLeft, nbNodesRight);
                LOGGER.info(String.format("COMPUTING COOCCURRENCE MATRIX: Graph %d: Time: %d", g, System.currentTimeMillis() - start));
                // saving cooccurence matrix
                SaveMatrixToFile.save(cooccurrence, String.format("coocc_graph%d.csv", g));
                LOGGER.info(String.format("SAVING COOCCURRENCE MATRIX: Graph %d: Time: %d", g, System.currentTimeMillis() - start));
                // saving adjacency list
                LoadAdjacencyListFile.save(adjacencyList, String.format("adj_list_graph%d.csv", g), ' ');
                LOGGER.info(String.format("SAVING ADJACENCY LIST: Graph %d: Time: %d", g, System.currentTimeMillis() - start));
            }

        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }

        LOGGER.info(String.format("END TIME: %d", System.currentTimeMillis() - start));


        System.exit(0);
    }

}
