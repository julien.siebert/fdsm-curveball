package de.kl.uni.cs.aalab.experiments.curveballAndCooccurrence;

import de.kl.uni.cs.aalab.curveball.adjacencyListImpl.bipartite.sequential.SequentialCooccWhileTrading;
import de.kl.uni.cs.aalab.utilities.LoadAdjacencyListFile;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Random;
import java.util.logging.Logger;

public class TestCooccurrenceWhileTrading {


    private final static Logger LOGGER = Logger.getLogger(StatsCooccurrence.class.getName());

    private static String usage = "This program samples cooccurrences from the curveball trade\n" +
            "USAGE:\n" +
            "This program requires 8 command line arguments, namely\n" +
            "the name of the file to load\n" +
            "the number of nodes in the left side of the bipartite graph\n" +
            "the number of nodes in the right side of the bipartite graph\n" +
            "the minimum index (usually 1)\n" +
            "the number of steps to run before computing curveballAndCooccurrence (usually 20)\n" +
            "the number of graphs to generate (usually 100)\n" +
            "the random seed\n" +
            "the name of the output file for saving cooccurrences";


    public static void main(String[] args) {
        if (args.length != 8) {
            System.err.println(usage);
            System.exit(1);
        }
        String fileName = args[0];
        int nbNodesLeft = Integer.parseInt(args[1]);
        int nbNodesRight = Integer.parseInt(args[2]);
        int indexMinimum = Integer.parseInt(args[3]);
        int nbSteps = Integer.parseInt(args[4]);
        int nbGraphs = Integer.parseInt(args[5]);
        long seed = Long.parseLong(args[6]);
        String outputFilename = args[7];

        long start = System.currentTimeMillis();


        LOGGER.info(String.format("FILE: %s", fileName));
        LOGGER.info(String.format("BIPARTITE GRAPH (nl, nr) = (%d %d)", nbNodesLeft, nbNodesRight));
        LOGGER.info(String.format("MINIMUM INDEX %d", indexMinimum));
        LOGGER.info(String.format("GENERATING %d Graphs EACH AFTER %d Steps", nbGraphs, nbSteps));
        LOGGER.info(String.format("START TIME: %d", start));

        try {

            // load adjacency list
            LoadAdjacencyListFile loader = new LoadAdjacencyListFile();
            HashSet<Integer>[] adjacencyList = loader.load(fileName, nbNodesLeft, indexMinimum);

            LOGGER.info(String.format("LOADING ADJACENCY FILE, Time %d", System.currentTimeMillis() - start));

            // setting up list of nodes
            int[] nodesIndices = new int[nbNodesLeft];
            for (int i = 0; i < nbNodesLeft; i++) {
                nodesIndices[i] = i;
            }

            // init random generator
            Random rnd = new Random(seed);

            // init curveball implementation
            SequentialCooccWhileTrading curveball = new SequentialCooccWhileTrading(nodesIndices, adjacencyList, rnd, Paths.get(outputFilename));

            for (int g = 0; g < nbGraphs; g++) {
                // running Global Curveball algorithm
                LOGGER.info(String.format("START CURVEBALL: Graph %d: Time: %d", g, System.currentTimeMillis() - start));
                for (int i = 0; i < nbSteps; i++) {
                    curveball.step();
                }
                LOGGER.info(String.format("END CURVEBALL: Graph %d: Time: %d", g, System.currentTimeMillis() - start));
                // saving adjacency list
                LoadAdjacencyListFile.save(adjacencyList, String.format("adj_list_graph%d.csv", g), ' ');
                LOGGER.info(String.format("SAVING ADJACENCY LIST: Graph %d: Time: %d", g, System.currentTimeMillis() - start));
            }

            curveball.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        LOGGER.info(String.format("END TIME: %d", System.currentTimeMillis() - start));

        System.exit(0);
    }

}
