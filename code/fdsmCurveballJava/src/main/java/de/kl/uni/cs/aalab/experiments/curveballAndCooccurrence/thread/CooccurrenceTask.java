package de.kl.uni.cs.aalab.experiments.curveballAndCooccurrence.thread;

import de.kl.uni.cs.aalab.cooccurence.adjacencyListImpl.sequential.Sequential;
import de.kl.uni.cs.aalab.utilities.SaveMatrixToFile;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Logger;

public class CooccurrenceTask implements Runnable {
    private String filename;
    private Path folderToSave;
    private int nr;
    private int nl;
    private HashSet<Integer>[] adjacencyList;
    private final static Logger LOGGER = Logger.getLogger(CooccurrenceTask.class.getName());

    /**
     * @param adjacencyList : adjacency list
     * @param nl :            number of nodes on the left side
     * @param nr :            number of nodes on the right side
     * @param folderToSave: the path to the directory to save the curveballAndCooccurrence matrix
     */
    public CooccurrenceTask(HashSet<Integer>[] adjacencyList, int nl, int nr, Path folderToSave, String filename) {
        setAdjacencyList(adjacencyList);
        this.nl = nl;
        this.nr = nr;
        this.folderToSave = folderToSave;
        this.filename = filename;
    }

    public HashSet<Integer>[] getAdjacencyList() {
        return adjacencyList;
    }

    /**
     * Deep Copy of adjacency list
     *
     * @param adjacencyList : adjacency list
     */
    public void setAdjacencyList(HashSet<Integer>[] adjacencyList) {
        this.adjacencyList = new HashSet[adjacencyList.length];

        for (int i = 0; i < adjacencyList.length; i++) {
            HashSet<Integer> newNeighbors = new HashSet<>(adjacencyList[i].size(), 1.0f);
            newNeighbors.addAll(adjacencyList[i]);
            this.adjacencyList[i] = newNeighbors;
        }
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }


    /**
     * Computes Sequential and save the resulting matrix to a file
     * @throws Exception
     */
    @Override
    public void run() {
        LOGGER.info(String.format("Computing curveballAndCooccurrence %s",this.filename));
        HashMap<String, Integer> cooccurrence = Sequential.coocc(this.adjacencyList, this.nl, this.nr);
        Path fileToSave = folderToSave.resolve(Paths.get(this.filename));
        try {
            LOGGER.info(String.format("Save adjacency matrix %s", fileToSave.toString()));
            SaveMatrixToFile.save(cooccurrence, fileToSave.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
