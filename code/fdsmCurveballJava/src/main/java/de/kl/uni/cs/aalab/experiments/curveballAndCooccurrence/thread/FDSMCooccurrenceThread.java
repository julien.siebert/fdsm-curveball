package de.kl.uni.cs.aalab.experiments.curveballAndCooccurrence.thread;

import de.kl.uni.cs.aalab.curveball.adjacencyListImpl.bipartite.sequential.SequentialImpl;
import de.kl.uni.cs.aalab.utilities.LoadAdjacencyListFile;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class FDSMCooccurrenceThread {

    private final int nl;
    private final int nr;
    private final Path folderToSave;
    private ExecutorService pool;
    private SequentialImpl curveball;
    private final static Logger LOGGER = Logger.getLogger(FDSMCooccurrenceThread.class.getName());

    public FDSMCooccurrenceThread(SequentialImpl curveball, int nl, int nr, Path folderToSave) {
        // creating a pool of threads
        this.pool = Executors.newFixedThreadPool(2);
        this.curveball = curveball;
        this.nl = nl;
        this.nr = nr;
        this.folderToSave = folderToSave;
    }

    public void run(int nbGraphs, int nbSteps) throws InterruptedException {

        try {
            for (int i = 0; i < nbGraphs; i++) {
                // run the curveball
                LOGGER.info(String.format("Generating graph %d", i));
                for (int j = 0; j < nbSteps; j++) {
                    curveball.step();
                }

                // save adjacency list
                LOGGER.info(String.format("Saving adjacency list for graph %d", i));
                Path fileToSave = folderToSave.resolve(String.format("adjacency_list_%d.dat", i));
                LoadAdjacencyListFile.save(curveball.getAdjacencyList(), fileToSave.toString(), ' ');

                // run the curveballAndCooccurrence in a separate thread on a copy of the adjacency list
                pool.execute(new CooccurrenceTask(curveball.getAdjacencyList(), nl, nr, folderToSave, String.format("cooccurrence_%d.dat", i)));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            pool.shutdown();
            pool.awaitTermination(30, TimeUnit.MINUTES);
        }
    }
}
