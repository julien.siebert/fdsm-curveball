package de.kl.uni.cs.aalab.experiments.curveballAndCooccurrence.thread;

import de.kl.uni.cs.aalab.curveball.adjacencyListImpl.bipartite.sequential.SequentialImpl;
import de.kl.uni.cs.aalab.utilities.LoadAdjacencyListFile;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Random;

/**
 * Runs the global curveball for generating g random graphs
 * For each graph, computes the curveballAndCooccurrence matrix
 * Saves both adjacency list and curveballAndCooccurrence matrix for each random graph
 */
public class ProfilingFDSMCooccurrenceThread {

    private static String usage = "This program requires 7 command line arguments, namely\n" +
            "the name of the file to load\n" +
            "the number of rows in the adjacency list\n" +
            "the number of nodes on the right side\n" +
            "the minimum index (usually 1)\n" +
            "the number of graphs to generate\n" +
            "the number of steps to run\n" +
            "the random seed";

    public static void main(String[] args) {

        if (args.length != 7) {
            System.err.println(usage);
            System.exit(1);
        }

        String fileName = args[0];
        int nbLines = Integer.parseInt(args[1]);
        int nr = Integer.parseInt(args[2]);
        int indexMinimum = Integer.parseInt(args[3]);
        int nbGraphs = Integer.parseInt(args[4]);
        int nbSteps = Integer.parseInt(args[5]);
        long seed = Long.parseLong(args[6]);

        long start = System.currentTimeMillis();
        run(fileName, nbLines, nr, indexMinimum, nbGraphs, nbSteps, seed);
        long end = System.currentTimeMillis();

        System.out.println("Time elapsed: " + (end - start) + " ms");
    }

    /**
     * @param fileName:     the file to load
     * @param nbLines:      the number of lines in that file
     * @param nr:           the number of nodes on the right side
     * @param nbGraphs:     the number of random graphs to generate
     * @param indexMinimum: the index of the first node (usually 1)
     * @param nbSteps:      the number of steps to run the Global Curveball algorithm
     * @param seed:         the random seed
     */
    private static void run(String fileName, int nbLines, int nr, int indexMinimum, int nbGraphs, int nbSteps, long seed) {

        Path folderToSave = Paths.get(".");

        LoadAdjacencyListFile loader = new LoadAdjacencyListFile();
        try {
            HashSet<Integer>[] adjacencyList = loader.load(fileName, nbLines, indexMinimum);
            int[] nodesIndices = new int[nbLines];
            for (int i = 0; i < nbLines; i++) {
                nodesIndices[i] = i;
            }
            Random rnd = new Random(seed);
            SequentialImpl curveball = new SequentialImpl(nodesIndices, adjacencyList, rnd);

            FDSMCooccurrenceThread prog = new FDSMCooccurrenceThread(curveball, nbLines, nr, folderToSave);

            prog.run(nbGraphs, nbSteps);

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
