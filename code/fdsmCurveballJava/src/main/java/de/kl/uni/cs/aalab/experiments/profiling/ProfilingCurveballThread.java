package de.kl.uni.cs.aalab.experiments.profiling;

import de.kl.uni.cs.aalab.curveball.adjacencyListImpl.bipartite.thread.ThreadImpl;
import de.kl.uni.cs.aalab.utilities.LoadAdjacencyListFile;

import java.io.IOException;
import java.util.HashSet;
import java.util.Random;

public class ProfilingCurveballThread {

    private static String usage = "This program requires 6 command line arguments, namely\n" +
            "the name of the file to load\n" +
            "the number of rows in the adjacency list\n" +
            "the minimum index (usually 1)\n" +
            "the number of steps to run\n" +
            "the number of threads\n" +
            "the random seed";

    public static void main(String[] args) {

        if (args.length != 6) {
            System.err.println(usage);
            System.exit(1);
        }

        String fileName = args[0];
        int nbLines = Integer.parseInt(args[1]);
        int indexMinimum = Integer.parseInt(args[2]);
        int nbSteps = Integer.parseInt(args[3]);
        int nbThreads = Integer.parseInt(args[4]);
        long seed = Long.parseLong(args[5]);

        long start = System.currentTimeMillis();
        run(fileName, nbLines, indexMinimum, nbSteps, nbThreads, seed);
        long end = System.currentTimeMillis();

        System.out.println("Time elapsed: "+(end-start)+" ms");
    }

    /**
     * Runs `nbSteps` steps of the global de.uni.kl.cs.aalab.curveball algo
     * @param fileName : the file to load
     * @param nbLines : the number of lines in that file
     * @param indexMinimum : the index of the first node (usually 1)
     * @param nbSteps : the number of steps to run the algorithm
     * @param nbThreads : the number of threads used to run the algorithm
     * @param seed
     */
    private static void run(String fileName, int nbLines, int indexMinimum, int nbSteps, int nbThreads, long seed) {
        LoadAdjacencyListFile loader = new LoadAdjacencyListFile();
        try {
            HashSet<Integer>[] adjacencyList = loader.load(fileName, nbLines, indexMinimum);
            int[] nodesIndices = new int[nbLines];
            for (int i = 0; i < nbLines; i++) {
                nodesIndices[i] = i;
            }
            Random rnd = new Random(seed);
            ThreadImpl curveball = new ThreadImpl(nbThreads, nodesIndices, adjacencyList, rnd);

            for (int i = 0; i < nbSteps; i++) {
                curveball.step();
            }
            curveball.shutdown();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}