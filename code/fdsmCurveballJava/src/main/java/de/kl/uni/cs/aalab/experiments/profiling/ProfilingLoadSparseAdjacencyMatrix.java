package de.kl.uni.cs.aalab.experiments.profiling;

import com.zaxxer.sparsebits.SparseBitSet;
import de.kl.uni.cs.aalab.utilities.LoadAdjacencyListFile;

import java.io.IOException;
import java.util.ArrayList;

/**
 * This class is used to profile the functionality that loads a graph (adjacency list for instance) in memory
 * Loads several times a sparse adjacency matrix from a file (adjacency list format), for profiling purposes
 */
public class ProfilingLoadSparseAdjacencyMatrix {
    private static String usage = "This program requires 4 command line arguments, namely\n" +
            "the name of the file to load\n" +
            "the number of rows in the adjacency list\n" +
            "the minimum index (usually 1)\n" +
            "the number of times to load the data";

    public static void main(String[] args) {
        if (args.length != 4) {
            System.err.println(usage);
            System.exit(1);
        }
        String fileName = args[0];
        int nbLines = Integer.parseInt(args[1]);
        int indexMinimum = Integer.parseInt(args[2]);
        int nbLoad = Integer.parseInt(args[3]);
        long start = System.currentTimeMillis();
        LoadAdjacencyListFile loader = new LoadAdjacencyListFile();
        for (int i = 0; i < nbLoad; i++) {
            try {
                ArrayList<SparseBitSet> adjacencyMatrix = loader.loadSparseAdjacencyMatrix(fileName, nbLines, indexMinimum);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        long end = System.currentTimeMillis();
        System.out.println("Time elapsed: "+(end-start)+" ms");
    }
}
