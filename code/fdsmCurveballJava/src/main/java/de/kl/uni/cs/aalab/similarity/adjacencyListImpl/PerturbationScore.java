package de.kl.uni.cs.aalab.similarity.adjacencyListImpl;

import java.util.HashSet;

/**
 * Computes the Perturbation Score between tow graphs G0 and G1
 * Using Adjacency List
 */
public class PerturbationScore {

    /**
     * @param nodeNeighboursG0
     * @param nodeNeighboursG1
     * @return
     */
    protected static int nbEdgesChanged(HashSet<Integer> nodeNeighboursG0, HashSet<Integer> nodeNeighboursG1) {
        HashSet<Integer> diff = new HashSet<>(nodeNeighboursG0);
        diff.removeAll(nodeNeighboursG1);
        return diff.size();
    }

    /**
     * Computes the perturbation score between two graphs
     * The perturbation score is defined as
     * <p>
     * Perturbation is measured as the percentage of cells differing from the corresponding ones of the original matrix.
     * Giovanni Strona, Domenico Nappo, Francesco Boccacci, Simone Fattorini & Jesus San-Miguel-Ayanz.
     * A fast and unbiased procedure to randomize ecological binary matrices with fixed row and column totals
     * Nature Communications volume 5, Article number: 4114 (2014)
     * doi:10.1038/ncomms5114
     *
     * @param adjacencyListG0: adjacency list of the first graph G0
     * @param adjacencyListG1: adjacency list of the second graph G1
     * @return perturbation score
     */
    public static float computeScore(HashSet<Integer>[] adjacencyListG0, HashSet<Integer>[] adjacencyListG1) {
        int nbDifferentEdges = 0;
        int totalNbEdges = 0;
        for (int i = 0; i < adjacencyListG0.length; i++) {
            nbDifferentEdges += nbEdgesChanged(adjacencyListG0[i], adjacencyListG1[i]);
            totalNbEdges += adjacencyListG0[i].size();
        }
        return (float) nbDifferentEdges / (float) totalNbEdges;
    }
}
