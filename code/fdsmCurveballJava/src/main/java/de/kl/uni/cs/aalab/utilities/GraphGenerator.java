package de.kl.uni.cs.aalab.utilities;

import java.util.HashSet;

/**
 * Utilities to generate graphs with specific degree distributions
 */
public class GraphGenerator {

    /**
     * @param base
     * @param exponent
     * @return the adjacency list of a bipartite graph with n = base^exponent nodes with a degree distribution following a power law
     */
    public static HashSet<Integer>[] powerLaw(int base, int exponent) {
        int n = (int) Math.pow(base, exponent);
        HashSet<Integer>[] adjacencyList = new HashSet[n];
        for (int i = 0; i < n; i++) {
            adjacencyList[i] = new HashSet<Integer>();
        }
        for (int i = 0; i < exponent; i++) {
            for (int j = 0; j < n / Math.pow(base, i); j++) {
                adjacencyList[j].add(i + j);
            }
        }
        return adjacencyList;
    }

    /**
     * @param n
     * @return the adjacency list of a bipartite graph with n nodes where the degree distribution of each side is {n,n-1,n-2,...,3,2,1}
     */
    public static HashSet<Integer>[] linear(int n) {
        HashSet<Integer>[] adjacencyList = new HashSet[n];
        for (int i = 0; i < n; i++) {
            adjacencyList[i] = new HashSet<Integer>(n - 1, 1);
            for (int j = 0; j < n - i; j++) {
                adjacencyList[i].add(j);
            }
        }
        return adjacencyList;
    }
}
