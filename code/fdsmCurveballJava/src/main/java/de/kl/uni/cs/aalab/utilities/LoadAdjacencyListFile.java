package de.kl.uni.cs.aalab.utilities;

import com.zaxxer.sparsebits.SparseBitSet;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashSet;

public class LoadAdjacencyListFile {

    public static void save(HashSet<Integer>[] adjacencyList, String filename) throws IOException {
        save(adjacencyList, filename, ' ');
    }

    /**
     * Save a adjacency list in a file with CSV format
     *
     * @param adjacencyList the adjacency list to save
     * @param filename      the name of the file to save
     * @param separator     separator
     * @throws IOException
     */
    public static void save(HashSet<Integer>[] adjacencyList, String filename, char separator) throws IOException {
        BufferedWriter outputWriter;
        outputWriter = new BufferedWriter(new FileWriter(filename));

        for (int i = 0; i < adjacencyList.length; i++) {
            outputWriter.write(Integer.toString(i));
            for (Integer neighbor : adjacencyList[i]) {
                outputWriter.write(separator + Integer.toString(neighbor));
            }
            outputWriter.newLine();
        }

        outputWriter.flush();
        outputWriter.close();
    }

    /**
     * Loads an Adjacency List file,
     * <p>
     * with format:
     * <p>
     * node1 neighbour11 neighbour12 ... neighbour1K
     * node2 neighbour21 neighbour22 ... neighbour2K
     * ...
     * nodeN neighbourN1 neighbourN2 ... neighbourNK
     * <p>
     * Returns an adjacency list where the first item is a Set {neighbour11 neighbour12 ... neighbour1K},
     * the second is a Set {neighbour21 neighbour22 ... neighbour2K} ...
     * until the last item is a set {neighbourN1 neighbourN2 ... neighbourNK}
     *
     * @param fileName: path to the file containing the adjacency list
     * @param nbLines:  the number of lines in the adjacency list file
     * @param indexMin: the index of the first node (usually 1, so one has to convert it to 0)
     * @return adjacency list
     * @throws IOException
     */
    public HashSet<Integer>[] load(String fileName, int nbLines, int indexMin) throws IOException {

        // initialisation of adjacency list
        HashSet<Integer>[] adjacencyList = new HashSet[nbLines];

        HashSet<Integer> neighbors;

        BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            String[] s = line.split("\\s+");
            int nodeIndex = Integer.parseInt(s[0]) - indexMin;

            neighbors = new HashSet<>(s.length - 1, 1);
            if (s.length > 1) {
                for (String a : Arrays.copyOfRange(s, 1, s.length)) {
                    neighbors.add(Integer.parseInt(a) - indexMin);
                }
            }
            adjacencyList[nodeIndex] = neighbors;
        }

        // Always close files.
        bufferedReader.close();

        return adjacencyList;
    }


    /**
     * Loads an Adjacency List file,
     * <p>
     * with format:
     * <p>
     * node1 neighbour11 neighbour12 ... neighbour1K
     * node2 neighbour21 neighbour22 ... neighbour2K
     * ...
     * nodeN neighbourN1 neighbourN2 ... neighbourNK
     * <p>
     * Returns an adjacency matrix
     *
     * @param fileName: path to the file containing the adjacency list
     * @param nbLines:  the number of lines in the adjacency list file
     * @param indexMin: the index of the first node (usually 1, so one has to convert it to 0)
     * @return adjacency matrix
     * @throws IOException
     */
    public ArrayList<BitSet> loadAdjacencyMatrix(String fileName, int nbLines, int indexMin) throws IOException {

        // initialisation of adjacency matrix
        ArrayList<BitSet> adjacencyMatrix = new ArrayList<>(nbLines);

        BitSet row;

        for (int i = 0; i < nbLines; i++) {
            row = new BitSet();
            adjacencyMatrix.add(row);
        }

        BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            String[] s = line.split("\\s+");
            int nodeIndex = Integer.parseInt(s[0]) - indexMin;


            if (s.length > 1) {
                for (String a : Arrays.copyOfRange(s, 1, s.length)) {
                    int neighborIndex = Integer.parseInt(a) - indexMin;
                    adjacencyMatrix.get(nodeIndex).set(neighborIndex);
                }
            }
        }

        // Always close files.
        bufferedReader.close();

        return adjacencyMatrix;
    }

    public ArrayList<SparseBitSet> loadSparseAdjacencyMatrix(String fileName, int nbLines, int indexMin) throws IOException {
        // initialisation of adjacency matrix
        ArrayList<SparseBitSet> adjacencyMatrix = new ArrayList<SparseBitSet>(nbLines);

        SparseBitSet row;

        for (int i = 0; i < nbLines; i++) {
            row = new SparseBitSet();
            adjacencyMatrix.add(row);
        }

        BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            String[] s = line.split("\\s+");
            int nodeIndex = Integer.parseInt(s[0]) - indexMin;


            if (s.length > 1) {
                for (String a : Arrays.copyOfRange(s, 1, s.length)) {
                    int neighborIndex = Integer.parseInt(a) - indexMin;
                    adjacencyMatrix.get(nodeIndex).set(neighborIndex);
                }
            }
        }

        // Always close files.
        bufferedReader.close();

        return adjacencyMatrix;
    }
}
