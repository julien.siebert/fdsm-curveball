package de.kl.uni.cs.aalab.utilities;

import de.kl.uni.cs.aalab.datastructure.CooccHashMap;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Save a matrix of int in a file, csv format
 */
public class SaveMatrixToFile {


    /**
     * Save a matrix of int in a file with CSV format
     *
     * @param matrix the matrix of int to save
     * @param filename the name of the file to save
     * @throws IOException
     */
    public static void save(int[][] matrix, String filename) throws IOException {
        BufferedWriter outputWriter = null;
        outputWriter = new BufferedWriter(new FileWriter(filename));

        for (int i = 0; i < matrix.length; i++) {
            // write a row of integer
            for (int j = 0; j < matrix.length - 1; j++) {
                outputWriter.write(Integer.toString(matrix[i][j]) + ",");
            }
            // write last integer
            outputWriter.write(Integer.toString(matrix[i][matrix.length - 1]));
            outputWriter.newLine();
        }

        outputWriter.flush();
        outputWriter.close();
    }


    /**
     * Save a matrix of int in a file with CSV format
     *
     * @param matrix the matrix of int to save
     * @param filename the name of the file to save
     * @throws IOException
     */
    public static void save(HashMap<String, Integer> matrix, String filename) throws IOException {
        BufferedWriter outputWriter;
        outputWriter = new BufferedWriter(new FileWriter(filename));

        for (Map.Entry<String, Integer> entry : matrix.entrySet()) {
            outputWriter.write(String.format("%s %d", entry.getKey(), entry.getValue()));
            outputWriter.newLine();
        }

        outputWriter.flush();
        outputWriter.close();
    }


    /**
     * Save a matrix of int in a file with CSV format
     *
     * @param cooccHashMap the matrix of int to save
     * @param filename the name of the file to save
     * @throws IOException
     */
    public static void save(CooccHashMap cooccHashMap, String filename) throws IOException {
        BufferedWriter outputWriter;
        outputWriter = new BufferedWriter(new FileWriter(filename));

        for (Map.Entry<CooccHashMap.Key, Integer> entry : cooccHashMap.entrySet()) {
            outputWriter.write(String.format("%s %d", entry.getKey(), entry.getValue()));
            outputWriter.newLine();
        }

        outputWriter.flush();
        outputWriter.close();
    }
}
