package de.kl.uni.cs.aalab.utilities;

import java.util.Random;

public class Shuffle {

    /**
     * Inplace shuffling of an array (Fisher-Yates)
     * @param array: the array to shuffle
     * @param random: the random generator to use
     */
    public static void shuffleArray(int[] array, Random random) {
        int index, temp;
        for (int i = array.length - 1; i > 0; i--) {
            index = random.nextInt(i + 1);
            temp = array[index];
            array[index] = array[i];
            array[i] = temp;
        }
    }
}

