# coding: utf-8

from collections import defaultdict

# create an adjacency list
adj = defaultdict(list)

# open the file containing the edge list and convert it to an adjacency list
with open("out.actor-movie","r") as f:
    for line in f:
        if line[0] != "%":
            s,t = line.strip().split()
            adj[s] += [t]

# save the adjacency list
with open("adj.txt","w") as f:
    for k,v in adj.iteritems():
        f.write("{} {}\n".format(k," ".join(v)))
        
