#! /bin/bash
#SBATCH -J Profiling_Curveball
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --cpus-per-task=16 
#SBATCH -o Profiling_Curveball-%j.out
#SBATCH -e Profiling_Curveball-%j.err
#SBATCH -t 120

for THREADS in 2 4 8 12;
do
  for STEPS in 100 500 1000 5000 10000 50000;
  do
    echo $THREADS $STEPS $(java -cp fdsmCurveballJava-1.0-SNAPSHOT.jar de.kl.uni.cs.aalab.experiments.profiling.ProfilingCurveballThread adj.txt 127823 1 $STEPS $THREADS 0);
  done;
done;
