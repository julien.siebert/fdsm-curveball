package de.kl.uni.cs.aalab.cooccurence;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CooccurrenceTest {

    @Test
    @DisplayName("Converts a left-side adjacency list of a bipartite graph to a right-side adjacency list")
    void convert() {
        // number of nodes in the graph
        int nl = 3;
        int nr = 5;

        // initializing the adjacency list to be converted
        HashSet<Integer>[] adjacencyList = new HashSet[nl];
        for (int i = 0; i < nl; i++) {
            adjacencyList[i] = new HashSet<>();
        }
        adjacencyList[0].addAll(Arrays.asList(0, 1, 2));
        adjacencyList[1].addAll(Arrays.asList(3, 4));
        adjacencyList[2].addAll(Arrays.asList(0, 2, 4));

        // initializing the expected adjacency list
        HashSet<Integer>[] expectedAdjacencyList = new HashSet[nr];
        for (int i = 0; i < nr; i++) {
            expectedAdjacencyList[i] = new HashSet<>();
        }
        expectedAdjacencyList[0].addAll(Arrays.asList(0, 2));
        expectedAdjacencyList[1].add(0);
        expectedAdjacencyList[2].addAll(Arrays.asList(0, 2));
        expectedAdjacencyList[3].add(1);
        expectedAdjacencyList[4].addAll(Arrays.asList(1, 2));

        HashSet<Integer>[] convertedAdjacencyList = Cooccurrence.convert(adjacencyList, nl, nr);

        for (int i = 0; i < nr; i++) {
            assertEquals(expectedAdjacencyList[i], convertedAdjacencyList[i]);
        }
    }

    @Test
    void convert1() {
        // initializing adjacency list
        HashSet<Integer>[] adjacencyList = new HashSet[6];
        for (int i = 0; i < 6; i++) {
            adjacencyList[i] = new HashSet<>();
        }

        adjacencyList[0].addAll(Arrays.asList(1, 2));
        adjacencyList[1].addAll(Arrays.asList(0, 3));
        adjacencyList[2].addAll(Arrays.asList(0, 1, 3));
        adjacencyList[3].addAll(Arrays.asList(2, 4, 5));
        adjacencyList[4].addAll(Arrays.asList(0, 1, 2, 3));
        adjacencyList[5].addAll(Arrays.asList(4, 6, 7, 8));

        // initializing the adjacency list
        HashSet<Integer>[] expectedAdjacencyList = new HashSet[9];
        for (int i = 0; i < 9; i++) {
            expectedAdjacencyList[i] = new HashSet<>();
        }
        expectedAdjacencyList[0].addAll(Arrays.asList(1, 2, 4));
        expectedAdjacencyList[1].addAll(Arrays.asList(0, 2, 4));
        expectedAdjacencyList[2].addAll(Arrays.asList(0, 3, 4));
        expectedAdjacencyList[3].addAll(Arrays.asList(1, 2, 4));
        expectedAdjacencyList[4].addAll(Arrays.asList(3, 5));
        expectedAdjacencyList[5].add(3);
        expectedAdjacencyList[6].add(5);
        expectedAdjacencyList[7].add(5);
        expectedAdjacencyList[8].add(5);


        HashSet<Integer>[] convertedAdjacencyList = Cooccurrence.convert(adjacencyList, 6, 9);

        for (int i = 0; i < 9; i++) {
            assertEquals(expectedAdjacencyList[i], convertedAdjacencyList[i]);
        }

    }
}