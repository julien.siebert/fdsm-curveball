package de.kl.uni.cs.aalab.cooccurence.adjacencyListImpl.sequential;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SequentialTest {

    /**
     * The tested graph looks like this
     * <p>
     * 0: {3,4}
     * 1: {5,6}
     * 2: {7}
     * <p>
     * <p>
     * Expected curveballAndCooccurrence should be:
     * <p>
     * 0 0 0
     * 0 0 0
     * 0 0 0
     */
    @Test
    @DisplayName("Cooccurence Matrix full of Zeros when no common neighbors exist")
    void cooccLeftZeros() {
        // number of nodes in the graph
        int nl = 3;
        int nr = 2;

        // initializing the adjacency list
        HashSet<Integer>[] leftSideAdjacencyList = new HashSet[nl];

        HashSet<Integer> neighbors = new HashSet<>(2, 1);
        neighbors.addAll(Arrays.asList(3, 4));
        leftSideAdjacencyList[0] = neighbors;

        neighbors = new HashSet<>(2, 1);
        neighbors.addAll(Arrays.asList(5, 6));
        leftSideAdjacencyList[1] = neighbors;

        neighbors = new HashSet<>(1, 1);
        neighbors.add(7);
        leftSideAdjacencyList[2] = neighbors;

        int[][] expectedCooccurrence = new int[nl][nl];
        expectedCooccurrence[0][1] = 0;
        expectedCooccurrence[0][2] = 0;
        expectedCooccurrence[1][2] = 0;
        expectedCooccurrence[1][0] = 0;
        expectedCooccurrence[2][0] = 0;
        expectedCooccurrence[2][1] = 1;

        int[][] cooccurrence = Sequential.cooccLeft(leftSideAdjacencyList, nl, nr);

        assertEquals(expectedCooccurrence[0][1], cooccurrence[0][1]);
        assertEquals(expectedCooccurrence[0][2], cooccurrence[0][2]);
        assertEquals(expectedCooccurrence[1][2], cooccurrence[1][2]);

        assertEquals(0, cooccurrence[0][0]);
        assertEquals(0, cooccurrence[1][1]);
        assertEquals(0, cooccurrence[2][2]);

        assertEquals(cooccurrence[0][1], cooccurrence[1][0]);
        assertEquals(cooccurrence[0][2], cooccurrence[2][0]);
        assertEquals(cooccurrence[1][2], cooccurrence[2][1]);
    }

    /**
     * The tested graph looks like this
     * <p>
     * 0: {4}
     * 1: {3,4}
     * 2: {3,4}
     * <p>
     * <p>
     * Expected curveballAndCooccurrence should be:
     * <p>
     * 0 1 1
     * 1 0 2
     * 1 2 0
     */
    @Test
    @DisplayName("Cooccurence Matrix is computed as expected, the diagonal is zero, is symmetric")
    void cooccLeft() {
        // number of nodes in the graph
        int nl = 3;
        int nr = 2;

        // initializing the adjacency list
        HashSet<Integer>[] leftSideAdjacencyList = new HashSet[nl];

        HashSet<Integer> neighbors = new HashSet<>(1, 1);
        neighbors.add(4);
        leftSideAdjacencyList[0] = neighbors;

        neighbors = new HashSet<>(2, 1);
        neighbors.addAll(Arrays.asList(3, 4));
        leftSideAdjacencyList[1] = neighbors;

        neighbors = new HashSet<>(2, 1);
        neighbors.addAll(Arrays.asList(3, 4));
        leftSideAdjacencyList[2] = neighbors;

        int[][] cooccurrence = Sequential.cooccLeft(leftSideAdjacencyList, nl, nr);

        assertEquals(1, cooccurrence[0][1]);
        assertEquals(1, cooccurrence[0][2]);
        assertEquals(2, cooccurrence[1][2]);

        assertEquals(0, cooccurrence[0][0]);
        assertEquals(0, cooccurrence[1][1]);
        assertEquals(0, cooccurrence[2][2]);

        assertEquals(1, cooccurrence[1][0]);
        assertEquals(1, cooccurrence[2][0]);
        assertEquals(2, cooccurrence[2][1]);
    }

    /**
     * The tested graph looks like this
     * <p>
     * 0: {2}
     * 1: {3}
     * <p>
     * <p>
     * Expected curveballAndCooccurrence should be:
     * <p>
     * 0 0
     * 0 0
     */
    @Test
    @DisplayName("Cooccurence Matrix full of Zeros when no common neighbors exist")
    void cooccRightZeros() {
        // number of nodes in the graph
        int nl = 2;
        int nr = 2;

        // initializing the adjacency list
        HashSet<Integer>[] rightSideAdjacencyList = new HashSet[nr];

        HashSet<Integer> neighbors = new HashSet<>(1, 1);
        neighbors.add(0);
        rightSideAdjacencyList[0] = neighbors;

        neighbors = new HashSet<>(1, 1);
        neighbors.add(1);
        rightSideAdjacencyList[1] = neighbors;
        int[][] cooccurrence = Sequential.cooccRight(rightSideAdjacencyList, nl);

        assertEquals(0, cooccurrence[0][1]);

        assertEquals(0, cooccurrence[0][0]);
        assertEquals(0, cooccurrence[1][1]);

        assertEquals(0, cooccurrence[1][0]);
    }


    /**
     * The tested graph looks like this
     * <p>
     * 0: {5}
     * 1: {3,5}
     * 2: {3,4}
     * <p>
     * Corresponding "right side adjacency list"
     * <p>
     * 3: {1,2}
     * 4: {2}
     * 5: {0,1}
     * <p>
     * Expected curveballAndCooccurrence should be:
     * <p>
     * 0 1 0
     * 1 0 1
     * 0 1 0
     */
    @Test
    @DisplayName("Cooccurence Matrix is computed as expected, the diagonal is zero, is symmetric")
    void cooccRight() {
        // number of nodes in the graph
        int nl = 3;
        int nr = 3;

        // initializing the adjacency list
        HashSet<Integer>[] rightSideAdjacencyList = new HashSet[nr];

        HashSet<Integer> neighbors = new HashSet<>(2, 1);
        neighbors.addAll(Arrays.asList(1, 2));
        rightSideAdjacencyList[0] = neighbors;

        neighbors = new HashSet<>(1, 1);
        neighbors.add(2);
        rightSideAdjacencyList[1] = neighbors;

        neighbors = new HashSet<>(2, 1);
        neighbors.addAll(Arrays.asList(0, 1));
        rightSideAdjacencyList[2] = neighbors;

        int[][] cooccurrence = Sequential.cooccRight(rightSideAdjacencyList, nl);

        assertEquals(1, cooccurrence[0][1]);
        assertEquals(0, cooccurrence[0][2]);
        assertEquals(1, cooccurrence[1][2]);

        assertEquals(0, cooccurrence[0][0]);
        assertEquals(0, cooccurrence[1][1]);
        assertEquals(0, cooccurrence[2][2]);

        assertEquals(1, cooccurrence[1][0]);
        assertEquals(0, cooccurrence[2][0]);
        assertEquals(1, cooccurrence[2][1]);
    }


    @Test
    void cooccNaive() {
        // initializing the adjacency list
        HashSet<Integer>[] adjacencyList = new HashSet[3];
        HashSet<Integer> neighbors = new HashSet<>(3, 1.0f);
        neighbors.addAll(Arrays.asList(1, 2, 3));
        adjacencyList[0] = neighbors;
        neighbors = new HashSet<>(2, 1.0f);
        neighbors.addAll(Arrays.asList(1, 2));
        adjacencyList[1] = neighbors;
        neighbors = new HashSet<>(1, 1.0f);
        neighbors.add(3);
        adjacencyList[2] = neighbors;

        // expected curveballAndCooccurrence
        HashMap<String, Integer> expected_cooccurrence = new HashMap<>();
        expected_cooccurrence.put("0 1", 2);
        expected_cooccurrence.put("0 2", 1);

        HashMap<String, Integer> actual_cooccurrence = Sequential.cooccNaive(adjacencyList);

        assertEquals(actual_cooccurrence, expected_cooccurrence);
    }

    @Test
    void coocc() {
        // initializing the adjacency list
        HashSet<Integer>[] adjacencyList = new HashSet[3];
        HashSet<Integer> neighbors = new HashSet<>(3, 1.0f);
        neighbors.addAll(Arrays.asList(1, 2, 3));
        adjacencyList[0] = neighbors;
        neighbors = new HashSet<>(2, 1.0f);
        neighbors.addAll(Arrays.asList(1, 2));
        adjacencyList[1] = neighbors;
        neighbors = new HashSet<>(1, 1.0f);
        neighbors.add(3);
        adjacencyList[2] = neighbors;

        // expected curveballAndCooccurrence
        HashMap<String, Integer> expected_cooccurrence = new HashMap<>();
        expected_cooccurrence.put("0 1", 2);
        expected_cooccurrence.put("0 2", 1);

        HashMap<String, Integer> actual_cooccurrence = Sequential.coocc(adjacencyList, 3, 3);

        assertEquals(actual_cooccurrence, expected_cooccurrence);
    }
}