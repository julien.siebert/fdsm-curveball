package de.kl.uni.cs.aalab.cooccurence.adjacencyListImpl.thread;

import de.kl.uni.cs.aalab.cooccurence.Cooccurrence;
import de.kl.uni.cs.aalab.datastructure.CooccHashMap;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class ParallelCooccurrenceTaskTest {

    @Test
    @DisplayName("Checks partial cooccurrence matrix")
    void call() {
        // number of nodes
        int n = 6;

        // initializing adjacency list
        HashSet<Integer>[] adjacencyList = new HashSet[n];
        for (int i = 0; i < n; i++) {
            adjacencyList[i] = new HashSet(n, 1.0f);
        }

        adjacencyList[0].addAll(Arrays.asList(1, 2));
        adjacencyList[1].addAll(Arrays.asList(0, 3));
        adjacencyList[2].addAll(Arrays.asList(0, 1, 3));
        adjacencyList[3].addAll(Arrays.asList(2, 4, 5));
        adjacencyList[4].addAll(Arrays.asList(0, 1, 2, 3));
        adjacencyList[5].addAll(Arrays.asList(4, 6, 7, 8));

        /*
        Expected cooccurrence:
        (0,2) -> 1
        (0,3) -> 1
        (0,4) -> 2
        (1,2) -> 2
        (1,4) -> 2
        (2,4) -> 3
        (3,4) -> 1
         */

        ParallelCooccurrenceTask task = new ParallelCooccurrenceTask(Cooccurrence.convert(adjacencyList,6,9),0,4);
        CooccHashMap coocc = task.call();
        assertEquals(7, coocc.entrySet().size());
        assertEquals(1, coocc.get(0,2));
        assertEquals(1, coocc.get(0,3));
        assertEquals(2, coocc.get(0,4));
        assertEquals(2, coocc.get(1,2));
        assertEquals(2, coocc.get(1,4));
        assertEquals(3, coocc.get(2,4));
        assertEquals(1, coocc.get(3,4));
    }

}