package de.kl.uni.cs.aalab.cooccurence.adjacencyListImpl.thread;

import de.kl.uni.cs.aalab.cooccurence.Cooccurrence;
import de.kl.uni.cs.aalab.datastructure.CooccHashMap;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ThreadImplTest {

    @Test
    @DisplayName("Creates a list of task with the right number of elements per task")
    void creation() {
        // number of nodes
        int n = 6;

        // initializing adjacency list
        HashSet<Integer>[] adjacencyList = new HashSet[n];
        for (int i = 0; i < n; i++) {
            adjacencyList[i] = new HashSet(n, 1.0f);
        }

        adjacencyList[0].addAll(Arrays.asList(1, 2));
        adjacencyList[1].addAll(Arrays.asList(0, 3));
        adjacencyList[2].addAll(Arrays.asList(0, 1, 3));
        adjacencyList[3].addAll(Arrays.asList(2, 4, 5));
        adjacencyList[4].addAll(Arrays.asList(0, 1, 2, 3));
        adjacencyList[5].addAll(Arrays.asList(4, 6, 7, 8));

        ThreadImpl cooccurrence = new ThreadImpl(2, adjacencyList);
        ArrayList<ParallelCooccurrenceTask> tasks = cooccurrence.getTasks();
        assertEquals(2, tasks.size());
        assertEquals(0, tasks.get(0).getStartIndex());
        assertEquals(3, tasks.get(0).getEndIndex());
        assertEquals(3, tasks.get(1).getStartIndex());
        assertEquals(6, tasks.get(1).getEndIndex());
    }

    @Test
    @DisplayName("computes expected cooccurrence")
    void coocc() {
        // number of nodes
        int n = 6;

        // initializing adjacency list
        HashSet<Integer>[] adjacencyList = new HashSet[n];
        for (int i = 0; i < n; i++) {
            adjacencyList[i] = new HashSet(n, 1.0f);
        }

        adjacencyList[0].addAll(Arrays.asList(1, 2));
        adjacencyList[1].addAll(Arrays.asList(0, 3));
        adjacencyList[2].addAll(Arrays.asList(0, 1, 3));
        adjacencyList[3].addAll(Arrays.asList(2, 4, 5));
        adjacencyList[4].addAll(Arrays.asList(0, 1, 2, 3));
        adjacencyList[5].addAll(Arrays.asList(4, 6, 7, 8));

        ThreadImpl cooccurrence = new ThreadImpl(2, Cooccurrence.convert(adjacencyList,6,9));
        try {
            CooccHashMap coocc = cooccurrence.coocc();
            assertEquals(8, coocc.entrySet().size());
            assertEquals(1, coocc.get(0,2));
            assertEquals(1, coocc.get(0,3));
            assertEquals(2, coocc.get(0,4));
            assertEquals(2, coocc.get(1,2));
            assertEquals(2, coocc.get(1,4));
            assertEquals(3, coocc.get(2,4));
            assertEquals(1, coocc.get(3,4));
            assertEquals(1, coocc.get(3,5));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            cooccurrence.shutdown();
        }


    }
}