package de.kl.uni.cs.aalab.curveball.adjacencyListImpl;

import org.junit.jupiter.api.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AdjacencyListImplTest {

    private static Random rnd;

    @BeforeAll
    static void init() {
        rnd = new Random(0);
    }

    @Test
    @DisplayName("getting possible trade allowing self loop")
    void getPossibleTradesWithSelfLoop() {
        // init possible trades set
        HashSet<Integer> possibleTrades_i = new HashSet<Integer>();
        HashSet<Integer> possibleTrades_j = new HashSet<Integer>();
        // expected possible trades
        HashSet<Integer> expectedPossibleTrades_i = new HashSet<Integer>();
        HashSet<Integer> expectedPossibleTrades_j = new HashSet<Integer>();
        expectedPossibleTrades_i.addAll(Arrays.asList(0, 1, 2));
        expectedPossibleTrades_j.addAll(Arrays.asList(5, 6, 7));
        // init djacency list
        HashSet<Integer>[] adjacencyList = new HashSet[2];
        HashSet<Integer> neighbors_i = new HashSet<>(5, 1.0f);
        HashSet<Integer> neighbors_j = new HashSet<>(5, 1.0f);
        neighbors_i.addAll(Arrays.asList(0, 1, 2, 3, 4));
        neighbors_j.addAll(Arrays.asList(3, 4, 5, 6, 7));
        adjacencyList[0] = neighbors_i;
        adjacencyList[1] = neighbors_j;
        // init curveball
        AdjacencyListImpl curveball = new AdjacencyListImpl(null, adjacencyList, null);
        // function to test
        curveball.getPossibleTradesWithSelfLoop(0, 1, possibleTrades_i, possibleTrades_j);
        // asserts
        assertEquals(possibleTrades_i, expectedPossibleTrades_i);
        assertEquals(possibleTrades_j, expectedPossibleTrades_j);
    }

    @Test
    @DisplayName("getting possible trade not allowing self loop")
    void getPossibleTradesWithoutSelfLoop() {
        // init possible trades set
        HashSet<Integer> possibleTrades_i = new HashSet<Integer>();
        HashSet<Integer> possibleTrades_j = new HashSet<Integer>();
        // expected possible trades
        HashSet<Integer> expectedPossibleTrades_i = new HashSet<Integer>();
        HashSet<Integer> expectedPossibleTrades_j = new HashSet<Integer>();
        expectedPossibleTrades_i.addAll(Arrays.asList(0, 2));
        expectedPossibleTrades_j.addAll(Arrays.asList(5, 6, 7));
        // init djacency list
        HashSet<Integer>[] adjacencyList = new HashSet[2];
        HashSet<Integer> neighbors_i = new HashSet<>(5, 1.0f);
        HashSet<Integer> neighbors_j = new HashSet<>(5, 1.0f);
        neighbors_i.addAll(Arrays.asList(0, 1, 2, 3, 4));
        neighbors_j.addAll(Arrays.asList(3, 4, 5, 6, 7));
        adjacencyList[0] = neighbors_i;
        adjacencyList[1] = neighbors_j;
        // init curveball
        AdjacencyListImpl curveball = new AdjacencyListImpl(null, adjacencyList, null);
        // function to test
        curveball.getPossibleTradesWithoutSelfLoop(0, 1, possibleTrades_i, possibleTrades_j);
        // asserts
        assertEquals(possibleTrades_i, expectedPossibleTrades_i);
        assertEquals(possibleTrades_j, expectedPossibleTrades_j);
    }

    /**
     * The trade method should keep the degree (e.g. the size of the neighborhood sets)
     */
    @Test
    @DisplayName("Trade Method Keeps Degree")
    @RepeatedTest(50)
    void keepsDegree() {
        // number of nodes in the graph
        int n = 2;

        // initializing the nodes indices
        int[] nodesIndices = new int[n];
        for (int i = 0; i < n; i++) {
            nodesIndices[i] = i;
        }

        // initializing the adjacency list
        HashSet<Integer>[] adjacencyList = new HashSet[n];

        HashSet<Integer> neighbors_i = new HashSet<>(4, 1);
        neighbors_i.addAll(Arrays.asList(2, 4, 5, 6));

        adjacencyList[0] = neighbors_i;

        HashSet<Integer> neighbors_j = new HashSet<>(4, 1);
        neighbors_j.addAll(Arrays.asList(1, 3, 4, 5));

        adjacencyList[1] = neighbors_j;

        // initializing possible trades for both nodes
        HashSet<Integer> possibleTrades_i = new HashSet<>(2, 1);
        possibleTrades_i.addAll(Arrays.asList(2, 6));

        HashSet<Integer> possibleTrades_j = new HashSet<>(2, 1);
        possibleTrades_j.addAll(Arrays.asList(1, 3));

        // initializing de.uni.kl.cs.aalab.curveball implementation
        AdjacencyListImpl curveball = new AdjacencyListImpl(nodesIndices, adjacencyList, rnd);

        // method to test
        curveball.trade(0, 1, possibleTrades_i, possibleTrades_j);

        assertEquals(adjacencyList.length, n);
        assertEquals(adjacencyList[0].size(), 4);
        assertEquals(adjacencyList[1].size(), 4);
    }

    /**
     * The trade method should keep the degree (e.g. the size of the neighborhood sets)
     */
    @Test
    @DisplayName("Trade Method Keeps Degree")
    @RepeatedTest(50)
    void keepsDegreePiGtPj() {
        // number of nodes in the graph
        int n = 2;

        // initializing the nodes indices
        int[] nodesIndices = new int[n];
        for (int i = 0; i < n; i++) {
            nodesIndices[i] = i;
        }

        // initializing the adjacency list
        HashSet<Integer>[] adjacencyList = new HashSet[n];

        HashSet<Integer> neighbors_i = new HashSet<>(4, 1);
        neighbors_i.addAll(Arrays.asList(2, 4, 5, 6));

        adjacencyList[0] = neighbors_i;

        HashSet<Integer> neighbors_j = new HashSet<>(2, 1);
        neighbors_j.addAll(Arrays.asList(1, 4, 5));

        adjacencyList[1] = neighbors_j;

        // initializing possible trades for both nodes
        HashSet<Integer> possibleTrades_i = new HashSet<>(2, 1);
        possibleTrades_i.addAll(Arrays.asList(2, 6));

        HashSet<Integer> possibleTrades_j = new HashSet<>(2, 1);
        possibleTrades_j.add(1);

        // initializing de.uni.kl.cs.aalab.curveball implementation
        AdjacencyListImpl curveball = new AdjacencyListImpl(nodesIndices, adjacencyList, rnd);

        // method to test
        curveball.trade(0, 1, possibleTrades_i, possibleTrades_j);

        assertEquals(adjacencyList.length, n);
        assertEquals(adjacencyList[0].size(), 4);
        assertEquals(adjacencyList[1].size(), 3);
    }

    /**
     * The trade method should keep the degree (e.g. the size of the neighborhood sets)
     */
    @Test
    @DisplayName("Trade Method Keeps Degree")
    @RepeatedTest(50)
    void keepsDegreePiLtPj() {
        // number of nodes in the graph
        int n = 2;

        // initializing the nodes indices
        int[] nodesIndices = new int[n];
        for (int i = 0; i < n; i++) {
            nodesIndices[i] = i;
        }

        // initializing the adjacency list
        HashSet<Integer>[] adjacencyList = new HashSet[n];

        HashSet<Integer> neighbors_i = new HashSet<>(4, 1);
        neighbors_i.addAll(Arrays.asList(2, 4, 5));

        adjacencyList[0] = neighbors_i;

        HashSet<Integer> neighbors_j = new HashSet<>(2, 1);
        neighbors_j.addAll(Arrays.asList(1, 3, 4, 5));

        adjacencyList[1] = neighbors_j;

        // initializing possible trades for both nodes
        HashSet<Integer> possibleTrades_i = new HashSet<>(2, 1);
        possibleTrades_i.add(2);

        HashSet<Integer> possibleTrades_j = new HashSet<>(2, 1);
        possibleTrades_j.addAll(Arrays.asList(1, 3));

        // initializing de.uni.kl.cs.aalab.curveball implementation
        AdjacencyListImpl curveball = new AdjacencyListImpl(nodesIndices, adjacencyList, rnd);

        // method to test
        curveball.trade(0, 1, possibleTrades_i, possibleTrades_j);

        assertEquals(adjacencyList.length, n);
        assertEquals(adjacencyList[0].size(), 3);
        assertEquals(adjacencyList[1].size(), 4);
    }

    @Test
    @DisplayName("")
    void getAllPossibleTrades() {
        HashSet<Integer> possibleTrade_i = new HashSet<>();
        possibleTrade_i.addAll(Arrays.asList(0, 1, 2, 3));
        HashSet<Integer> possibleTrade_j = new HashSet<>();
        possibleTrade_j.addAll(Arrays.asList(4, 5, 6, 7));

        AdjacencyListImpl curveball = new AdjacencyListImpl(null, null, rnd);
        HashSet<Integer> possibleTrades = curveball.getAllPossibleTrades(possibleTrade_i, possibleTrade_j);

        assertEquals(possibleTrades.size(), 8);
        for (int t : possibleTrades) {
            assertTrue(possibleTrade_i.contains(t) || possibleTrade_j.contains(t));
        }
    }

    @Nested
    @DisplayName("Trading With Self-Loops")
    class TradingWithSelfLoop {

        @Test
        @DisplayName("Keeps Degree")
        @RepeatedTest(50)
        void keepsDegree() {
            // number of nodes in the graph
            int n = 2;

            // initializing the nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }

            // initializing the adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[n];

            HashSet<Integer> neighbors_i = new HashSet<>(4, 1);
            neighbors_i.addAll(Arrays.asList(2, 4, 5, 6));

            adjacencyList[0] = neighbors_i;

            HashSet<Integer> neighbors_j = new HashSet<>(4, 1);
            neighbors_j.addAll(Arrays.asList(1, 3, 4, 5));

            adjacencyList[1] = neighbors_j;

            // initializing de.uni.kl.cs.aalab.curveball implementation
            AdjacencyListImpl curveball = new AdjacencyListImpl(nodesIndices, adjacencyList, rnd);

            // method to test
            curveball.tradeWithSelfLoop(0, 1);

            assertEquals(adjacencyList.length, n);
            assertEquals(adjacencyList[0].size(), 4);
            assertEquals(adjacencyList[1].size(), 4);
        }

        @Test
        @DisplayName("Does nothing when no trade possible")
        void fixedPoint() {
            // number of nodes in the graph
            int n = 2;

            // initializing the nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }

            // initializing the adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[n];

            HashSet<Integer> neighbors = new HashSet<>(4, 1);
            neighbors.addAll(Arrays.asList(2, 4, 5, 6));

            adjacencyList[0] = neighbors;
            adjacencyList[1] = neighbors;

            // initializing de.uni.kl.cs.aalab.curveball implementation
            AdjacencyListImpl curveball = new AdjacencyListImpl(nodesIndices, adjacencyList, rnd);
            curveball.tradeWithSelfLoop(0, 1);
            assertEquals(adjacencyList.length, n);
            assertEquals(adjacencyList[0], neighbors);
            assertEquals(adjacencyList[1], neighbors);
        }

        @Test
        @RepeatedTest(50)
        @DisplayName("computes the curveballAndCooccurrence of nodes i and j while trading")
        void cooccurenceWhileTradeWithSelfLoop() {

            // number of nodes in the graph
            int n = 2;

            // initializing the nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }

            // initializing the adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[n];
            HashSet<Integer> neighbors_i = new HashSet<>(4, 1);
            neighbors_i.addAll(Arrays.asList(2, 4, 5, 6));
            adjacencyList[0] = neighbors_i;
            HashSet<Integer> neighbors_j = new HashSet<>(4, 1);
            neighbors_j.addAll(Arrays.asList(1, 3, 4, 5));
            adjacencyList[1] = neighbors_j;

            // initializing de.uni.kl.cs.aalab.curveball implementation
            AdjacencyListImpl curveball = new AdjacencyListImpl(nodesIndices, adjacencyList, rnd);

            // method to test
            int coocc = curveball.cooccurenceWhileTradeWithSelfLoop(0, 1);

            assertEquals(coocc, 2);
        }
    }

    @Nested
    @DisplayName("Trading Without Self-Loops")
    class TradingWithoutSelfLoop {

        @Test
        @DisplayName("Keeps Degree")
        @RepeatedTest(50)
        void keepsDegree() {
            // number of nodes in the graph
            int n = 2;

            // initializing the nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }

            // initializing the adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[n];

            HashSet<Integer> neighbors_i = new HashSet<>(4, 1);
            neighbors_i.addAll(Arrays.asList(2, 4, 5, 6));

            adjacencyList[0] = neighbors_i;

            HashSet<Integer> neighbors_j = new HashSet<>(4, 1);
            neighbors_j.addAll(Arrays.asList(1, 3, 4, 5));

            adjacencyList[1] = neighbors_j;

            // initializing de.uni.kl.cs.aalab.curveball implementation
            AdjacencyListImpl curveball = new AdjacencyListImpl(nodesIndices, adjacencyList, rnd);

            // method to test
            curveball.tradeWithoutSelfLoop(0, 1);

            assertEquals(adjacencyList.length, n);
            assertEquals(adjacencyList[0].size(), 4);
            assertEquals(adjacencyList[1].size(), 4);
        }

        @Test
        @DisplayName("Does nothing when no trade possible")
        void fixedPoint() {
            // number of nodes in the graph
            int n = 2;

            // initializing the nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }

            // initializing the adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[n];

            HashSet<Integer> neighbors = new HashSet<>(4, 1);
            neighbors.addAll(Arrays.asList(2, 4, 5, 6));

            adjacencyList[0] = neighbors;
            adjacencyList[1] = neighbors;

            // initializing de.uni.kl.cs.aalab.curveball implementation
            AdjacencyListImpl curveball = new AdjacencyListImpl(nodesIndices, adjacencyList, rnd);

            curveball.tradeWithoutSelfLoop(0, 1);

            assertEquals(adjacencyList.length, n);
            assertEquals(adjacencyList[0], neighbors);
            assertEquals(adjacencyList[1], neighbors);
        }

        /**
         * We place ourselves in a case where the only trade possible would lead to a self-loop.
         * Since the method we test does not allow self-loops, then we should see no trade at all.
         */
        @Test
        @DisplayName("Does not allow self-loop")
        @RepeatedTest(50)
        void noSelfLoop() {
            // number of nodes in the graph
            int n = 2;

            // initializing the nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }

            // initializing the adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[n];

            HashSet<Integer> neighbors_i = new HashSet<>(4, 1);
            neighbors_i.addAll(Arrays.asList(1, 4, 5, 6));

            adjacencyList[0] = neighbors_i;

            HashSet<Integer> neighbors_j = new HashSet<>(4, 1);
            neighbors_j.addAll(Arrays.asList(0, 4, 5, 6));

            adjacencyList[1] = neighbors_j;

            // initializing de.uni.kl.cs.aalab.curveball implementation
            AdjacencyListImpl curveball = new AdjacencyListImpl(nodesIndices, adjacencyList, rnd);

            // method to test
            curveball.tradeWithoutSelfLoop(0, 1);

            assertEquals(adjacencyList.length, n);
            assertEquals(adjacencyList[0], neighbors_i);
            assertEquals(adjacencyList[1], neighbors_j);
        }

        @Test
        @RepeatedTest(50)
        @DisplayName("computes the curveballAndCooccurrence of nodes i and j while trading")
        void cooccurenceWhileTradeWithoutSelfLoop() {

            // number of nodes in the graph
            int n = 2;

            // initializing the nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }

            // initializing the adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[n];
            HashSet<Integer> neighbors_i = new HashSet<>(4, 1);
            neighbors_i.addAll(Arrays.asList(2, 4, 5, 6));
            adjacencyList[0] = neighbors_i;
            HashSet<Integer> neighbors_j = new HashSet<>(4, 1);
            neighbors_j.addAll(Arrays.asList(1, 3, 4, 5));
            adjacencyList[1] = neighbors_j;

            // initializing de.uni.kl.cs.aalab.curveball implementation
            AdjacencyListImpl curveball = new AdjacencyListImpl(nodesIndices, adjacencyList, rnd);

            // method to test
            int coocc = curveball.cooccurrenceWhileTradeWithoutSelfLoop(0, 1);

            assertEquals(coocc, 2);
        }
    }
}