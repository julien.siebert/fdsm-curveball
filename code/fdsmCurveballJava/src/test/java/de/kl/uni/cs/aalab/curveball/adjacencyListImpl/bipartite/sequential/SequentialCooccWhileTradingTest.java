package de.kl.uni.cs.aalab.curveball.adjacencyListImpl.bipartite.sequential;

import org.junit.jupiter.api.*;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SequentialCooccWhileTradingTest {

    private static Random rnd;
    private static Path resourcesDirectory;

    @BeforeAll
    static void init() {
        rnd = new Random(0);
        resourcesDirectory = Paths.get("src", "test", "resources");
    }


    @Nested
    @DisplayName("Step")
    class Step {

        @Test
        @DisplayName("Works for odd number of nodes")
        void oddNbNodes() {
            // number of nodes
            int n = 5;

            // initializing list of nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }
            // initializing adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[n];

            HashSet<Integer> neighbors = new HashSet<>(4, 1.0f);
            neighbors.addAll(Arrays.asList(1, 2, 3, 4));
            adjacencyList[0] = neighbors;

            neighbors = new HashSet<>(4, 1.0f);
            neighbors.addAll(Arrays.asList(0, 1, 3, 4));
            adjacencyList[1] = neighbors;

            neighbors = new HashSet<>(4, 1.0f);
            neighbors.addAll(Arrays.asList(0, 1, 2, 4));
            adjacencyList[2] = neighbors;

            neighbors = new HashSet<>(4, 1.0f);
            neighbors.addAll(Arrays.asList(0, 1, 2, 3));
            adjacencyList[3] = neighbors;

            neighbors = new HashSet<>(4, 1.0f);
            neighbors.addAll(Arrays.asList(0, 1, 2, 3));
            adjacencyList[4] = neighbors;

            // initializing de.uni.kl.cs.aalab.curveball implementation
            Path cooccurrenceFilename = resourcesDirectory.resolve(Paths.get("coocc.txt"));
            SequentialCooccWhileTrading curveball = new SequentialCooccWhileTrading(nodesIndices, adjacencyList, rnd, cooccurrenceFilename);

            // calling step method (this shall not raise any error)
            try {
                curveball.step();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Test
        @DisplayName("Works for even number of nodes")
        void evenNbNodes() {
            // number of nodes
            int n = 6;

            // initializing list of nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }
            // initializing adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[n];

            HashSet<Integer> neighbors = new HashSet<>(n);

            neighbors.addAll(Arrays.asList(1, 2, 3, 4, 5));
            adjacencyList[0] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.addAll(Arrays.asList(0, 2, 3, 4, 5));
            adjacencyList[1] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.addAll(Arrays.asList(0, 1, 3, 4, 5));
            adjacencyList[2] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.addAll(Arrays.asList(0, 1, 2, 4, 5));
            adjacencyList[3] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.addAll(Arrays.asList(0, 1, 2, 3, 5));
            adjacencyList[4] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.addAll(Arrays.asList(0, 1, 2, 3, 4));
            adjacencyList[5] = neighbors;

            // initializing de.uni.kl.cs.aalab.curveball implementation
            Path cooccurrenceFilename = resourcesDirectory.resolve(Paths.get("coocc.txt"));
            SequentialCooccWhileTrading curveball = new SequentialCooccWhileTrading(nodesIndices, adjacencyList, rnd, cooccurrenceFilename);

            // calling step method (this shall not raise any error)
            try {
                curveball.step();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @RepeatedTest(100)
        @DisplayName("keeps degree sequence fixed")
        void keepsDegreeSequence() {
            // number of nodes
            int n = 6;

            // initializing list of nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }

            // initializing adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[n];

            HashSet<Integer> neighbors = new HashSet<>(n);

            neighbors.addAll(Arrays.asList(1, 2));
            adjacencyList[0] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.addAll(Arrays.asList(0, 2, 3));
            adjacencyList[1] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.addAll(Arrays.asList(0, 1, 3, 4));
            adjacencyList[2] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.addAll(Arrays.asList(0, 1, 2, 4, 5));
            adjacencyList[3] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.addAll(Arrays.asList(0, 1, 2));
            adjacencyList[4] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.add(0);
            adjacencyList[5] = neighbors;


            // degree sequence
            ArrayList<Integer> degreeSequence = new ArrayList<>(n);
            degreeSequence.addAll(Arrays.asList(2, 3, 4, 5, 3, 1));

            // initializing de.uni.kl.cs.aalab.curveball implementation
            Path cooccurrenceFilename = resourcesDirectory.resolve(Paths.get("coocc.txt"));
            SequentialCooccWhileTrading curveball = new SequentialCooccWhileTrading(nodesIndices, adjacencyList, rnd, cooccurrenceFilename);

            // calling step method (this shall not raise any error)
            try {
                curveball.step();
                for (int i = 0; i < n; i++) {
                    int expectedDegree = degreeSequence.get(i);
                    int actualDegree = adjacencyList[i].size();
                    assertEquals(expectedDegree, actualDegree);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }
}