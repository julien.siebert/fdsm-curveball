package de.kl.uni.cs.aalab.curveball.adjacencyListImpl.bipartite.sequential;

import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Classical Curveball Algorithm Implementation, " +
        "Using Adjacency List, " +
        "Sequential Implementation, " +
        "Allowing Self-loops")
class SequentialImplTest {

    private static Random rnd;

    @BeforeAll
    static void init() {
        rnd = new Random(0);
    }


    @Nested
    @DisplayName("Step")
    class Step {

        @Test
        @DisplayName("Works for odd number of nodes")
        void oddNbNodes() {
            // number of nodes
            int n = 5;

            // initializing list of nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }
            // initializing adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[n];

            HashSet<Integer> neighbors = new HashSet<>(4, 1.0f);
            neighbors.addAll(Arrays.asList(1, 2, 3, 4));
            adjacencyList[0] = neighbors;

            neighbors = new HashSet<>(4, 1.0f);
            neighbors.addAll(Arrays.asList(0, 1, 3, 4));
            adjacencyList[1] = neighbors;

            neighbors = new HashSet<>(4, 1.0f);
            neighbors.addAll(Arrays.asList(0, 1, 2, 4));
            adjacencyList[2] = neighbors;

            neighbors = new HashSet<>(4, 1.0f);
            neighbors.addAll(Arrays.asList(0, 1, 2, 3));
            adjacencyList[3] = neighbors;

            neighbors = new HashSet<>(4, 1.0f);
            neighbors.addAll(Arrays.asList(0, 1, 2, 3));
            adjacencyList[4] = neighbors;

            // initializing de.uni.kl.cs.aalab.curveball implementation
            SequentialImpl curveball = new SequentialImpl(nodesIndices, adjacencyList, rnd);

            // calling step method (this shall not raise any error)
            curveball.step();
        }

        @Test
        @DisplayName("Works for even number of nodes")
        void evenNbNodes() {
            // number of nodes
            int n = 6;

            // initializing list of nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }
            // initializing adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[n];

            HashSet<Integer> neighbors = new HashSet<>(n);

            neighbors.addAll(Arrays.asList(1, 2, 3, 4, 5));
            adjacencyList[0] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.addAll(Arrays.asList(0, 2, 3, 4, 5));
            adjacencyList[1] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.addAll(Arrays.asList(0, 1, 3, 4, 5));
            adjacencyList[2] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.addAll(Arrays.asList(0, 1, 2, 4, 5));
            adjacencyList[3] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.addAll(Arrays.asList(0, 1, 2, 3, 5));
            adjacencyList[4] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.addAll(Arrays.asList(0, 1, 2, 3, 4));
            adjacencyList[5] = neighbors;

            // initializing curveball implementation
            SequentialImpl curveball = new SequentialImpl(nodesIndices, adjacencyList, rnd);

            // calling step method (this shall not raise any error)
            curveball.step();
        }

        @RepeatedTest(100)
        @DisplayName("keeps degree sequence fixed")
        void keepsDegreeSequence() {
            // number of nodes
            int n = 6;

            // initializing list of nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }

            // initializing adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[n];

            HashSet<Integer> neighbors = new HashSet<>(n);

            neighbors.addAll(Arrays.asList(1, 2));
            adjacencyList[0] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.addAll(Arrays.asList(0, 2, 3));
            adjacencyList[1] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.addAll(Arrays.asList(0, 1, 3, 4));
            adjacencyList[2] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.addAll(Arrays.asList(0, 1, 2, 4, 5));
            adjacencyList[3] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.addAll(Arrays.asList(0, 1, 2));
            adjacencyList[4] = neighbors;

            neighbors = new HashSet<>(n);
            neighbors.add(0);
            adjacencyList[5] = neighbors;


            // degree sequence
            ArrayList<Integer> degreeSequence = new ArrayList<>(n);
            degreeSequence.addAll(Arrays.asList(2, 3, 4, 5, 3, 1));

            // initializing de.uni.kl.cs.aalab.curveball implementation
            SequentialImpl curveball = new SequentialImpl(nodesIndices, adjacencyList, rnd);

            // calling de.uni.kl.cs.aalab.curveball step method
            curveball.step();
            for (int i = 0; i < n; i++) {
                int expectedDegree = degreeSequence.get(i);
                int actualDegree = adjacencyList[i].size();
                assertEquals(expectedDegree, actualDegree);
            }
        }

    }
}