package de.kl.uni.cs.aalab.curveball.adjacencyListImpl.bipartite.thread;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class ThreadImplTest {

    private static Random rnd;

    @BeforeAll
    static void init() {
        rnd = new Random(0);
    }

    @Test
    @DisplayName("Creates a list of task with the right number of elements per task")
    void creation() {
        // number of nodes
        int n = 6;

        // initializing list of nodes indices
        int[] nodesIndices = new int[n];
        for (int i = 0; i < n; i++) {
            nodesIndices[i] = i;
        }
        // initializing adjacency list
        HashSet<Integer>[] adjacencyList = new HashSet[n];
        for (int i = 0; i < n; i++) {
            adjacencyList[i] = new HashSet(n, 1.0f);
        }

        adjacencyList[0].addAll(Arrays.asList(1, 2));
        adjacencyList[1].addAll(Arrays.asList(0, 3));
        adjacencyList[2].addAll(Arrays.asList(0, 1, 3));
        adjacencyList[3].addAll(Arrays.asList(2, 4, 5));
        adjacencyList[4].addAll(Arrays.asList(0, 1, 2, 3));
        adjacencyList[5].addAll(Arrays.asList(4, 6, 7, 8));

        ThreadImpl curveball = new ThreadImpl(2, nodesIndices, adjacencyList, rnd);
        ArrayList<ParallelTradingTask> tasks = curveball.getTasks();
        assertEquals(2, tasks.size());
        assertEquals(0, tasks.get(0).getStartIndex());
        assertEquals(3, tasks.get(0).getEndIndex());
        assertEquals(3, tasks.get(1).getStartIndex());
        assertEquals(6, tasks.get(1).getEndIndex());
    }

    @Test
    @DisplayName("Keeps Degree Sequence")
    @RepeatedTest(100)
    void keepsDegreeSequence() {
        // number of nodes
        int n = 6;

        // initializing list of nodes indices
        int[] nodesIndices = new int[n];
        for (int i = 0; i < n; i++) {
            nodesIndices[i] = i;
        }
        // initializing adjacency list
        HashSet<Integer>[] adjacencyList = new HashSet[n];
        for (int i = 0; i < n; i++) {
            adjacencyList[i] = new HashSet<>(n, 1.0f);
        }

        adjacencyList[0].addAll(Arrays.asList(1, 2));
        adjacencyList[1].addAll(Arrays.asList(0, 3));
        adjacencyList[2].addAll(Arrays.asList(0, 1, 3));
        adjacencyList[3].addAll(Arrays.asList(2, 4, 5));
        adjacencyList[4].addAll(Arrays.asList(0, 1, 2, 3));
        adjacencyList[5].addAll(Arrays.asList(4, 6, 7, 8));

        ThreadImpl curveball = new ThreadImpl(2, nodesIndices, adjacencyList, rnd);
        try {
            curveball.step();
        } catch (InterruptedException e) {
            e.printStackTrace();
            fail("Should not throw exception");
        }
        curveball.shutdown();

        // check that the degree sequence is conserved
        assertEquals(2, adjacencyList[0].size());
        assertEquals(2, adjacencyList[1].size());
        assertEquals(3, adjacencyList[2].size());
        assertEquals(3, adjacencyList[3].size());
        assertEquals(4, adjacencyList[4].size());
        assertEquals(4, adjacencyList[5].size());

    }

    @Test
    @DisplayName("Runs more than one steps")
    void run2Steps() {
        // number of nodes
        int n = 6;

        // initializing list of nodes indices
        int[] nodesIndices = new int[n];
        for (int i = 0; i < n; i++) {
            nodesIndices[i] = i;
        }
        // initializing adjacency list
        HashSet<Integer>[] adjacencyList = new HashSet[n];
        for (int i = 0; i < n; i++) {
            adjacencyList[i] = new HashSet<>(n, 1.0f);
        }

        adjacencyList[0].addAll(Arrays.asList(1, 2));
        adjacencyList[1].addAll(Arrays.asList(0, 3));
        adjacencyList[2].addAll(Arrays.asList(0, 1, 3));
        adjacencyList[3].addAll(Arrays.asList(2, 4, 5));
        adjacencyList[4].addAll(Arrays.asList(0, 1, 2, 3));
        adjacencyList[5].addAll(Arrays.asList(4, 6, 7, 8));

        ThreadImpl curveball = new ThreadImpl(2, nodesIndices, adjacencyList, rnd);
        try {
            curveball.step();
        } catch (InterruptedException e) {
            e.printStackTrace();
            fail("Should not throw exception");
        }
        try {
            curveball.step();
        } catch (InterruptedException e) {
            e.printStackTrace();
            fail("Should not throw exception");
        }
        curveball.shutdown();

        // check that the degree sequence is conserved
        assertEquals(2, adjacencyList[0].size());
        assertEquals(2, adjacencyList[1].size());
        assertEquals(3, adjacencyList[2].size());
        assertEquals(3, adjacencyList[3].size());
        assertEquals(4, adjacencyList[4].size());
        assertEquals(4, adjacencyList[5].size());
    }
}