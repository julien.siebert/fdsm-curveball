package de.kl.uni.cs.aalab.curveball.adjacencyListImpl.directed.thread;

import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class ParallelTradingTaskTest {
    private static Random rnd;

    @BeforeAll
    static void init() {
        rnd = new Random(0);
    }

    @Nested
    @DisplayName("Trade a subset of the adjacency list")
    class Run {


        @Test
        @DisplayName("Keeps the degree sequence")
        @RepeatedTest(100)
        void keepsDegreeSequence() {
            // number of nodes
            int n = 6;

            // initializing list of nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }
            // initializing adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[n];

            HashSet<Integer> neighbors;

            neighbors = new HashSet<>(n, 1.0f);
            neighbors.addAll(Arrays.asList(1, 2));
            adjacencyList[0] = neighbors;

            neighbors = new HashSet<>(n, 1.0f);
            neighbors.addAll(Arrays.asList(0, 3));
            adjacencyList[1] = neighbors;

            neighbors = new HashSet<>(n, 1.0f);
            neighbors.addAll(Arrays.asList(0, 1, 3));
            adjacencyList[2] = neighbors;

            neighbors = new HashSet<>(n, 1.0f);
            neighbors.addAll(Arrays.asList(2, 4, 5));
            adjacencyList[3] = neighbors;

            neighbors = new HashSet<>(n, 1.0f);
            neighbors.addAll(Arrays.asList(0, 1, 2, 3));
            adjacencyList[4] = neighbors;

            neighbors = new HashSet<>(n, 1.0f);
            neighbors.addAll(Arrays.asList(4, 6, 7, 8));
            adjacencyList[5] = neighbors;

            ParallelTradingTask aTask = new ParallelTradingTask(nodesIndices, adjacencyList, rnd, 0, 3);

            // method to test
            try {
                aTask.call();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Should not throw exception");
            }

            assertEquals(2, adjacencyList[0].size());
            assertEquals(2, adjacencyList[1].size());
            assertEquals(3, adjacencyList[2].size());
            assertEquals(3, adjacencyList[3].size());
            assertEquals(4, adjacencyList[4].size());
            assertEquals(4, adjacencyList[5].size());
        }

        @Test
        @DisplayName("Trades Only a Subset of the adjacency list")
        void tradesSubset() {
            // number of nodes
            int n = 6;

            // initializing list of nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }
            // initializing adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[n];
            HashSet<Integer> neighbors;
            for (int i = 0; i < n; i++) {
                neighbors = new HashSet<>(1, 1);
                neighbors.add(i);
                adjacencyList[i]  = neighbors;
            }

            ParallelTradingTask aTask = new ParallelTradingTask(nodesIndices, adjacencyList, rnd, 0, 3);

            // method to test
            int nbTrades = 0;

            for (int i = 0; i < 100; i++) {
                try {
                    aTask.call();
                    for (int j = 0; j < n; j++) {
                        if (!adjacencyList[j].contains(j)) {
                            nbTrades++;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    fail("Should not throw exception");
                }
            }

            assertTrue(nbTrades > 0);
        }
    }
}