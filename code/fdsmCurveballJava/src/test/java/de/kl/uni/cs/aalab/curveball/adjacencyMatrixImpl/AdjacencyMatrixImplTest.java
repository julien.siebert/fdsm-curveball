package de.kl.uni.cs.aalab.curveball.adjacencyMatrixImpl;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class AdjacencyMatrixImplTest {
    private static Random rnd;

    @BeforeAll
    static void init() {
        rnd = new Random(0);
    }

    /**
     * Testing method random split
     * <p>
     * Example
     * <p>
     * BitSet to split = [0,0,1,1,0,0,1,1]
     * <p>
     * number of ones in both arrays = 2
     * <p>
     * would possibly Returns
     * [0,0,1,0,0,0,0,1], [0,0,0,1,0,0,1,0]
     * <p>
     * What is tested:
     * <p>
     * There should be 2 'ones' in the first array returned and 2 'ones' in the second array returned
     * <p>
     * The BitSet to split == [0,0,1,0,0,0,0,1] OR [0,0,0,1,0,0,1,0]
     */
    @Test
    @DisplayName("Randomly splits a BitSet in to two BitSets with the right number of ones in each")
    @RepeatedTest(100)
    void randomSplit() {
        AdjacencyMatrixImpl curveball = new AdjacencyMatrixImpl(null, null, rnd);

        int n = 8;
        BitSet toSplit = new BitSet(n);
        // [0,0,1,1,0,0,1,1]
        toSplit.clear();
        toSplit.set(2);
        toSplit.set(3);
        toSplit.set(6);
        toSplit.set(7);

        int nbOnesInFirst = 2;

        BitSet[] res = curveball.randomSplit(toSplit, nbOnesInFirst);
        BitSet first = res[0];
        BitSet second = res[1];

        assertEquals(2, first.cardinality());
        assertEquals(2, second.cardinality());

        first.or(second);

        assertEquals(toSplit, first);
    }

    @Test
    @DisplayName("Do not trade when no trade can be performed")
    void noTradePossible() {

        // number of nodes in the graph
        int n = 2;

        // initializing the nodes indices
        int[] nodesIndices = new int[n];
        for (int i = 0; i < n; i++) {
            nodesIndices[i] = i;
        }

        // initializing the adjacency matrix
        ArrayList<BitSet> adjacencyMatrix = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            BitSet row = new BitSet(n);
            row.clear();
            row.flip(0, n);
            adjacencyMatrix.add(row);
        }

        AdjacencyMatrixImpl curveball = new AdjacencyMatrixImpl(nodesIndices, adjacencyMatrix, rnd);
        curveball.tradeWithSelfLoop(0, 1);

        assertEquals(adjacencyMatrix.get(0), adjacencyMatrix.get(1));
    }

    @Test
    @DisplayName("Trade when single trade can be performed")
    void singleTradePossible() {


        // initializing the nodes indices
        int n = 2;

        int[] nodesIndices = new int[n];
        for (int i = 0; i < n; i++) {
            nodesIndices[i] = i;
        }

        // initializing the adjacency matrix
        ArrayList<BitSet> adjacencyMatrix = new ArrayList<>(2);

        BitSet firstRow = new BitSet(2);
        firstRow.clear();
        firstRow.set(1);
        adjacencyMatrix.add(firstRow);

        BitSet secondRow = new BitSet(2);
        secondRow.clear();
        secondRow.set(0);
        adjacencyMatrix.add(secondRow);

        AdjacencyMatrixImpl curveball = new AdjacencyMatrixImpl(nodesIndices, adjacencyMatrix, rnd);
        curveball.tradeWithSelfLoop(0, 1);

        assertFalse(adjacencyMatrix.get(0).intersects(adjacencyMatrix.get(1)));
    }
}