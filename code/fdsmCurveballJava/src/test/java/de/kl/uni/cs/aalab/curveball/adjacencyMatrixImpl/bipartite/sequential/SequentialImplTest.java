package de.kl.uni.cs.aalab.curveball.adjacencyMatrixImpl.bipartite.sequential;


import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Classical Curveball Algorithm Implementation, " +
        "Using Adjacency Matrix, " +
        "Allowing Self-loops")
class SequentialImplTest {

    private static Random rnd;

    @BeforeAll
    static void init() {
        rnd = new Random(0);
    }


    @Nested
    @DisplayName("Step")
    class Step {

        @Test
        @DisplayName("Works for odd number of nodes")
        void oddNbNodes() {
            // number of nodes
            int n = 5;

            // initializing the nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }

            // initializing the adjacency matrix
            ArrayList<BitSet> adjacencyMatrix = new ArrayList<>(n);

            BitSet row = new BitSet(n);
            row.clear();
            row.set(0);
            row.set(1);
            adjacencyMatrix.add(row);

            row = new BitSet(n);
            row.clear();
            row.set(0);
            row.set(1);
            row.set(2);
            adjacencyMatrix.add(row);

            row = new BitSet(n);
            row.clear();
            row.set(1);
            row.set(2);
            row.set(3);
            adjacencyMatrix.add(row);

            row = new BitSet(n);
            row.clear();
            row.set(2);
            row.set(3);
            row.set(4);
            adjacencyMatrix.add(row);

            row = new BitSet(n);
            row.clear();
            row.set(3);
            row.set(4);
            adjacencyMatrix.add(row);

            // initializing de.uni.kl.cs.aalab.curveball implementation
            SequentialImpl curveball = new SequentialImpl(nodesIndices, adjacencyMatrix, rnd);

            // calling de.uni.kl.cs.aalab.curveball step method (this shall not raise any error)
            curveball.step();
        }

        @org.junit.jupiter.api.Test
        @DisplayName("Works for even number of nodes")
        void evenNbNodes() {
            // number of nodes
            int n = 6;

            // initializing the nodes indices
int[] nodesIndices = new int[n]; for (int i = 0; i < n; i++){ nodesIndices[i] = i; }

            // initializing the adjacency matrix
            ArrayList<BitSet> adjacencyMatrix = new ArrayList<>(n);

            BitSet row = new BitSet(n);
            row.clear();
            row.set(0);
            row.set(1);
            adjacencyMatrix.add(row);

            row = new BitSet(n);
            row.clear();
            row.set(0);
            row.set(1);
            row.set(2);
            adjacencyMatrix.add(row);

            row = new BitSet(n);
            row.clear();
            row.set(1);
            row.set(2);
            row.set(3);
            adjacencyMatrix.add(row);

            row = new BitSet(n);
            row.clear();
            row.set(2);
            row.set(3);
            row.set(4);
            adjacencyMatrix.add(row);

            row = new BitSet(n);
            row.clear();
            row.set(3);
            row.set(4);
            row.set(5);
            adjacencyMatrix.add(row);

            row = new BitSet(n);
            row.clear();
            row.set(4);
            row.set(5);
            adjacencyMatrix.add(row);

            // initializing de.uni.kl.cs.aalab.curveball implementation
            SequentialImpl curveball = new SequentialImpl(nodesIndices, adjacencyMatrix, rnd);

            // calling de.uni.kl.cs.aalab.curveball step method (this shall not raise any error)
            curveball.step();
        }

        @RepeatedTest(100)
        @DisplayName("keeps degree sequence fixed")
        void keepsDegreeSequence() {
            // number of nodes
            int n = 6;

            // initializing the nodes indices
int[] nodesIndices = new int[n]; for (int i = 0; i < n; i++){ nodesIndices[i] = i; }

            // initializing the degree sequence
            ArrayList<Integer> degreeSequence = new ArrayList<>(n);
            degreeSequence.add(2);
            degreeSequence.add(3);
            degreeSequence.add(3);
            degreeSequence.add(3);
            degreeSequence.add(3);
            degreeSequence.add(2);

            // initializing the adjacency matrix
            ArrayList<BitSet> adjacencyMatrix = new ArrayList<>(n);

            BitSet row = new BitSet(n);
            row.clear();
            row.set(0);
            row.set(1);
            adjacencyMatrix.add(row);

            row = new BitSet(n);
            row.clear();
            row.set(0);
            row.set(1);
            row.set(2);
            adjacencyMatrix.add(row);

            row = new BitSet(n);
            row.clear();
            row.set(1);
            row.set(2);
            row.set(3);
            adjacencyMatrix.add(row);

            row = new BitSet(n);
            row.clear();
            row.set(2);
            row.set(3);
            row.set(4);
            adjacencyMatrix.add(row);

            row = new BitSet(n);
            row.clear();
            row.set(3);
            row.set(4);
            row.set(5);
            adjacencyMatrix.add(row);

            row = new BitSet(n);
            row.clear();
            row.set(4);
            row.set(5);
            adjacencyMatrix.add(row);

            // initializing de.uni.kl.cs.aalab.curveball implementation
            SequentialImpl curveball = new SequentialImpl(nodesIndices, adjacencyMatrix, rnd);

            // calling de.uni.kl.cs.aalab.curveball step method
            curveball.step();
            for (int i = 0; i < n; i++) {
                int expectedDegree = degreeSequence.get(i);
                int actualDegree = adjacencyMatrix.get(i).cardinality();
                assertEquals(expectedDegree, actualDegree);
            }
        }

    }
}