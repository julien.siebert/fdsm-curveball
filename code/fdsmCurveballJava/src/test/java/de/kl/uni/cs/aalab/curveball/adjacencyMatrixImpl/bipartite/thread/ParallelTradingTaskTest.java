package de.kl.uni.cs.aalab.curveball.adjacencyMatrixImpl.bipartite.thread;

import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class ParallelTradingTaskTest {

    private static Random rnd;

    @BeforeAll
    static void init() {
        rnd = new Random(0);
    }

    @Nested
    @DisplayName("Trade a subset of the adjacency list")
    class Run {


        @Test
        @DisplayName("Keeps the degree sequence")
        @RepeatedTest(100)
        void keepsDegreeSequence() {
            // number of nodes
            int n = 6;

            // initializing the nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }

            // initializing the adjacency matrix
            ArrayList<BitSet> adjacencyMatrix = new ArrayList<>(n);

            BitSet row = new BitSet(n);
            row.clear();
            row.set(0);
            row.set(1);
            adjacencyMatrix.add(row);

            row = new BitSet(n);
            row.clear();
            row.set(0);
            row.set(1);
            row.set(2);
            adjacencyMatrix.add(row);

            row = new BitSet(n);
            row.clear();
            row.set(1);
            row.set(2);
            row.set(3);
            adjacencyMatrix.add(row);

            row = new BitSet(n);
            row.clear();
            row.set(2);
            row.set(3);
            row.set(4);
            adjacencyMatrix.add(row);

            row = new BitSet(n);
            row.clear();
            row.set(3);
            row.set(4);
            row.set(5);
            adjacencyMatrix.add(row);

            row = new BitSet(n);
            row.clear();
            row.set(4);
            row.set(5);
            adjacencyMatrix.add(row);

            ParallelTradingTask aTask = new ParallelTradingTask(nodesIndices, adjacencyMatrix, rnd, 0, 3);

            // method to test
            try {
                aTask.call();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Should not throw exception");
            }

            assertEquals(2, adjacencyMatrix.get(0).cardinality());
            assertEquals(3, adjacencyMatrix.get(1).cardinality());
            assertEquals(3, adjacencyMatrix.get(2).cardinality());
            assertEquals(3, adjacencyMatrix.get(3).cardinality());
            assertEquals(3, adjacencyMatrix.get(4).cardinality());
            assertEquals(2, adjacencyMatrix.get(5).cardinality());
        }

        @Test
        @DisplayName("Trades Only a Subset of the adjacency list")
        /**
         * Checks that at least one trade happened
         */
        void tradesSubset() {
            // number of nodes
            int n = 6;

            // initializing the nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }

            // initializing the adjacency matrix
            ArrayList<BitSet> adjacencyMatrix = new ArrayList<>(n);

            BitSet row;
            for (int i = 0; i < n; i++) {
                row = new BitSet();
                row.clear();
                row.set(i);
                adjacencyMatrix.add(row);
            }

            ParallelTradingTask aTask = new ParallelTradingTask(nodesIndices, adjacencyMatrix, rnd, 0, 3);

            // method to test
            int nbTrades = 0;

            for (int i = 0; i < 100; i++) {
                try {
                    aTask.call();
                    for (int j = 0; j < n; j++) {
                        if (!adjacencyMatrix.get(j).get(j)) {
                            nbTrades++;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    fail("Should not throw exception");
                }
            }

            assertTrue(nbTrades > 0);
        }
    }
}