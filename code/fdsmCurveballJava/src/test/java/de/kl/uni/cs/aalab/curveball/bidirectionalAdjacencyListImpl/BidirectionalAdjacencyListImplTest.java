package de.kl.uni.cs.aalab.curveball.bidirectionalAdjacencyListImpl;

import de.kl.uni.cs.aalab.datastructure.BidirectionalAdjacencyList;
import org.junit.jupiter.api.*;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BidirectionalAdjacencyListImplTest {

    private static Random rnd;

    @BeforeAll
    static void init() {
        rnd = new Random(0);
    }

    @Test
    @DisplayName("getting possible trade allowing self loop")
    void getPossibleTradesWithSelfLoop() {
        // init possible trades set
        HashSet<Integer> possibleTrades_i = new HashSet<Integer>();
        HashSet<Integer> possibleTrades_j = new HashSet<Integer>();
        // expected possible trades
        HashSet<Integer> expectedPossibleTrades_i = new HashSet<Integer>();
        HashSet<Integer> expectedPossibleTrades_j = new HashSet<Integer>();
        expectedPossibleTrades_i.addAll(Arrays.asList(0, 1, 2));
        expectedPossibleTrades_j.addAll(Arrays.asList(5, 6, 7));
        // init bidirectional adjacency list
        HashSet<Integer>[] adjacencyList = new HashSet[2];
        HashSet<Integer> neighbors_i = new HashSet<>(5, 1.0f);
        HashSet<Integer> neighbors_j = new HashSet<>(5, 1.0f);
        neighbors_i.addAll(Arrays.asList(0, 1, 2, 3, 4));
        neighbors_j.addAll(Arrays.asList(3, 4, 5, 6, 7));
        adjacencyList[0] = neighbors_i;
        adjacencyList[1] = neighbors_j;
        BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, 8);
        // init curveball
        BidirectionalAdjacencyListImpl curveball = new BidirectionalAdjacencyListImpl(bidirectionalAdjacencyList, rnd);
        // function to test
        curveball.getPossibleTradesWithSelfLoop(0, 1, possibleTrades_i, possibleTrades_j);
        // asserts
        assertEquals(possibleTrades_i, expectedPossibleTrades_i);
        assertEquals(possibleTrades_j, expectedPossibleTrades_j);
    }

    @Test
    @DisplayName("getting possible trade allowing self loop")
    void getPossibleTradesRightWithSelfLoop() {
        BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(3, 3);
        bidirectionalAdjacencyList.addEdge(0, 0);
        bidirectionalAdjacencyList.addEdge(1, 1);
        bidirectionalAdjacencyList.addEdge(2, 1);
        bidirectionalAdjacencyList.addEdge(2, 1);

        // init possible trades set
        HashSet<Integer> possibleTrades_i = new HashSet<Integer>();
        HashSet<Integer> possibleTrades_j = new HashSet<Integer>();
        // expected possible trades
        HashSet<Integer> expectedPossibleTrades_i = new HashSet<Integer>(Collections.singletonList(0));
        HashSet<Integer> expectedPossibleTrades_j = new HashSet<Integer>(Arrays.asList(1, 2));




        // init curveball
        BidirectionalAdjacencyListImpl curveball = new BidirectionalAdjacencyListImpl(bidirectionalAdjacencyList, rnd);
        // function to test
        curveball.getPossibleTradesRightWithSelfLoop(0, 1, possibleTrades_i, possibleTrades_j);
        // asserts
        assertEquals(expectedPossibleTrades_i, possibleTrades_i);
        assertEquals(expectedPossibleTrades_j, possibleTrades_j);
    }



    @DisplayName("getting possible trade not allowing self loop")
    void getPossibleTradesRIghtWithoutSelfLoop() {
        // init possible trades set
        HashSet<Integer> possibleTrades_i = new HashSet<Integer>();
        HashSet<Integer> possibleTrades_j = new HashSet<Integer>();
        // expected possible trades
        HashSet<Integer> expectedPossibleTrades_i = new HashSet<Integer>();
        HashSet<Integer> expectedPossibleTrades_j = new HashSet<Integer>();
        expectedPossibleTrades_i.addAll(Arrays.asList(0, 2));
        expectedPossibleTrades_j.addAll(Arrays.asList(5, 6, 7));
        // init bidirectional adjacency list
        HashSet<Integer>[] adjacencyList = new HashSet[2];
        HashSet<Integer> neighbors_i = new HashSet<>(5, 1.0f);
        HashSet<Integer> neighbors_j = new HashSet<>(5, 1.0f);
        neighbors_i.addAll(Arrays.asList(0, 1, 2, 3, 4));
        neighbors_j.addAll(Arrays.asList(3, 4, 5, 6, 7));
        adjacencyList[0] = neighbors_i;
        adjacencyList[1] = neighbors_j;
        BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, 8);
        // init curveball
        BidirectionalAdjacencyListImpl curveball = new BidirectionalAdjacencyListImpl(bidirectionalAdjacencyList, rnd);
        // function to test
        curveball.getPossibleTradesWithoutSelfLoop(0, 1, possibleTrades_i, possibleTrades_j);
        // asserts
        assertEquals(possibleTrades_i, expectedPossibleTrades_i);
        assertEquals(possibleTrades_j, expectedPossibleTrades_j);
    }

    @Test
    @DisplayName("all possible trades works as expected")
    void getAllPossibleTrades() {
        HashSet<Integer> possibleTrade_i = new HashSet<>();
        possibleTrade_i.addAll(Arrays.asList(0, 1, 2, 3));
        HashSet<Integer> possibleTrade_j = new HashSet<>();
        possibleTrade_j.addAll(Arrays.asList(4, 5, 6, 7));

        BidirectionalAdjacencyListImpl curveball = new BidirectionalAdjacencyListImpl(null, rnd);
        HashSet<Integer> possibleTrades = curveball.getAllPossibleTrades(possibleTrade_i, possibleTrade_j);

        assertEquals(possibleTrades.size(), 8);
        for (int t : possibleTrades) {
            assertTrue(possibleTrade_i.contains(t) || possibleTrade_j.contains(t));
        }
    }

    /**
     * The trade method should keep the degree (e.g. the size of the neighborhood sets)
     */
    @Test
    @DisplayName("Trade Method Keeps Degree")
    @RepeatedTest(50)
    void keepsDegree() {


        // initializing the bidirectional adjacency list
        HashSet<Integer>[] adjacencyList = new HashSet[2];
        HashSet<Integer> neighbors_i = new HashSet<>(4, 1);
        neighbors_i.addAll(Arrays.asList(2, 4, 5, 6));
        adjacencyList[0] = neighbors_i;
        HashSet<Integer> neighbors_j = new HashSet<>(4, 1);
        neighbors_j.addAll(Arrays.asList(1, 3, 4, 5));
        adjacencyList[1] = neighbors_j;
        BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, 7);

        // initializing possible trades for both nodes
        HashSet<Integer> possibleTrades_i = new HashSet<>(2, 1);
        possibleTrades_i.addAll(Arrays.asList(2, 6));

        HashSet<Integer> possibleTrades_j = new HashSet<>(2, 1);
        possibleTrades_j.addAll(Arrays.asList(1, 3));

        // initializing de.uni.kl.cs.aalab.curveball implementation
        BidirectionalAdjacencyListImpl curveball = new BidirectionalAdjacencyListImpl(bidirectionalAdjacencyList, rnd);

        // method to test
        curveball.trade(0, 1, possibleTrades_i, possibleTrades_j);

        assertEquals(adjacencyList.length, 2);
        assertEquals(adjacencyList[0].size(), 4);
        assertEquals(adjacencyList[1].size(), 4);
    }

    /**
     * The trade method should keep the degree (e.g. the size of the neighborhood sets)
     */
    @Test
    @DisplayName("Trade Method Keeps Degree")
    @RepeatedTest(50)
    void keepsDegreePiGtPj() {


        // initializing the adjacency list
        HashSet<Integer>[] adjacencyList = new HashSet[2];
        HashSet<Integer> neighbors_i = new HashSet<>(4, 1);
        neighbors_i.addAll(Arrays.asList(2, 4, 5, 6));
        adjacencyList[0] = neighbors_i;
        HashSet<Integer> neighbors_j = new HashSet<>(2, 1);
        neighbors_j.addAll(Arrays.asList(1, 4, 5));
        adjacencyList[1] = neighbors_j;
        BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, 7);

        // initializing possible trades for both nodes
        HashSet<Integer> possibleTrades_i = new HashSet<>(2, 1);
        possibleTrades_i.addAll(Arrays.asList(2, 6));

        HashSet<Integer> possibleTrades_j = new HashSet<>(2, 1);
        possibleTrades_j.add(1);

        // initializing de.uni.kl.cs.aalab.curveball implementation
        BidirectionalAdjacencyListImpl curveball = new BidirectionalAdjacencyListImpl(bidirectionalAdjacencyList, rnd);

        // method to test
        curveball.trade(0, 1, possibleTrades_i, possibleTrades_j);

        assertEquals(adjacencyList.length, 2);
        assertEquals(adjacencyList[0].size(), 4);
        assertEquals(adjacencyList[1].size(), 3);
    }

    /**
     * The trade method should keep the degree (e.g. the size of the neighborhood sets)
     */
    @Test
    @DisplayName("Trade Method Keeps Degree")
    @RepeatedTest(50)
    void keepsDegreePiLtPj() {


        // initializing the adjacency list
        HashSet<Integer>[] adjacencyList = new HashSet[2];
        HashSet<Integer> neighbors_i = new HashSet<>(4, 1);
        neighbors_i.addAll(Arrays.asList(2, 4, 5));
        adjacencyList[0] = neighbors_i;
        HashSet<Integer> neighbors_j = new HashSet<>(2, 1);
        neighbors_j.addAll(Arrays.asList(1, 3, 4, 5));
        adjacencyList[1] = neighbors_j;
        BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, 6);

        // initializing possible trades for both nodes
        HashSet<Integer> possibleTrades_i = new HashSet<>(2, 1);
        possibleTrades_i.add(2);

        HashSet<Integer> possibleTrades_j = new HashSet<>(2, 1);
        possibleTrades_j.addAll(Arrays.asList(1, 3));

        // initializing de.uni.kl.cs.aalab.curveball implementation
        BidirectionalAdjacencyListImpl curveball = new BidirectionalAdjacencyListImpl(bidirectionalAdjacencyList, rnd);

        // method to test
        curveball.trade(0, 1, possibleTrades_i, possibleTrades_j);

        assertEquals(adjacencyList.length, 2);
        assertEquals(adjacencyList[0].size(), 3);
        assertEquals(adjacencyList[1].size(), 4);
    }

    @Test
    @DisplayName("Increase curveballAndCooccurrence even if key is not present")
    void increaseCoocc() {
        BidirectionalAdjacencyListImpl curveball = new BidirectionalAdjacencyListImpl(null, rnd);
        HashMap<String, Integer> coocc = new HashMap<>();
        curveball.increaseCoocc(1, 2, coocc);
        assertEquals(java.util.Optional.of(coocc.get("1 2")), java.util.Optional.of(1));
        curveball.increaseCoocc(1, 2, coocc);
        assertEquals(java.util.Optional.of(coocc.get("1 2")), java.util.Optional.of(2));
    }

    @Test
    @DisplayName("Decrease curveballAndCooccurrence even if key is not present")
    void decreaseCoocc() {
        BidirectionalAdjacencyListImpl curveball = new BidirectionalAdjacencyListImpl(null, rnd);
        HashMap<String, Integer> coocc = new HashMap<>();
        curveball.decreaseCoocc(1, 2, coocc);
        assertEquals(java.util.Optional.of(coocc.get("1 2")), java.util.Optional.of(-1));
        curveball.decreaseCoocc(1, 2, coocc);
        assertEquals(java.util.Optional.of(coocc.get("1 2")), java.util.Optional.of(-2));
    }

    @Nested
    @DisplayName("Trading With Self-Loops")
    class TradingWithSelfLoop {

        @Test
        @DisplayName("Keeps Degree")
        @RepeatedTest(50)
        void keepsDegree() {


            // initializing the adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[2];
            HashSet<Integer> neighbors_i = new HashSet<>(4, 1);
            neighbors_i.addAll(Arrays.asList(2, 4, 5, 6));
            adjacencyList[0] = neighbors_i;
            HashSet<Integer> neighbors_j = new HashSet<>(4, 1);
            neighbors_j.addAll(Arrays.asList(1, 3, 4, 5));
            adjacencyList[1] = neighbors_j;
            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, 7);

            // initializing de.uni.kl.cs.aalab.curveball implementation
            BidirectionalAdjacencyListImpl curveball = new BidirectionalAdjacencyListImpl(bidirectionalAdjacencyList, rnd);

            // method to test
            curveball.tradeWithSelfLoop(0, 1);

            assertEquals(adjacencyList.length, 2);
            assertEquals(adjacencyList[0].size(), 4);
            assertEquals(adjacencyList[1].size(), 4);
        }

        @Test
        @DisplayName("Does nothing when no trade possible")
        void fixedPoint() {


            // initializing the adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[2];
            HashSet<Integer> neighbors = new HashSet<>(4, 1);
            neighbors.addAll(Arrays.asList(2, 4, 5, 6));
            adjacencyList[0] = neighbors;
            adjacencyList[1] = neighbors;
            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, 7);

            // initializing de.uni.kl.cs.aalab.curveball implementation
            BidirectionalAdjacencyListImpl curveball = new BidirectionalAdjacencyListImpl(bidirectionalAdjacencyList, rnd);
            curveball.tradeWithSelfLoop(0, 1);
            assertEquals(adjacencyList.length, 2);
            assertEquals(adjacencyList[0], neighbors);
            assertEquals(adjacencyList[1], neighbors);
        }

        @Test
        @RepeatedTest(50)
        @DisplayName("computes the curveballAndCooccurrence of nodes i and j while trading")
        void cooccurenceWhileTradeWithSelfLoop() {


            // initializing the adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[2];
            HashSet<Integer> neighbors_i = new HashSet<>(4, 1);
            neighbors_i.addAll(Arrays.asList(2, 4, 5, 6));
            adjacencyList[0] = neighbors_i;
            HashSet<Integer> neighbors_j = new HashSet<>(4, 1);
            neighbors_j.addAll(Arrays.asList(1, 3, 4, 5));
            adjacencyList[1] = neighbors_j;
            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, 7);

            // initializing de.uni.kl.cs.aalab.curveball implementation
            BidirectionalAdjacencyListImpl curveball = new BidirectionalAdjacencyListImpl(bidirectionalAdjacencyList, rnd);

            // method to test
            int coocc = curveball.cooccurenceWhileTradeWithSelfLoop(0, 1);

            assertEquals(coocc, 2);
        }
    }

    @Nested
    @DisplayName("Trading Without Self-Loops")
    class TradingWithoutSelfLoop {

        @Test
        @DisplayName("Keeps Degree")
        @RepeatedTest(50)
        void keepsDegree() {
            // initializing the adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[2];
            HashSet<Integer> neighbors_i = new HashSet<>(4, 1);
            neighbors_i.addAll(Arrays.asList(2, 4, 5, 6));
            adjacencyList[0] = neighbors_i;
            HashSet<Integer> neighbors_j = new HashSet<>(4, 1);
            neighbors_j.addAll(Arrays.asList(1, 3, 4, 5));
            adjacencyList[1] = neighbors_j;
            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, 7);

            // initializing de.uni.kl.cs.aalab.curveball implementation
            BidirectionalAdjacencyListImpl curveball = new BidirectionalAdjacencyListImpl(bidirectionalAdjacencyList, rnd);

            // method to test
            curveball.tradeWithoutSelfLoop(0, 1);

            assertEquals(adjacencyList.length, 2);
            assertEquals(adjacencyList[0].size(), 4);
            assertEquals(adjacencyList[1].size(), 4);
        }

        @Test
        @DisplayName("Does nothing when no trade possible")
        void fixedPoint() {


            // initializing the adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[2];
            HashSet<Integer> neighbors = new HashSet<>(4, 1);
            neighbors.addAll(Arrays.asList(2, 4, 5, 6));
            adjacencyList[0] = neighbors;
            adjacencyList[1] = neighbors;
            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, 7);

            // initializing de.uni.kl.cs.aalab.curveball implementation
            BidirectionalAdjacencyListImpl curveball = new BidirectionalAdjacencyListImpl(bidirectionalAdjacencyList, rnd);

            curveball.tradeWithoutSelfLoop(0, 1);

            assertEquals(adjacencyList.length, 2);
            assertEquals(adjacencyList[0], neighbors);
            assertEquals(adjacencyList[1], neighbors);
        }

        /**
         * We place ourselves in a case where the only trade possible would lead to a self-loop.
         * Since the method we test does not allow self-loops, then we should see no trade at all.
         */
        @Test
        @DisplayName("Does not allow self-loop")
        @RepeatedTest(50)
        void noSelfLoop() {

            // initializing the adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[2];
            HashSet<Integer> neighbors_i = new HashSet<>(4, 1);
            neighbors_i.addAll(Arrays.asList(1, 4, 5, 6));
            adjacencyList[0] = neighbors_i;
            HashSet<Integer> neighbors_j = new HashSet<>(4, 1);
            neighbors_j.addAll(Arrays.asList(0, 4, 5, 6));
            adjacencyList[1] = neighbors_j;
            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, 7);

            // initializing de.uni.kl.cs.aalab.curveball implementation
            BidirectionalAdjacencyListImpl curveball = new BidirectionalAdjacencyListImpl(bidirectionalAdjacencyList, rnd);

            // method to test
            curveball.tradeWithoutSelfLoop(0, 1);

            assertEquals(adjacencyList.length, 2);
            assertEquals(adjacencyList[0], neighbors_i);
            assertEquals(adjacencyList[1], neighbors_j);
        }

        @Test
        @RepeatedTest(50)
        @DisplayName("Computes the curveballAndCooccurrence of nodes i and j while trading")
        void cooccurrenceWhileTradeWithoutSelfLoop() {

            // initializing the adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[2];
            HashSet<Integer> neighbors_i = new HashSet<>(4, 1);
            neighbors_i.addAll(Arrays.asList(1, 2, 4, 5));
            adjacencyList[0] = neighbors_i;
            HashSet<Integer> neighbors_j = new HashSet<>(4, 1);
            neighbors_j.addAll(Arrays.asList(0, 4, 5, 6));
            adjacencyList[1] = neighbors_j;
            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, 7);

            // initializing de.uni.kl.cs.aalab.curveball implementation
            BidirectionalAdjacencyListImpl curveball = new BidirectionalAdjacencyListImpl(bidirectionalAdjacencyList, rnd);

            // method to test
            int coocc = curveball.cooccurrenceWhileTradeWithoutSelfLoop(0, 1);

            assertEquals(2, coocc);
        }
    }

    @Nested
    @DisplayName("Update Cooccurrences while trading")
    class UpdateCooccurrenceWhileTrading {

        @Test
        @DisplayName("Updating curveballAndCooccurrence as expected")
        void update() {
            // adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[4];
            HashSet<Integer> neighbors = new HashSet<>();
            neighbors.add(0);
            adjacencyList[0] = neighbors;
            neighbors = new HashSet<>();
            neighbors.add(0);
            adjacencyList[1] = neighbors;
            neighbors = new HashSet<>();
            neighbors.add(1);
            adjacencyList[2] = neighbors;
            neighbors = new HashSet<>();
            neighbors.add(1);
            adjacencyList[3] = neighbors;
            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, 2);

            // coocc
            HashMap<String, Integer> coocc = new HashMap<String, Integer>();


            BidirectionalAdjacencyListImpl curveball = new BidirectionalAdjacencyListImpl(bidirectionalAdjacencyList, rnd);
            curveball.update(1, 2, 0, coocc);
            curveball.update(2, 1, 1, coocc);

            // expected coocc
            HashMap<String, Integer> expectedCoocc = new HashMap<String, Integer>();
            expectedCoocc.put("0 1", 1);
            expectedCoocc.put("0 2", -1);
            expectedCoocc.put("1 3", -1);
            expectedCoocc.put("2 3", 1);

            assertEquals(expectedCoocc, coocc);
        }

        @RepeatedTest(50)
        @DisplayName("Trade keeps degree")
        void keepsDegree() {

            // adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[4];
            HashSet<Integer> neighbors = new HashSet<>();
            neighbors.add(0);
            adjacencyList[0] = neighbors;
            neighbors = new HashSet<>();
            neighbors.add(0);
            adjacencyList[1] = neighbors;
            neighbors = new HashSet<>();
            neighbors.add(1);
            adjacencyList[2] = neighbors;
            neighbors = new HashSet<>();
            neighbors.add(1);
            adjacencyList[3] = neighbors;
            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, 2);

            // nodes i and j
            int i = 1;
            int j = 2;

            // possible trades for i and j
            HashSet<Integer> possibleTrades_i = new HashSet<>();
            possibleTrades_i.add(0);
            HashSet<Integer> possibleTrades_j = new HashSet<>();
            possibleTrades_j.add(1);

            BidirectionalAdjacencyListImpl curveball = new BidirectionalAdjacencyListImpl(bidirectionalAdjacencyList, rnd);
            HashMap<String, Integer> coocc = curveball.tradeUpdateCoocc(i, j, possibleTrades_i, possibleTrades_j);

            // assert degree is kept constant
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft().length, 4);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft()[0].size(), 1);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft()[1].size(), 1);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft()[2].size(), 1);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft()[3].size(), 1);

            assertEquals(bidirectionalAdjacencyList.getAdjacencyListRight().length, 2);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListRight()[0].size(), 2);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListRight()[1].size(), 2);
        }

        @RepeatedTest(50)
        @DisplayName("Update curveballAndCooccurrence when needed")
        void updateCoocc() {

            // adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[4];
            HashSet<Integer> neighbors = new HashSet<>();
            neighbors.add(0);
            adjacencyList[0] = neighbors;
            neighbors = new HashSet<>();
            neighbors.add(0);
            adjacencyList[1] = neighbors;
            neighbors = new HashSet<>();
            neighbors.add(1);
            adjacencyList[2] = neighbors;
            neighbors = new HashSet<>();
            neighbors.add(1);
            adjacencyList[3] = neighbors;
            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, 2);

            // nodes i and j
            int i = 1;
            int j = 2;

            // possible trades for i and j
            HashSet<Integer> possibleTrades_i = new HashSet<>();
            possibleTrades_i.add(0);
            HashSet<Integer> possibleTrades_j = new HashSet<>();
            possibleTrades_j.add(1);

            BidirectionalAdjacencyListImpl curveball = new BidirectionalAdjacencyListImpl(bidirectionalAdjacencyList, rnd);
            HashMap<String, Integer> coocc = curveball.tradeUpdateCoocc(i, j, possibleTrades_i, possibleTrades_j);

            if (curveball.getBidirectionalAdjacencyList().getAdjacencyListLeft()[1].contains(0)) {
                // adjacency list have not change
                assertTrue(coocc.isEmpty());
            } else {
                // adjacency list have changed
                // expected coocc
                HashMap<String, Integer> expectedCoocc = new HashMap<String, Integer>();
                expectedCoocc.put("0 1", 1);
                expectedCoocc.put("0 2", -1);
                expectedCoocc.put("1 3", -1);
                expectedCoocc.put("2 3", 1);

                assertEquals(expectedCoocc, coocc);
            }
        }
    }
}