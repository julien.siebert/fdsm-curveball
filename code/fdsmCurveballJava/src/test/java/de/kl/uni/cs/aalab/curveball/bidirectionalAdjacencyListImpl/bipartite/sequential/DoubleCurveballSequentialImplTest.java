package de.kl.uni.cs.aalab.curveball.bidirectionalAdjacencyListImpl.bipartite.sequential;

import de.kl.uni.cs.aalab.datastructure.BidirectionalAdjacencyList;
import org.junit.jupiter.api.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DoubleCurveballSequentialImplTest {
    private static Random rnd;

    @BeforeAll
    static void init() {
        rnd = new Random(0);
    }


    @Nested
    @DisplayName("Step")
    class Step {

        @Test
        @DisplayName("Works for odd number of nodes")
        void oddNbNodes() {
            // number of nodes
            int nLeft = 5;
            int nRight = 5;

            // initializing list of nodes indices
            int[] nodesIndicesLeft = new int[nLeft];
            for (int i = 0; i < nLeft; i++) {
                nodesIndicesLeft[i] = i;
            }
            int[] nodesIndicesRight = new int[nRight];
            for (int i = 0; i < nRight; i++) {
                nodesIndicesRight[i] = i;
            }
            // initializing adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[nLeft];

            HashSet<Integer> neighbors = new HashSet<>(4, 1.0f);
            neighbors.addAll(Arrays.asList(1, 2, 3, 4));
            adjacencyList[0] = neighbors;

            neighbors = new HashSet<>(4, 1.0f);
            neighbors.addAll(Arrays.asList(0, 1, 3, 4));
            adjacencyList[1] = neighbors;

            neighbors = new HashSet<>(4, 1.0f);
            neighbors.addAll(Arrays.asList(0, 1, 2, 4));
            adjacencyList[2] = neighbors;

            neighbors = new HashSet<>(4, 1.0f);
            neighbors.addAll(Arrays.asList(0, 1, 2, 3));
            adjacencyList[3] = neighbors;

            neighbors = new HashSet<>(4, 1.0f);
            neighbors.addAll(Arrays.asList(0, 1, 2, 3));
            adjacencyList[4] = neighbors;

            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, nRight);

            // initializing de.uni.kl.cs.aalab.curveball implementation
            DoubleCurveballSequentialImpl curveball = new DoubleCurveballSequentialImpl(nodesIndicesLeft, nodesIndicesRight, bidirectionalAdjacencyList, rnd);

            // calling step method (this shall not raise any error)
            curveball.step();
        }

        @Test
        @DisplayName("Works for even number of nodes")
        void evenNbNodes() {
            // number of nodes
            int nLeft = 6;
            int nRight = 6;

            // initializing list of nodes indices
            int[] nodesIndicesLeft = new int[nLeft];
            for (int i = 0; i < nLeft; i++) {
                nodesIndicesLeft[i] = i;
            }
            int[] nodesIndicesRight = new int[nRight];
            for (int i = 0; i < nRight; i++) {
                nodesIndicesRight[i] = i;
            }
            // initializing adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[nLeft];

            HashSet<Integer> neighbors = new HashSet<>(nLeft);

            neighbors.addAll(Arrays.asList(1, 2, 3, 4, 5));
            adjacencyList[0] = neighbors;

            neighbors = new HashSet<>(nLeft);
            neighbors.addAll(Arrays.asList(0, 2, 3, 4, 5));
            adjacencyList[1] = neighbors;

            neighbors = new HashSet<>(nLeft);
            neighbors.addAll(Arrays.asList(0, 1, 3, 4, 5));
            adjacencyList[2] = neighbors;

            neighbors = new HashSet<>(nLeft);
            neighbors.addAll(Arrays.asList(0, 1, 2, 4, 5));
            adjacencyList[3] = neighbors;

            neighbors = new HashSet<>(nLeft);
            neighbors.addAll(Arrays.asList(0, 1, 2, 3, 5));
            adjacencyList[4] = neighbors;

            neighbors = new HashSet<>(nLeft);
            neighbors.addAll(Arrays.asList(0, 1, 2, 3, 4));
            adjacencyList[5] = neighbors;

            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, nRight);

            // initializing curveball implementation
            DoubleCurveballSequentialImpl curveball = new DoubleCurveballSequentialImpl(nodesIndicesLeft, nodesIndicesRight, bidirectionalAdjacencyList, rnd);

            // calling step method (this shall not raise any error)
            curveball.step();
        }

        @RepeatedTest(100)
        @DisplayName("keeps degree sequence fixed")
        void keepsDegreeSequence() {
            // number of nodes
            int nLeft = 6;
            int nRight = 6;

            // initializing list of nodes indices
            int[] nodesIndicesLeft = new int[nLeft];
            for (int i = 0; i < nLeft; i++) {
                nodesIndicesLeft[i] = i;
            }
            int[] nodesIndicesRight = new int[nRight];
            for (int i = 0; i < nRight; i++) {
                nodesIndicesRight[i] = i;
            }
            // initializing adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[nLeft];

            HashSet<Integer> neighbors = new HashSet<>(nLeft);

            neighbors.addAll(Arrays.asList(1, 2, 3, 4, 5));
            adjacencyList[0] = neighbors;

            neighbors = new HashSet<>(nLeft);
            neighbors.addAll(Arrays.asList(0, 2, 3, 4, 5));
            adjacencyList[1] = neighbors;

            neighbors = new HashSet<>(nLeft);
            neighbors.addAll(Arrays.asList(0, 1, 3, 4, 5));
            adjacencyList[2] = neighbors;

            neighbors = new HashSet<>(nLeft);
            neighbors.addAll(Arrays.asList(0, 1, 2, 4, 5));
            adjacencyList[3] = neighbors;

            neighbors = new HashSet<>(nLeft);
            neighbors.addAll(Arrays.asList(0, 1, 2, 3, 5));
            adjacencyList[4] = neighbors;

            neighbors = new HashSet<>(nLeft);
            neighbors.addAll(Arrays.asList(0, 1, 2, 3, 4));
            adjacencyList[5] = neighbors;

            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, nRight);

            // initializing curveball implementation
            DoubleCurveballSequentialImpl curveball = new DoubleCurveballSequentialImpl(nodesIndicesLeft, nodesIndicesRight, bidirectionalAdjacencyList, rnd);
            // calling de.uni.kl.cs.aalab.curveball step method
            curveball.step();

            // degree sequences
            int[] degreeSequenceLeft = {5, 5, 5, 5, 5, 5};
            int[] degreeSequenceRight = {5, 5, 5, 5, 5, 5};


            for (int i = 0; i < nLeft; i++) {
                int expectedDegree = degreeSequenceLeft[i];
                int actualDegree = bidirectionalAdjacencyList.getAdjacencyListLeft()[i].size();
                assertEquals(expectedDegree, actualDegree);
            }
            for (int i = 0; i < nRight; i++) {
                int expectedDegree = degreeSequenceRight[i];
                int actualDegree = bidirectionalAdjacencyList.getAdjacencyListRight()[i].size();
                assertEquals(expectedDegree, actualDegree);
            }
        }

    }
}