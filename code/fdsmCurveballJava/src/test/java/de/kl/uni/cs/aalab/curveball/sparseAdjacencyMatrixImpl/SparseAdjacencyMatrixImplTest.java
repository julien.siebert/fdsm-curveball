package de.kl.uni.cs.aalab.curveball.sparseAdjacencyMatrixImpl;


import com.zaxxer.sparsebits.SparseBitSet;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class SparseAdjacencyMatrixImplTest {
    private static Random rnd;

    @BeforeAll
    static void init() {
        rnd = new Random(0);
    }

    /**
     * Testing method random split
     * <p>
     * Example
     * <p>
     * BitSet to split = [0,0,1,1,0,0,1,1]
     * <p>
     * number of ones in both arrays = 2
     * <p>
     * would possibly Returns
     * [0,0,1,0,0,0,0,1], [0,0,0,1,0,0,1,0]
     * <p>
     * What is tested:
     * <p>
     * There should be 2 'ones' in the first array returned and 2 'ones' in the second array returned
     * <p>
     * The BitSet to split == [0,0,1,0,0,0,0,1] OR [0,0,0,1,0,0,1,0]
     */
    @Test
    @DisplayName("Randomly splits a BitSet in to two BitSets with the right number of ones in each")
    @RepeatedTest(100)
    void randomSplit() {
        SparseAdjacencyMatrixImpl curveball = new SparseAdjacencyMatrixImpl(null, null, rnd);

        int n = 8;
        SparseBitSet toSplit = new SparseBitSet(n);
        // [0,0,1,1,0,0,1,1]
        toSplit.set(2);
        toSplit.set(3);
        toSplit.set(6);
        toSplit.set(7);

        int nbOnesInFirst = 2;

        SparseBitSet[] res = curveball.randomSplit(toSplit, nbOnesInFirst);
        SparseBitSet first = res[0];
        SparseBitSet second = res[1];

        assertEquals(2, first.cardinality());
        assertEquals(2, second.cardinality());

        first.or(second);

        assertEquals(toSplit, first);
    }

    @Test
    @DisplayName("Do not trade when no trade can be performed")
    void noTradePossible() {

        // number of nodes in the graph
        int n = 2;

        // initializing the nodes indices
        int[] nodesIndices = new int[n];
        for (int i = 0; i < n; i++) {
            nodesIndices[i] = i;
        }

        // initializing the adjacency matrix
        ArrayList<SparseBitSet> adjacencyMatrix = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            SparseBitSet row = new SparseBitSet(n);
            row.flip(0, n);
            adjacencyMatrix.add(row);
        }

        SparseAdjacencyMatrixImpl curveball = new SparseAdjacencyMatrixImpl(nodesIndices, adjacencyMatrix, rnd);
        curveball.tradeWithSelfLoop(0, 1);

        assertEquals(adjacencyMatrix.get(0), adjacencyMatrix.get(1));
    }

    @Test
    @DisplayName("Trade when single trade can be performed")
    void singleTradePossible() {

        // initializing the nodes indices
        int n = 2;
        int[] nodesIndices = new int[n];
        for (int i = 0; i < n; i++) {
            nodesIndices[i] = i;
        }

        // initializing the adjacency matrix
        ArrayList<SparseBitSet> adjacencyMatrix = new ArrayList<>(2);

        SparseBitSet firstRow = new SparseBitSet(2);
        firstRow.set(1);
        adjacencyMatrix.add(firstRow);

        SparseBitSet secondRow = new SparseBitSet(2);
        secondRow.set(0);
        adjacencyMatrix.add(secondRow);

        SparseAdjacencyMatrixImpl curveball = new SparseAdjacencyMatrixImpl(nodesIndices, adjacencyMatrix, rnd);
        curveball.tradeWithSelfLoop(0, 1);

        assertFalse(adjacencyMatrix.get(0).intersects(adjacencyMatrix.get(1)));
    }
}