package de.kl.uni.cs.aalab.curveball.sparseAdjacencyMatrixImpl.bipartite.thread;

import com.zaxxer.sparsebits.SparseBitSet;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class ThreadImplTest {
    private static Random rnd;

    @BeforeAll
    static void init() {
        rnd = new Random(0);
    }

    @Test
    @DisplayName("Creates a list of task with the right number of elements per task")
    void creation() {
        // number of nodes
        int n = 6;

        // initializing the nodes indices
        int[] nodesIndices = new int[n];
        for (int i = 0; i < n; i++) {
            nodesIndices[i] = i;
        }

        // initializing the adjacency matrix
        ArrayList<SparseBitSet> adjacencyMatrix = new ArrayList<>(n);

        ThreadImpl curveball = new ThreadImpl(2, nodesIndices, adjacencyMatrix, rnd);

        ArrayList<ParallelTradingTask> tasks = curveball.getTasks();
        assertEquals(2, tasks.size());
        assertEquals(0, tasks.get(0).getStartIndex());
        assertEquals(3, tasks.get(0).getEndIndex());
        assertEquals(3, tasks.get(1).getStartIndex());
        assertEquals(6, tasks.get(1).getEndIndex());
    }

    @Test
    @DisplayName("Keeps Degree Sequence")
    @RepeatedTest(100)
    void keepsDegreeSequence() {
        // number of nodes
        int n = 6;

        // initializing the nodes indices
        int[] nodesIndices = new int[n];
        for (int i = 0; i < n; i++) {
            nodesIndices[i] = i;
        }

        // initializing the adjacency matrix
        ArrayList<SparseBitSet> adjacencyMatrix = new ArrayList<>(n);

        SparseBitSet row = new SparseBitSet(n);

        row.set(0);
        row.set(1);
        adjacencyMatrix.add(row);

        row = new SparseBitSet(n);

        row.set(0);
        row.set(1);
        row.set(2);
        adjacencyMatrix.add(row);

        row = new SparseBitSet(n);

        row.set(1);
        row.set(2);
        row.set(3);
        adjacencyMatrix.add(row);

        row = new SparseBitSet(n);

        row.set(2);
        row.set(3);
        row.set(4);
        adjacencyMatrix.add(row);

        row = new SparseBitSet(n);

        row.set(3);
        row.set(4);
        row.set(5);
        adjacencyMatrix.add(row);

        row = new SparseBitSet(n);

        row.set(4);
        row.set(5);
        adjacencyMatrix.add(row);

        ThreadImpl curveball = new ThreadImpl(2, nodesIndices, adjacencyMatrix, rnd);
        try {
            curveball.step();
        } catch (InterruptedException e) {
            e.printStackTrace();
            fail("Should not throw exception");
        }
        curveball.shutdown();

        // check that the degree sequence is conserved
        assertEquals(2, adjacencyMatrix.get(0).cardinality());
        assertEquals(3, adjacencyMatrix.get(1).cardinality());
        assertEquals(3, adjacencyMatrix.get(2).cardinality());
        assertEquals(3, adjacencyMatrix.get(3).cardinality());
        assertEquals(3, adjacencyMatrix.get(4).cardinality());
        assertEquals(2, adjacencyMatrix.get(5).cardinality());

    }

    @Test
    @DisplayName("Runs more than one steps")
    void run2Steps() {
        // number of nodes
        int n = 6;

        // initializing the nodes indices
        int[] nodesIndices = new int[n];
        for (int i = 0; i < n; i++) {
            nodesIndices[i] = i;
        }

        // initializing the adjacency matrix
        ArrayList<SparseBitSet> adjacencyMatrix = new ArrayList<>(n);

        SparseBitSet row = new SparseBitSet(n);

        row.set(0);
        row.set(1);
        adjacencyMatrix.add(row);

        row = new SparseBitSet(n);

        row.set(0);
        row.set(1);
        row.set(2);
        adjacencyMatrix.add(row);

        row = new SparseBitSet(n);

        row.set(1);
        row.set(2);
        row.set(3);
        adjacencyMatrix.add(row);

        row = new SparseBitSet(n);

        row.set(2);
        row.set(3);
        row.set(4);
        adjacencyMatrix.add(row);

        row = new SparseBitSet(n);

        row.set(3);
        row.set(4);
        row.set(5);
        adjacencyMatrix.add(row);

        row = new SparseBitSet(n);

        row.set(4);
        row.set(5);
        adjacencyMatrix.add(row);

        ThreadImpl curveball = new ThreadImpl(2, nodesIndices, adjacencyMatrix, rnd);
        try {
            curveball.step();
        } catch (InterruptedException e) {
            e.printStackTrace();
            fail("Should not throw exception");
        }
        try {
            curveball.step();
        } catch (InterruptedException e) {
            e.printStackTrace();
            fail("Should not throw exception");
        }
        curveball.shutdown();

        // check that the degree sequence is conserved
        assertEquals(2, adjacencyMatrix.get(0).cardinality());
        assertEquals(3, adjacencyMatrix.get(1).cardinality());
        assertEquals(3, adjacencyMatrix.get(2).cardinality());
        assertEquals(3, adjacencyMatrix.get(3).cardinality());
        assertEquals(3, adjacencyMatrix.get(4).cardinality());
        assertEquals(2, adjacencyMatrix.get(5).cardinality());
    }
}