package de.kl.uni.cs.aalab.datastructure;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class BidirectionalAdjacencyListTest {

    @Nested
    @DisplayName("add edge")
    class AddEdge {

        @Test
        @DisplayName("when bidirectional adjacency list is empty")
        void emptyList() {
            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(3, 3);
            bidirectionalAdjacencyList.addEdge(0, 2);

            // assert left = 0:{2} | right = 2:{0}
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft()[0].size(), 1);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft()[1].size(), 0);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft()[2].size(), 0);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListRight()[0].size(), 0);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListRight()[1].size(), 0);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListRight()[2].size(), 1);
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListLeft()[0].contains(2));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListRight()[2].contains(0));
        }


        @Test
        @DisplayName("when bidirectional adjacency list is created from an existing one")
        void existingList() {
            // existing adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[3];
            adjacencyList[0] = new HashSet<>();
            adjacencyList[1] = new HashSet<>();
            adjacencyList[2] = new HashSet<>();
            adjacencyList[0].addAll(Arrays.asList(0, 1));
            adjacencyList[1].add(2);
            adjacencyList[2].add(1);

            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, 3);
            bidirectionalAdjacencyList.addEdge(0, 2);

            // assert left = 0:{0,1,2} 1:{2} 2:{1} | right = 0:{0} 1:{0,2} 2:{0,1}
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft()[0].size(), 3);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft()[1].size(), 1);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft()[2].size(), 1);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListRight()[0].size(), 1);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListRight()[1].size(), 2);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListRight()[2].size(), 2);
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListLeft()[0].containsAll(Arrays.asList(0, 1, 2)));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListLeft()[1].contains(2));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListLeft()[2].contains(1));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListRight()[0].contains(0));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListRight()[1].containsAll(Arrays.asList(0, 2)));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListRight()[2].containsAll(Arrays.asList(0, 1)));
        }


        @Test
        @DisplayName("when edge is already there")
        void existingEdge() {
            // existing adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[3];
            adjacencyList[0] = new HashSet<>();
            adjacencyList[1] = new HashSet<>();
            adjacencyList[2] = new HashSet<>();
            adjacencyList[0].addAll(Arrays.asList(0, 1));
            adjacencyList[1].add(2);
            adjacencyList[2].add(1);

            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, 3);
            bidirectionalAdjacencyList.addEdge(0, 1);

            // assert left = 0:{0,1} 1:{2} 2:{1} | right = 0:{0} 1:{0,2} 2:{1}
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft()[0].size(), 2);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft()[1].size(), 1);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft()[2].size(), 1);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListRight()[0].size(), 1);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListRight()[1].size(), 2);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListRight()[2].size(), 1);
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListLeft()[0].containsAll(Arrays.asList(0, 1)));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListLeft()[1].contains(2));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListLeft()[2].contains(1));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListRight()[0].contains(0));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListRight()[1].containsAll(Arrays.asList(0, 2)));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListRight()[2].contains(1));
        }

        @Test
        @DisplayName("when nodes of the edge are out of bounds")
        void outOfBoundsNodes() {
            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(3, 3);
            assertThrows(ArrayIndexOutOfBoundsException.class, () -> bidirectionalAdjacencyList.addEdge(10, 45));
        }
    }


    @Nested
    @DisplayName("remove edge")
    class RemoveEdge {

        @Test
        @DisplayName("when bidirectional adjacency list is empty")
        void emptyList() {
            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(3, 3);
            bidirectionalAdjacencyList.removeEdge(0, 2);

            // should still be empty
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListLeft()[0].isEmpty());
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListLeft()[1].isEmpty());
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListLeft()[2].isEmpty());
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListRight()[0].isEmpty());
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListRight()[1].isEmpty());
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListRight()[2].isEmpty());
        }


        @Test
        @DisplayName("when bidirectional adjacency list is created from an existing one")
        void existingList() {
            // existing adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[3];
            adjacencyList[0] = new HashSet<>();
            adjacencyList[1] = new HashSet<>();
            adjacencyList[2] = new HashSet<>();
            adjacencyList[0].addAll(Arrays.asList(0, 1));
            adjacencyList[1].add(2);
            adjacencyList[2].add(1);

            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, 3);
            bidirectionalAdjacencyList.removeEdge(0, 0);

            // assert left = 0:{1} 1:{2} 2:{1} | right = 0:{} 1:{0, 2} 2:{1}
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft()[0].size(), 1);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft()[1].size(), 1);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft()[2].size(), 1);
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListRight()[0].isEmpty());
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListRight()[1].size(), 2);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListRight()[2].size(), 1);
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListLeft()[0].contains(1));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListLeft()[1].contains(2));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListLeft()[2].contains(1));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListRight()[1].containsAll(Arrays.asList(0, 2)));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListRight()[2].contains(1));
        }

        @Test
        @DisplayName("when nodes of the edge are out of bounds")
        void outOfBoundsNodes() {
            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(3, 3);
            assertThrows(ArrayIndexOutOfBoundsException.class, () -> bidirectionalAdjacencyList.removeEdge(10, 45));
        }
    }

    @Nested
    @DisplayName("swap edges")
    class SwapEdges {

        @Test
        @DisplayName("when bidirectional adjacency list is empty")
        void emptyList() {
            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(3, 3);
            bidirectionalAdjacencyList.swapEdges(1, 2, 2, 1);

            // should still be empty
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListLeft()[0].isEmpty());
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListLeft()[1].isEmpty());
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListLeft()[2].isEmpty());
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListRight()[0].isEmpty());
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListRight()[1].isEmpty());
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListRight()[2].isEmpty());
        }

        @Test
        @DisplayName("when both edges do exist")
        void existingEdges() {
            // existing adjacency list
            HashSet<Integer>[] adjacencyList = new HashSet[3];
            adjacencyList[0] = new HashSet<>();
            adjacencyList[1] = new HashSet<>();
            adjacencyList[2] = new HashSet<>();
            adjacencyList[0].addAll(Arrays.asList(0, 1));
            adjacencyList[1].add(2);
            adjacencyList[2].add(1);

            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(adjacencyList, 3);
            bidirectionalAdjacencyList.swapEdges(1, 2, 2, 1);

            // assert left = 0:{0, 1} 1:{1} 2:{2} | right = 0:{0} 1:{0, 1} 2:{2}
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft()[0].size(), 2);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft()[1].size(), 1);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListLeft()[2].size(), 1);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListRight()[0].size(), 1);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListRight()[1].size(), 2);
            assertEquals(bidirectionalAdjacencyList.getAdjacencyListRight()[2].size(), 1);
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListLeft()[0].containsAll(Arrays.asList(0, 1)));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListLeft()[1].contains(1));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListLeft()[2].contains(2));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListRight()[0].contains(0));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListRight()[1].containsAll(Arrays.asList(0, 1)));
            assertTrue(bidirectionalAdjacencyList.getAdjacencyListRight()[2].contains(2));
        }

        @Test
        @DisplayName("when nodes of the edge are out of bounds")
        void outOfBoundsNodes() {
            BidirectionalAdjacencyList bidirectionalAdjacencyList = new BidirectionalAdjacencyList(3, 3);
            assertThrows(ArrayIndexOutOfBoundsException.class, () -> bidirectionalAdjacencyList.swapEdges(10, 45, 65, 43));
        }
    }
}