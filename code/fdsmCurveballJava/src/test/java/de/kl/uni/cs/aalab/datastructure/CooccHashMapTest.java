package de.kl.uni.cs.aalab.datastructure;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CooccHashMapTest {

    @Test
    @DisplayName("Checks that incrementing coocc does increment")
    void increment() {
        CooccHashMap coocc = new CooccHashMap();
        coocc.increment(1, 0,1);
        try {
            assertEquals(coocc.get(0, 1), 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        coocc.increment(0, 1,1);
        try {
            assertEquals(coocc.get(0, 1), 2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        coocc.increment(0, 1,7);
        try {
            assertEquals(coocc.get(0, 1), 9);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Checks that exception occurs")
    void incrementException() {
        CooccHashMap coocc = new CooccHashMap();
        assertThrows(Exception.class, () -> {
            coocc.get(1, 3);
        });
    }
}