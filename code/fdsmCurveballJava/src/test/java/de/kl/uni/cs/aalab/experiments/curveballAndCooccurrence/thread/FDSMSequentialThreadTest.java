package de.kl.uni.cs.aalab.experiments.curveballAndCooccurrence.thread;

import de.kl.uni.cs.aalab.curveball.adjacencyListImpl.bipartite.sequential.SequentialImpl;
import de.kl.uni.cs.aalab.utilities.LoadAdjacencyListFile;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Random;

class FDSMSequentialThreadTest {

    private static Path resourcesDirectory;
    private static Random rnd;

    /**
     * Initialization of random generator and path to the resources
     */
    @BeforeAll
    static void init() {
        resourcesDirectory = Paths.get("src", "test", "resources");
        rnd = new Random(0);
    }

    @Test
    @DisplayName("Should be able to generate a single random graph without exception, saves both adjacency list and curveballAndCooccurrence files")
    void run() {
        int n = 8;
        int nl = 8;
        int nr = 8;
        LoadAdjacencyListFile loader = new LoadAdjacencyListFile();
        Path fileToLoad = resourcesDirectory.resolve(Paths.get("simpleDirectedGraph", "directed_aa.txt"));
        try {
            // loading adjacency list from file
            HashSet<Integer>[] adjacencyList = loader.load(fileToLoad.toString(), n, 1);
            // initialization of the nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }
            // initialization of the curveball
            SequentialImpl curveball = new SequentialImpl(nodesIndices, adjacencyList, rnd);
            FDSMCooccurrenceThread prog = new FDSMCooccurrenceThread(curveball, nl, nr, resourcesDirectory);
            prog.run(1, 1);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Should be able to 2 random graphs without exception, saves both adjacency list and curveballAndCooccurrence files")
    void run2() {
        int n = 8;
        int nl = 8;
        int nr = 8;
        LoadAdjacencyListFile loader = new LoadAdjacencyListFile();
        Path fileToLoad = resourcesDirectory.resolve(Paths.get("simpleDirectedGraph", "directed_aa.txt"));
        try {
            // loading adjacency list from file
            HashSet<Integer>[] adjacencyList = loader.load(fileToLoad.toString(), n, 1);
            // initialization of the nodes indices
            int[] nodesIndices = new int[n];
            for (int i = 0; i < n; i++) {
                nodesIndices[i] = i;
            }
            // initialization of the curveball
            SequentialImpl curveball = new SequentialImpl(nodesIndices, adjacencyList, rnd);
            FDSMCooccurrenceThread prog = new FDSMCooccurrenceThread(curveball, nl, nr, resourcesDirectory);
            prog.run(2, 1);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}