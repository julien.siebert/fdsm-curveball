package de.kl.uni.cs.aalab.experiments.curveballAndCooccurrence.thread;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class SequentialTaskTest {

    @Test
    @DisplayName("Ensure deep copy")
    void setAdjacencyList() {
        HashSet<Integer>[] adjacencyList = new HashSet[2];
        HashSet<Integer> neighbors = new HashSet<>(5);
        neighbors.addAll(Arrays.asList(1,2,3,4,5));
        adjacencyList[0] = neighbors;

        neighbors = new HashSet<>(3);
        neighbors.addAll(Arrays.asList(7,8,9));
        adjacencyList[1] = neighbors;

        CooccurrenceTask coocc = new CooccurrenceTask(adjacencyList, 0,0, null, "");

        // assert Objects are different (different references)
        assertTrue(adjacencyList != coocc.getAdjacencyList());
        // each HashSet must be a different Object
        for (int i = 0; i < adjacencyList.length; i++) {
            assertTrue(adjacencyList[i] != coocc.getAdjacencyList()[i]);
            // assert all values are the same
            assertEquals(adjacencyList[i],coocc.getAdjacencyList()[i]);
        }
    }
}