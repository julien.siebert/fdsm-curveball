package de.kl.uni.cs.aalab.similarity.adjacencyListImpl;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PerturbationScoreTest {

    @Test
    @DisplayName("Returns 0 when nothing has changed between the two sets")
    void testSameSets() {
        HashSet<Integer> nodesNeighboursG0 = new HashSet<Integer>(10, 1);
        nodesNeighboursG0.addAll(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
        HashSet<Integer> nodesNeighboursG1 = new HashSet<Integer>(10, 1);
        nodesNeighboursG1.addAll(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));

        assertEquals(0, PerturbationScore.nbEdgesChanged(nodesNeighboursG0, nodesNeighboursG1));
    }

    @Test
    @DisplayName("Returns the right number of different elements between two sets")
    void testDiffSets() {
        HashSet<Integer> nodesNeighboursG0 = new HashSet<Integer>(10, 1);
        nodesNeighboursG0.addAll(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
        HashSet<Integer> nodesNeighboursG1 = new HashSet<Integer>(10, 1);
        nodesNeighboursG1.addAll(Arrays.asList(1, 2, 3, 4, 5, 11, 12, 13, 14, 15));

        assertEquals(5, PerturbationScore.nbEdgesChanged(nodesNeighboursG0, nodesNeighboursG1));
    }

    @Test
    @DisplayName("Returns 0 when both graphs G0 and G1 are identical")
    void testScoreSameGraphs() {
        HashSet<Integer>[] adjacencyListG0 = new HashSet[4];
        HashSet<Integer> neighborsNode0 = new HashSet<Integer>(2, 1);
        neighborsNode0.addAll(Arrays.asList(1, 3));
        adjacencyListG0[0] = neighborsNode0;
        HashSet<Integer> neighborsNode1 = new HashSet<Integer>(2, 1);
        neighborsNode1.addAll(Arrays.asList(0, 2));
        adjacencyListG0[1] = neighborsNode1;
        HashSet<Integer> neighborsNode2 = new HashSet<Integer>(2, 1);
        neighborsNode2.addAll(Arrays.asList(1, 3));
        adjacencyListG0[2] = neighborsNode2;
        HashSet<Integer> neighborsNode3 = new HashSet<Integer>(2, 1);
        neighborsNode3.addAll(Arrays.asList(0, 2));
        adjacencyListG0[3] = neighborsNode3;

        assertEquals(0, PerturbationScore.computeScore(adjacencyListG0, adjacencyListG0));
    }

    @Test
    @DisplayName("Returns 1 when both graphs G0 and G1 do not share any common edge")
    void testScoreDiffGraphs() {
        // first graph G1
        HashSet<Integer>[] adjacencyListG0 = new HashSet[4];
        HashSet<Integer> neighborsNode0 = new HashSet<Integer>(2, 1);
        neighborsNode0.addAll(Arrays.asList(1, 3));
        adjacencyListG0[0] = neighborsNode0;
        HashSet<Integer> neighborsNode1 = new HashSet<Integer>(2, 1);
        neighborsNode1.addAll(Arrays.asList(0, 2));
        adjacencyListG0[1] = neighborsNode1;
        HashSet<Integer> neighborsNode2 = new HashSet<Integer>(2, 1);
        neighborsNode2.addAll(Arrays.asList(1, 3));
        adjacencyListG0[2] = neighborsNode2;
        HashSet<Integer> neighborsNode3 = new HashSet<Integer>(2, 1);
        neighborsNode3.addAll(Arrays.asList(0, 2));
        adjacencyListG0[3] = neighborsNode3;

        // second graph G1
        HashSet<Integer>[] adjacencyListG1 = new HashSet[4];
        neighborsNode0 = new HashSet<Integer>(2, 1);
        neighborsNode0.addAll(Arrays.asList(0, 2));
        adjacencyListG1[0] = neighborsNode0;
        neighborsNode1 = new HashSet<Integer>(2, 1);
        neighborsNode1.addAll(Arrays.asList(1, 3));
        adjacencyListG1[1] = neighborsNode1;
        neighborsNode2 = new HashSet<Integer>(2, 1);
        neighborsNode2.addAll(Arrays.asList(0, 2));
        adjacencyListG1[2] = neighborsNode2;
        neighborsNode3 = new HashSet<Integer>(2, 1);
        neighborsNode3.addAll(Arrays.asList(1, 3));
        adjacencyListG1[3] = neighborsNode3;

        assertEquals(1, PerturbationScore.computeScore(adjacencyListG0, adjacencyListG1));
    }

    @Test
    @DisplayName("Returns the expected score")
    void testScoreGraphs() {
        // first graph G0
        HashSet<Integer>[] adjacencyListG0 = new HashSet[4];
        HashSet<Integer> neighborsNode0 = new HashSet<Integer>(2, 1);
        neighborsNode0.addAll(Arrays.asList(1, 3));
        adjacencyListG0[0] = neighborsNode0;
        HashSet<Integer> neighborsNode1 = new HashSet<Integer>(2, 1);
        neighborsNode1.addAll(Arrays.asList(0, 2));
        adjacencyListG0[1] = neighborsNode1;
        HashSet<Integer> neighborsNode2 = new HashSet<Integer>(2, 1);
        neighborsNode2.addAll(Arrays.asList(1, 3));
        adjacencyListG0[2] = neighborsNode2;
        HashSet<Integer> neighborsNode3 = new HashSet<Integer>(2, 1);
        neighborsNode3.addAll(Arrays.asList(0, 2));
        adjacencyListG0[3] = neighborsNode3;

        // second graph G1

        HashSet<Integer>[] adjacencyListG1 = new HashSet[4];
        neighborsNode0 = new HashSet<Integer>(2, 1);
        neighborsNode0.addAll(Arrays.asList(2, 3));
        adjacencyListG1[0] = neighborsNode0;
        neighborsNode1 = new HashSet<Integer>(2, 1);
        neighborsNode1.addAll(Arrays.asList(1, 3));
        adjacencyListG1[1] = neighborsNode1;
        neighborsNode2 = new HashSet<Integer>(2, 1);
        neighborsNode2.addAll(Arrays.asList(1, 3));
        adjacencyListG1[2] = neighborsNode2;
        neighborsNode3 = new HashSet<Integer>(2, 1);
        neighborsNode3.addAll(Arrays.asList(2, 3));
        adjacencyListG1[3] = neighborsNode3;

        assertEquals(0.5, PerturbationScore.computeScore(adjacencyListG0, adjacencyListG1));
    }


}