package de.kl.uni.cs.aalab.utilities;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

import static de.kl.uni.cs.aalab.utilities.GraphGenerator.linear;
import static de.kl.uni.cs.aalab.utilities.GraphGenerator.powerLaw;
import static org.junit.jupiter.api.Assertions.*;

class GraphGeneratorTest {

    @Test
    @DisplayName("generates a graph following a power law")
    void testPowerLawGenerator() {
        HashSet<Integer>[] expected = new HashSet[8];
        for (int i = 0; i < 8; i++) {
            expected[i] = new HashSet<Integer>();
        }
        expected[0].addAll(Arrays.asList(0,1,2));
        expected[1].addAll(Arrays.asList(1,2,3));
        expected[2].addAll(Arrays.asList(2,3));
        expected[3].addAll(Arrays.asList(3,4));
        expected[4].addAll(Arrays.asList(4));
        expected[5].addAll(Arrays.asList(5));
        expected[6].addAll(Arrays.asList(6));
        expected[7].addAll(Arrays.asList(7));

        HashSet<Integer>[] adjacencyList = powerLaw(2,3);

        for (int i = 0; i < 8; i++) {
            assertEquals(expected[i],adjacencyList[i]);
        }

    }

    @Test
    void testLinear() {
        HashSet<Integer>[] expected = new HashSet[4];
        for (int i = 0; i < 4; i++) {
            expected[i] = new HashSet<Integer>();
        }
        expected[0].addAll(Arrays.asList(0,1,2,3));
        expected[1].addAll(Arrays.asList(0,1,2));
        expected[2].addAll(Arrays.asList(0,1));
        expected[3].addAll(Arrays.asList(0));


        HashSet<Integer>[] adjacencyList = linear(4);

        for (int i = 0; i < 4; i++) {
            assertEquals(expected[i],adjacencyList[i]);
        }
    }
}