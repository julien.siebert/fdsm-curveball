package de.kl.uni.cs.aalab.utilities;

import com.zaxxer.sparsebits.SparseBitSet;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LoadAdjacencyListFileTest {

    private static Path resourcesDirectory;

    @BeforeAll
    static void init() {
        resourcesDirectory = Paths.get("src", "test", "resources");
    }

    @Test
    @DisplayName("Should extract the correct adjacency list even if the minimum index is 1")
    void loadMinIndexOne() {
        // creating expected adjacency list
        HashSet<Integer>[] expectedAdjacencyList = new HashSet[8];
        HashSet<Integer> neighbors = new HashSet<Integer>(1, 1);
        neighbors.add(1);
        expectedAdjacencyList[0] = neighbors;
        neighbors = new HashSet<Integer>(2, 1);
        neighbors.addAll(Arrays.asList(2, 4));
        expectedAdjacencyList[1] = neighbors;
        neighbors = new HashSet<Integer>(1, 1);
        expectedAdjacencyList[2] = neighbors;
        neighbors.add(3);
        neighbors = new HashSet<Integer>(1, 1);
        expectedAdjacencyList[3] = neighbors;
        neighbors.add(6);
        neighbors = new HashSet<Integer>(1, 1);
        expectedAdjacencyList[4] = neighbors;
        neighbors.add(5);
        neighbors = new HashSet<Integer>(1, 1);
        expectedAdjacencyList[5] = neighbors;
        neighbors.add(6);
        neighbors = new HashSet<Integer>(1, 1);
        expectedAdjacencyList[6] = neighbors;
        neighbors.add(7);
        neighbors = new HashSet<Integer>(0, 1);
        expectedAdjacencyList[7] = neighbors;

        try {
            LoadAdjacencyListFile loader = new LoadAdjacencyListFile();
            Path fileToLoad = resourcesDirectory.resolve(Paths.get("simpleDirectedGraph", "directed_aa.txt"));
            HashSet<Integer>[] adjacencyList = loader.load(fileToLoad.toString(), 8, 1);
            for (int i = 0; i < expectedAdjacencyList.length; i++) {
                assertEquals(adjacencyList[i], expectedAdjacencyList[i]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Should extract the correct adjacency list even if the nodes are not sorted")
    void loadNotSortedNodes() {
        // creating expected adjacency list

        HashSet<Integer>[] expectedAdjacencyList = new HashSet[8];
        HashSet<Integer> neighbors = new HashSet<Integer>(1, 1);
        neighbors.add(6);
        expectedAdjacencyList[0] = neighbors;
        neighbors = new HashSet<Integer>(2, 1);
        neighbors.addAll(Arrays.asList(3, 5));
        expectedAdjacencyList[1] = neighbors;
        neighbors = new HashSet<Integer>(1, 1);
        expectedAdjacencyList[2] = neighbors;
        neighbors.add(1);
        neighbors = new HashSet<Integer>(1, 1);
        expectedAdjacencyList[3] = neighbors;
        neighbors.add(7);
        neighbors = new HashSet<Integer>(1, 1);
        expectedAdjacencyList[4] = neighbors;
        neighbors.add(6);
        neighbors = new HashSet<Integer>(1, 1);
        expectedAdjacencyList[5] = neighbors;
        neighbors.add(2);
        neighbors = new HashSet<Integer>(1, 1);
        expectedAdjacencyList[6] = neighbors;
        neighbors.add(4);
        neighbors = new HashSet<Integer>(0, 1);
        expectedAdjacencyList[7] = neighbors;

        try {
            LoadAdjacencyListFile loader = new LoadAdjacencyListFile();
            Path fileToLoad = resourcesDirectory.resolve(Paths.get("simpleDirectedGraph", "directed_ab.txt"));
            HashSet<Integer>[] adjacencyList = loader.load(fileToLoad.toString(), 8, 1);
            for (int i = 0; i < expectedAdjacencyList.length; i++) {
                assertEquals(adjacencyList[i], expectedAdjacencyList[i]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Save an adjacency list in the correct format")
    void save() {

        HashSet<Integer>[] adjacencyList = new HashSet[8];
        HashSet<Integer> neighbors = new HashSet<Integer>(1, 1);
        neighbors.add(6);
        adjacencyList[0] = neighbors;
        neighbors = new HashSet<Integer>(2, 1);
        neighbors.addAll(Arrays.asList(3, 5));
        adjacencyList[1] = neighbors;
        neighbors = new HashSet<Integer>(1, 1);
        adjacencyList[2] = neighbors;
        neighbors.add(1);
        neighbors = new HashSet<Integer>(1, 1);
        adjacencyList[3] = neighbors;
        neighbors.add(7);
        neighbors = new HashSet<Integer>(1, 1);
        adjacencyList[4] = neighbors;
        neighbors.add(6);
        neighbors = new HashSet<Integer>(1, 1);
        adjacencyList[5] = neighbors;
        neighbors.add(2);
        neighbors = new HashSet<Integer>(1, 1);
        adjacencyList[6] = neighbors;
        neighbors.add(4);
        neighbors = new HashSet<Integer>(0, 1);
        adjacencyList[7] = neighbors;

        try {
            Path fileToSave = resourcesDirectory.resolve(Paths.get("adjacency_list.csv"));
            LoadAdjacencyListFile.save(adjacencyList, fileToSave.toString(), ' ');
            LoadAdjacencyListFile loader = new LoadAdjacencyListFile();
            HashSet<Integer>[] loadedAdjacencyList = loader.load(fileToSave.toString(), 8, 0);
            for (int i = 0; i < adjacencyList.length; i++) {
                assertEquals(adjacencyList[i], loadedAdjacencyList[i]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Load an adjacency list from a file and return the right adjacency matrix")
    void loadAdjacencyMatrix() {
        ArrayList<BitSet> expectedAdjacencyMatrix = new ArrayList<>(8);
        BitSet row;
        for (int i = 0; i < 8; i++) {
            row = new BitSet();
            row.clear();
            expectedAdjacencyMatrix.add(row);
        }

        expectedAdjacencyMatrix.get(0).set(6);
        expectedAdjacencyMatrix.get(1).set(7);
        expectedAdjacencyMatrix.get(1).set(6);
        expectedAdjacencyMatrix.get(2).set(3);
        expectedAdjacencyMatrix.get(3).set(5);
        expectedAdjacencyMatrix.get(4).set(2);
        expectedAdjacencyMatrix.get(5).set(4);
        expectedAdjacencyMatrix.get(6).set(1);

        try {
            Path fileToLoad = resourcesDirectory.resolve(Paths.get("simpleDirectedGraph", "directed_ac.txt"));
            LoadAdjacencyListFile loader = new LoadAdjacencyListFile();
            ArrayList<BitSet> adjacencyMatrix = loader.loadAdjacencyMatrix(fileToLoad.toString(), 8, 1);
            for (int i = 0; i < 8; i++) {
                assertEquals(expectedAdjacencyMatrix.get(i), adjacencyMatrix.get(i));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void loadSparseAdjacencyMatrix() {
        ArrayList<SparseBitSet> expectedAdjacencyMatrix = new ArrayList<>(8);
        SparseBitSet row;
        for (int i = 0; i < 8; i++) {
            row = new SparseBitSet();
            expectedAdjacencyMatrix.add(row);
        }

        expectedAdjacencyMatrix.get(0).set(6);
        expectedAdjacencyMatrix.get(1).set(7);
        expectedAdjacencyMatrix.get(1).set(6);
        expectedAdjacencyMatrix.get(2).set(3);
        expectedAdjacencyMatrix.get(3).set(5);
        expectedAdjacencyMatrix.get(4).set(2);
        expectedAdjacencyMatrix.get(5).set(4);
        expectedAdjacencyMatrix.get(6).set(1);

        try {
            Path fileToLoad = resourcesDirectory.resolve(Paths.get("simpleDirectedGraph", "directed_ac.txt"));
            LoadAdjacencyListFile loader = new LoadAdjacencyListFile();
            ArrayList<SparseBitSet> adjacencyMatrix = loader.loadSparseAdjacencyMatrix(fileToLoad.toString(), 8, 1);
            for (int i = 0; i < 8; i++) {
                assertEquals(expectedAdjacencyMatrix.get(i), adjacencyMatrix.get(i));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}