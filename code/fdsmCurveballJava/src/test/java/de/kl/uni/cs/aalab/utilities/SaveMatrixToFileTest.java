package de.kl.uni.cs.aalab.utilities;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SaveMatrixToFileTest {

    private static Path resourcesDirectory;

    @BeforeAll
    static void init() { resourcesDirectory = Paths.get("src","test","resources");}

    @Test
    void saveMatrixIntArray() {
        int[][] matrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        try {
            Path fileToSave = resourcesDirectory.resolve(Paths.get("adjacency_matrix.csv"));
            SaveMatrixToFile.save(matrix, fileToSave.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        // check that we have save the right content
        BufferedReader reader = null;
        try {
            Path fileToLoad = resourcesDirectory.resolve(Paths.get("adjacency_matrix.csv"));
            reader = new BufferedReader(new FileReader(fileToLoad.toString()));
            String readLine = "";

            System.out.println("Reading file using Buffered Reader");

            int i = 0;
            String[] expectedLines = {"1,2,3","4,5,6","7,8,9"};
            while ((readLine = reader.readLine()) != null) {
                assertEquals(readLine,expectedLines[i]);
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    void saveMatrixHashMap() {
        HashMap<String, Integer> matrix = new HashMap<>();
        matrix.put("0 1", 3);
        matrix.put("1 2", 2);
        try {
            Path fileToSave = resourcesDirectory.resolve(Paths.get("adjacency_matrix_hashmap.csv"));
            SaveMatrixToFile.save(matrix, fileToSave.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        // check that we have save the right content
        BufferedReader reader = null;
        try {
            Path fileToLoad = resourcesDirectory.resolve(Paths.get("adjacency_matrix_hashmap.csv"));
            reader = new BufferedReader(new FileReader(fileToLoad.toString()));
            String readLine = "";

            System.out.println("Reading file using Buffered Reader");

            int i = 0;
            String[] expectedLines = {"0 1 3","1 2 2"};
            while ((readLine = reader.readLine()) != null) {
                assertEquals(readLine,expectedLines[i]);
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}