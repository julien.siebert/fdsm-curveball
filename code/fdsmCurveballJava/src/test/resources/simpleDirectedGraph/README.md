
In this directory are stored all examples of graphs with 8 nodes
and the following degree distribution

1 node  with in-degree k_in = 1 and out-degree k_out = 0
1 node  with in-degree k_in = 0 and out-degree k_out = 1
4 nodes with in-degree k_in = 1 and out-degree k_out = 1
1 node  with in-degree k_in = 1 and out-degree k_out = 2
1 node  with in-degree k_in = 2 and out-degree k_out = 1


