import cProfile
import os

from utilities.load_save_data import read_csv
from metrics.cooccurrence import compute_cooccurrence_graph


def main():
    # G_0: 1. read_csv->Dict[int,set]   2.degree sequence->Dict[int,int]   3.coocc
    adjacency_map = read_csv(filename=os.path.join(os.path.dirname(__file__), 'data/adj_list_right.csv'))

    fixed_degree_sequence = {}
    for node in adjacency_map:
        fixed_degree_sequence[node] = len(adjacency_map[node])

    coocc = compute_cooccurrence_graph(adjacency_list=adjacency_map)
    """
    1:{2: 5, 3: 9, 4: 3, 5: 3}
    2:{3: 5, 4: 2, 5: 0}
    3:{4: 1, 5: 0}
    4:{5: 2}
    """
    with open(os.path.join(os.path.dirname(__file__), 'data/coocc_run_0.csv'), 'w') as f:
        for node in list(adjacency_map.keys())[:-1]:
            f.write('{}:{}\n'.format(node, coocc[node]))

    # G_1-G_100: 1.read_csv->Dict[int,set]    2.coocc
    for i in range(1, 101):
        adj_map = read_csv(filename=os.path.join(os.path.dirname(__file__), 'data/run_seed0_step{}.csv'.format(i)))
        coocc = compute_cooccurrence_graph(adjacency_list=adj_map)
        with open(os.path.join(os.path.dirname(__file__), 'data/coocc_run_{}.csv'.format(i)), 'w') as f:
            for node in list(adjacency_map.keys())[:-1]:
                f.write('{}:{}\n'.format(node, coocc[node]))


if __name__ == '__main__':
    cProfile.run('main()')




