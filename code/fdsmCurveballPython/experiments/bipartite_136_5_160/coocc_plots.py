import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import cProfile
import os

from utilities.load_save_data import read_csv


def coocc_plot_2d(index_node_1: int, index_node_2: int):
    coocc_u_v = []
    adj_map = read_csv(os.path.join(os.path.dirname(__file__), 'data/adj_list_right.csv'))
    if index_node_1 > index_node_2:
        index_node_1, index_node_2 = index_node_2, index_node_1
    elif index_node_1 == index_node_2:
        coocc_u_v = [len(adj_map[index_node_1])] * 101
    else:
        for i in range(101):
            with open(os.path.join(os.path.dirname(__file__), 'data/coocc_run_{}.csv'.format(i)), 'r') as f:
                for line in f:
                    if line[0] == str(index_node_1):
                        coocc_u_v.insert(i, int(line.split(',')[index_node_2-index_node_1-1].split(':')[-1].strip('}\n')))
    X = np.arange(0, 101)
    plt.plot(X, [len(adj_map[index_node_1])]*101, label='deg({})'.format(index_node_1))
    plt.plot(X, [len(adj_map[index_node_2])]*101, label='deg({})'.format(index_node_2))
    plt.plot(X, coocc_u_v, '*')
    plt.plot(X, coocc_u_v)
    plt.xlabel('Random Graphs')
    plt.ylabel('coocc({},{})'.format(index_node_1, index_node_2))
    plt.legend()
    plt.show()


def main():
    coocc_plot_2d(2, 5)


if __name__ == '__main__':
    cProfile.run('main()')