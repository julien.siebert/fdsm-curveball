import cProfile
import os
import random
from memory_profiler import profile

from utilities.load_save_data import read_csv_list, save_csv_list
from curveball.classical.run import step, run
import curveball.classical.run_subprocess as subpro


@profile
def step_list(nb_samples=100, seed=0):
    adj_list = read_csv_list(filename=os.path.join(os.path.dirname(__file__), 'data/adj_list_right.csv'))
    random.seed(seed)
    for i in range(1, nb_samples+1):
        adj_list = step(adjacency_list=adj_list)
        save_csv_list(adjacency_list=adj_list,
                      filename=os.path.join(os.path.dirname(__file__), 'data/step_seed{}_step{}.csv'.format(seed, i)))


@profile
def run_list(nb_samples=100, nb_steps=10, seed=0):
    adj_list = read_csv_list(filename=os.path.join(os.path.dirname(__file__), 'data/adj_list_right.csv'))
    random.seed(seed)
    for i in range(1, nb_samples+1):
        adj_list = run(adjacency_list=adj_list, nb_steps=nb_steps)
        save_csv_list(adjacency_list=adj_list,
                      filename=os.path.join(os.path.dirname(__file__), 'data/run_size{}_seed{}_step{}.csv'.format(nb_steps, seed, i)))


@profile
def parallel_run(nb_samples=100, seed=0):
    adj_list = read_csv_list(filename=os.path.join(os.path.dirname(__file__), 'data/adj_list_right.csv'))
    random.seed(seed)
    for i in range(1, nb_samples+1):
        adj_list = subpro.run(adjacency_list=adj_list, nb_steps=10, nb_workers=3)
        save_csv_list(adjacency_list=adj_list, filename=os.path.join(os.path.dirname(__file__),
                                                                     'data/subprocess_run_seed{}_step{}.csv'
                                                                     .format(seed, i)))


def main():
    for i in range(10):
        step_list(seed=i)
    # run_list(100)
    # parallel_run(100)


if __name__ == '__main__':
    cProfile.run('main()')



