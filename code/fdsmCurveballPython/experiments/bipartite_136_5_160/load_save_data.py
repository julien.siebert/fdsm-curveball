import os
from utilities.load_save_data import extract_adjacency_list_right, save_csv


if __name__ == '__main__':
    dataset = os.path.join(os.path.dirname(__file__), 'data/out.brunson_revolution_revolution')
    adjacency_list = extract_adjacency_list_right(dataset)
    save_csv(adjacency_list=adjacency_list, filename=os.path.join(os.path.dirname(__file__), 'data/adj_list_right.csv'))


