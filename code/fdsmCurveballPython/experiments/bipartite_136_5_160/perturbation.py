import cProfile
import os

from utilities.plot.plot_perturbation_scores import \
    perturbation_scores_plot_0, perturbation_scores_plot, perturbation_scores_plot_3d, \
    compare_perturbation_scores_plot_0


def main():
    # perturbation_scores_plot_0(os.path.join(os.path.dirname(__file__), 'data/adj_list_right.csv'), os.path.join(os.path.dirname(__file__), 'data/step_seed0_step{}.csv'), 100)
    # perturbation_scores_plot(os.path.join(os.path.dirname(__file__), 'data/adj_list_right.csv'), os.path.join(os.path.dirname(__file__), 'data/step_seed0_step{}.csv'), 1000)
    # perturbation_scores_plot_3d(os.path.join(os.path.dirname(__file__), 'data/adj_list_right.csv'), os.path.join(os.path.dirname(__file__), 'data/step_seed0_step{}.csv'), 100)
    #
    # perturbation_scores_plot_0(os.path.join(os.path.dirname(__file__), 'data/adj_list_right.csv'), os.path.join(os.path.dirname(__file__), 'data/run_size10_seed0_step{}.csv'), 100)
    # perturbation_scores_plot(os.path.join(os.path.dirname(__file__), 'data/adj_list_right.csv'), os.path.join(os.path.dirname(__file__), 'data/run_size10_seed0_step{}.csv'), 100)
    perturbation_scores_plot_3d(os.path.join(os.path.dirname(__file__), 'data/adj_list_right.csv'), os.path.join(os.path.dirname(__file__), 'data/run_size10_seed0_step{}.csv'))

    # compare_perturbation_scores_plot_0(os.path.join(os.path.dirname(__file__), 'data/adj_list_right.csv'), os.path.join(os.path.dirname(__file__), 'data/step_seed{}_step{}.csv'), seed=10)


if __name__ == '__main__':
    cProfile.run('main()')