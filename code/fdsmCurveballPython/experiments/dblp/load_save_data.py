from utilities.load_save_data import extract_adjacency_list, save_csv


if __name__ == '__main__':
    dataset = 'D:\\WorkSpace\\Datasets\\dblp\\out.dblp-author'
    adjacency_list = extract_adjacency_list(edge_list_filename=dataset)
    save_csv(adjacency_list=adjacency_list, filename='D:\\WorkSpace\\Datasets\\dblp\\adj_list.csv')


