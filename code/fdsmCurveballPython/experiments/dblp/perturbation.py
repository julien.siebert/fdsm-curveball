import cProfile

from utilities.plot.plot_perturbation_scores import perturbation_scores_plot_0, perturbation_scores_plot, perturbation_scores_plot_3d, compare_perturbation_scores_plot_0


def main():
    perturbation_scores_plot_0('D:\\WorkSpace\\Datasets\\dblp\\adj_list.csv', 'D:\\WorkSpace\\Datasets\\dblp\\step_seed0_step{}.csv')
    perturbation_scores_plot('D:\\WorkSpace\\Datasets\\dblp\\adj_list.csv', 'D:\\WorkSpace\\Datasets\\dblp\\step_seed0_step{}.csv')
    # perturbation_scores_plot_3d('D:\\WorkSpace\\Datasets\\dblp\\adj_list.csv', 'D:\\WorkSpace\\Datasets\\dblp\\step_seed0_step{}.csv')
    #
    perturbation_scores_plot_0('D:\\WorkSpace\\Datasets\\dblp\\adj_list.csv', 'D:\\WorkSpace\\Datasets\\dblp\\run_size10_seed0_step{}.csv')
    perturbation_scores_plot('D:\\WorkSpace\\Datasets\\dblp\\adj_list.csv', 'D:\\WorkSpace\\Datasets\\dblp\\run_size10_seed0_step{}.csv')
    # perturbation_scores_plot_3d('D:\\WorkSpace\\Datasets\\dblp\\adj_list。csv', 'D:\\WorkSpace\\Datasets\\dblp\\run_size10_seed0_step{}.csv')
    #
    compare_perturbation_scores_plot_0('D:\\WorkSpace\\Datasets\\dblp\\adj_list.csv', 'D:\\WorkSpace\\Datasets\\dblp\\step_seed{}_step{}.csv', seed=5)


if __name__ == '__main__':
    cProfile.run('main()')