import networkx as nx
from networkx.algorithms import bipartite
import matplotlib.pyplot as plt
import numpy as np
import random

from utilities.load_save_data import read_csv, read_csv_list, save_csv, save_csv_list
from curveball.classical.run import step
from metrics.perturbation_score import perturbation
from utilities.plot.plot_perturbation_scores import count_edges, perturbation_scores_plot_0, compare_perturbation_scores_plot_0


def create_gnp_bipartite_csv(n: int, m: int, p: complex, seed: int):
    """
    generate a bipartite Erdős-Rényi graph G(n,m,p) and save it as adjacency map to a file

    :param n: the number of nodes in the first bipartite set
    :param m: the number of nodes in the second bipartite set
    :param p: probability for edge creation
    :param seed: seed for random number generator
    :return: path
    """
    if n < m:
        n, m = m, n
    bigraph = bipartite.random_graph(n, m, p, seed=seed)
    # print(nx.edges(bigraph))
    # colors = ['r']*n + ['y']*m
    # nx.draw_networkx(bigraph, pos=nx.circular_layout(bigraph), node_color=colors)
    # plt.show()
    adj_map = nx.to_dict_of_lists(bigraph)
    for i in range(0, n):
        adj_map.pop(i)
    for i in nx.isolates(bigraph):
        if i >= n:
            adj_map.pop(i)
    filename = "D:\\WorkSpace\\Datasets\\gnp_model\\bipartite\\{}_{}_{}_{}_0.csv".format(n, m, p, seed)
    save_csv(adjacency_list=adj_map, filename=filename)
    return filename


def global_curveball_step(n, m, p, seed, nb_samples: int, nb_seeds: int):
    filename_0 = create_gnp_bipartite_csv(n, m, p, seed)
    path = "D:\\WorkSpace\\Datasets\\gnp_model\\bipartite"
    for s in range(nb_seeds):
        random.seed(s)
        adjacency_list = read_csv_list(filename_0)
        for i in range(nb_samples):
            adjacency_list = step(adjacency_list=adjacency_list)
            filename = path + '\\{}_{}_{}_{}_seed{}_step{}.csv'.format(n, m, p, seed, s, i+1)
            save_csv_list(adjacency_list=adjacency_list, filename=filename)
    filename_1 = path + '\\{}_{}_{}_{}_'.format(n, m, p, seed) + 'seed{}_step{}.csv'
    compare_perturbation_scores_plot_0(filename_0, filename_1, nb_samples, nb_seeds)


def perturbation_plot_p(n, m, p_list: list, nb_samples):
    """
    plots perturbation scores for G(n, m, p) with the same n, m but different p in one figure
    :param n:
    :param m:
    :param p_list: the list of p
    :param nb_samples:
    :return:
    """
    plt.figure(dpi=600)
    x = np.arange(1, nb_samples+1)
    for p in p_list:
        adjacency_map_0 = read_csv(create_gnp_bipartite_csv(n, m, p, 0))
        number_of_edges = count_edges(adjacency_map=adjacency_map_0)
        perturbation_scores = []
        for i in range(1, nb_samples+1):
            adjacency_map_1 = \
                read_csv(filename='D:\\WorkSpace\\Datasets\\gnp_model\\bipartite\\{}_{}_{}_0_seed0_step{}.csv'.format(n, m, p, i))
            perturbation_scores.append(perturbation(adjacency_map_0=adjacency_map_0, adjacency_map_1=adjacency_map_1)/number_of_edges)
        y = np.array(perturbation_scores)
        plt.plot(x, y, label=r'G({},{},{})'.format(n, m, p))
        plt.scatter(x, y, s=15, alpha=0.5)
    plt.xlabel('Number of steps')
    plt.ylabel('Perturbation scores')
    plt.ylim((0.0, 1.0))
    plt.legend(loc='lower right')
    plt.grid(True)
    plt.show()


if __name__ == '__main__':
    # fill == p
    global_curveball_step(1000, 800, 0.1, 0, 30, 5)
    # global_curveball_step(1000, 800, 0.3, 1, 30, 5)
    # global_curveball_step(1000, 800, 0.5, 2, 30, 5)
    # global_curveball_step(1000, 800, 0.7, 3, 30, 5)
    perturbation_plot_p(1000, 800, [0.1, 0.3, 0.5, 0.7], 30)