import random
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np

from utilities.load_save_data import save_csv, save_csv_list, read_csv, read_csv_list
from curveball.directed.run import step
from metrics.perturbation_score import perturbation
from utilities.plot.plot_perturbation_scores import count_edges, compare_perturbation_scores_plot_0


def create_gnp_directed_csv(n: int, p: complex, seed):
    """
    Generate a directed Erdős-Rényi graph G(n,p) and save it as adjacency map to a file

    :param n: the number of nodes
    :param p: probability for edge creation
    :param seed: seed for random number generator
    :return:
    """
    digraph = nx.fast_gnp_random_graph(n, p, seed=seed, directed=True)
    # nx.draw_networkx(digraph, pos=nx.circular_layout(digraph))
    # plt.show()
    adj_map = nx.to_dict_of_lists(digraph)
    for node in list(adj_map):
        if not adj_map[node]:
            adj_map.pop(node)
    filename = "D:\\WorkSpace\\Datasets\\gnp_model\\directed\\{}_{}_{}_0.csv".format(n, p, seed)
    save_csv(adjacency_list=adj_map, filename=filename)
    return filename


def global_curveball_step(n, p, seed, nb_samples: int, nb_seeds: int):
    filename_0 = create_gnp_directed_csv(n, p, seed)
    path = "D:\\WorkSpace\\Datasets\\gnp_model\\directed"
    for s in range(nb_seeds):
        random.seed(s)
        adjacency_list = read_csv_list(filename_0)
        for i in range(nb_samples):
            adjacency_list = step(adjacency_list=adjacency_list)
            filename = path + '\\{}_{}_{}_seed{}_step{}.csv'.format(n, p, seed, s, i+1)
            save_csv_list(adjacency_list=adjacency_list, filename=filename)
    filename_1 = path + '\\{}_{}_{}_'.format(n, p, seed) + 'seed{}_step{}.csv'
    compare_perturbation_scores_plot_0(filename_0, filename_1, nb_samples, nb_seeds)


def perturbation_plot_p(n, p_list: list, nb_samples):
    """
    plots perturbation scores for G(n, p) with the same n but different p in one figure
    :param n:
    :param m:
    :param p_list: the list of p
    :param nb_samples:
    :return:
    """
    plt.figure(dpi=600)
    x = np.arange(1, nb_samples+1)
    for p in p_list:
        adjacency_map_0 = read_csv(create_gnp_directed_csv(n, p, 0))
        number_of_edges = count_edges(adjacency_map=adjacency_map_0)
        perturbation_scores = []
        for i in range(1, nb_samples+1):
            adjacency_map_1 = \
                read_csv(filename='D:\\WorkSpace\\Datasets\\gnp_model\\directed\\{}_{}_0_seed0_step{}.csv'.format(n, p, i))
            perturbation_scores.append(perturbation(adjacency_map_0=adjacency_map_0, adjacency_map_1=adjacency_map_1)/number_of_edges)
        y = np.array(perturbation_scores)
        plt.plot(x, y, label=r'G({},{})'.format(n, p))
        plt.scatter(x, y, s=15, alpha=0.5)
    plt.xlabel('Number of steps')
    plt.ylabel('Perturbation scores')
    plt.ylim((0.0, 1.0))
    plt.legend(loc='lower right')
    plt.grid(True)
    plt.show()


if __name__ == '__main__':
    # fill == p
    # global_curveball_step(1000, 0.1, 0, 30, 5)
    # global_curveball_step(1000, 0.3, 0, 30, 1)
    # global_curveball_step(1000, 0.5, 0, 30, 1)
    # global_curveball_step(1000, 0.7, 0, 30, 1)
    perturbation_plot_p(1000, [0.1, 0.3, 0.5, 0.7], 30)









