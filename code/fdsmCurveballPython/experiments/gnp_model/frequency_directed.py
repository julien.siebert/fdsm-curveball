import random
from typing import List, Tuple
from collections import defaultdict
import numpy as np
from matplotlib import pyplot as plt

from curveball.directed.run import run
from tests.utils import to_unique_string


def curveball_run(adjacency_list: List[Tuple[int, set]], nb_samples: int, seed: int):
    random.seed(seed)
    adjacency_lists = defaultdict(int)
    adjacency_lists[to_unique_string(adjacency_list)] += 1
    for i in range(nb_samples):
        adjacency_list = run(adjacency_list=adjacency_list, nb_steps=10)
        adjacency_lists[to_unique_string(adjacency_list=adjacency_list)] += 1
    return adjacency_lists


def compute_frequency(nb_phases: int, nb_samples: int):
    # the number of unique random graphs is about 350
    adjacency_list = [
        (1, {2}),
        (2, {3, 5}),
        (3, {4}),
        (4, {6}),
        (5, {2}),
        (6, {1})
    ]
    # burning of phase
    for i in range(nb_phases):
        adjacency_list = run(adjacency_list=adjacency_list, nb_steps=10)   # seed?
    adjacency_lists = curveball_run(adjacency_list, nb_samples, 0)
    frequency = []
    for i in adjacency_lists.keys():
        frequency.append((adjacency_lists[i]/nb_samples) * len(adjacency_lists))
    # the expected frequency: (1,1,...,1)
    return nb_phases, nb_samples, frequency


def plot_frequency(nb_phases, nb_samples, frequency: List):
    x = np.arange(len(frequency))
    y = np.array(frequency)
    var = np.var(y)
    plt.figure(dpi=600)
    plt.bar(x, y)
    plt.xlabel('Index of unique random graphs')
    plt.ylabel('Frequency')
    plt.legend(labels=['nb_phases={}'.format(nb_phases)+'  stepsize=10\nnb_samples={}'.format(nb_samples)+'\nvar={}'.format(var)], loc='best')
    plt.grid(True)
    plt.show()


def plot_var_of_freq(nb_samples_list: List, nb_phases):
    x = np.arange(nb_phases)
    plt.figure(dpi=600)
    for nb_samples in nb_samples_list:
        variance = []
        for i in range(nb_phases):
            variance.append(np.var(compute_frequency(nb_phases=i, nb_samples=nb_samples)[2]))
        y = np.array(variance)
        plt.plot(x, y, label='nb_samples={}'.format(nb_samples))
    plt.xlabel('Burning of phase (stepsize=10)')
    plt.ylabel('Variance of frequency')
    plt.legend(loc='best')
    plt.grid(True)
    plt.show()


if __name__ == '__main__':
    # result = compute_frequency(0, 5000)
    # plot_frequency(result[0], result[1], result[2])
    nb_samples_list = [1000, 2000, 5000, 8000]
    plot_var_of_freq(nb_samples_list, 100)