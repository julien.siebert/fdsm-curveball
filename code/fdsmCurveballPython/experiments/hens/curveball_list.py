import cProfile
import random

from utilities.load_save_data import read_csv_list, save_csv_list
from curveball.directed.run import step, run


def step_list(nb_samples=100, seed=0):
    adj_list = read_csv_list(filename='D:\\WorkSpace\\Datasets\\hens\\directed_adj_list.csv')
    random.seed(seed)
    for i in range(1, nb_samples+1):
        adj_list = step(adjacency_list=adj_list)
        save_csv_list(adjacency_list=adj_list,
                      filename='D:\\WorkSpace\\Datasets\\hens\\step_seed{}_step{}.csv'.format(seed, i))


def run_list(nb_samples=100, nb_steps=10, seed=0):
    adj_list = read_csv_list(filename='D:\\WorkSpace\\Datasets\\hens\\directed_adj_list.csv')
    random.seed(seed)
    for i in range(1, nb_samples+1):
        adj_list = run(adjacency_list=adj_list, nb_steps=nb_steps)
        save_csv_list(adjacency_list=adj_list,
                      filename='D:\\WorkSpace\\Datasets\\hens\\run_size{}_seed{}_step{}.csv'.format(nb_steps, seed, i))


def main():
    step_list()
    for i in range(10):
        run_list(100, 10, i)


if __name__ == '__main__':
    cProfile.run('main()')



