from utilities.load_save_data import extract_adjacency_list, save_csv


if __name__ == '__main__':
    dataset = 'D:\\WorkSpace\\Datasets\\hens\\out.moreno_hens_hens'
    adjacency_list = extract_adjacency_list(dataset)
    save_csv(adjacency_list=adjacency_list, filename='D:\\WorkSpace\\Datasets\\hens\\directed_adj_list.csv')


