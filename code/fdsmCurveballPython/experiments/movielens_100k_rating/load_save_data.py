from collections import defaultdict
from utilities.load_save_data import save_csv


def extract_adjacency_list(edge_list_filename, sep=' '):
    adj_list = defaultdict(set)
    with open(edge_list_filename, 'r') as f:
        for line in f:
            if line[0] == '%':
                continue
            user, movie, weight, timestampe = line.strip().split(sep)
            adj_list[int(user)].add(int(movie))
    return adj_list


if __name__ == '__main__':
    dataset = 'D:\\WorkSpace\\Datasets\\movielens_100k_rating\\rel.rating'
    adjacency_list = extract_adjacency_list(edge_list_filename=dataset)
    save_csv(adjacency_list=adjacency_list, filename='D:\\WorkSpace\\Datasets\\movielens_100k_rating\\adj_list.csv')
