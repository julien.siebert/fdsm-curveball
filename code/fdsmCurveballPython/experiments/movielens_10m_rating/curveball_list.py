import cProfile
import random

from utilities.load_save_data import read_csv_list, save_csv_list
from curveball.classical.run import step, run
import curveball.classical.run_subprocess as subpro


def step_list(nb_samples: int, seed=0):
    adj_list = read_csv_list(filename='D:\\WorkSpace\\Datasets\\movielens_10m_rating\\adj_list_right.csv')
    random.seed(seed)
    for i in range(1, nb_samples+1):
        adj_list = step(adjacency_list=adj_list)
        save_csv_list(adjacency_list=adj_list,
                      filename='D:\\WorkSpace\\Datasets\\movielens_10m_rating\\step_seed{}_step{}.csv'.format(seed, i))


def run_list(nb_samples: int, nb_steps=10, seed=0):
    adj_list = read_csv_list(filename='D:\\WorkSpace\\Datasets\\movielens_10m_rating\\adj_list_right.csv')
    random.seed(seed)
    for i in range(1, nb_samples+1):
        adj_list = run(adjacency_list=adj_list, nb_steps=nb_steps)
        save_csv_list(adjacency_list=adj_list,
                      filename='D:\\WorkSpace\\Datasets\\movielens_10m_rating\\run_size{}_seed{}_step{}.csv'.format(nb_steps, seed, i))


def parallel_run(nb_samples: int, seed=0):
    adj_list = read_csv_list(filename='D:\\WorkSpace\\Datasets\\movielens_10m_rating\\adj_list_right.csv')
    random.seed(seed)
    for i in range(1, nb_samples+1):
        adj_list = subpro.run(adjacency_list=adj_list, nb_steps=10, nb_workers=4)
        save_csv_list(adjacency_list=adj_list,
                      filename='D:\\WorkSpace\\Datasets\\movielens_10m_rating\\subprocess_run_seed{}_step{}.csv'.format(seed, i))


def main():
    for i in range(10):
        step_list(100, seed=i)
    # run_list(100)
    # parallel_run(100)


if __name__ == '__main__':
    cProfile.run('main()')



