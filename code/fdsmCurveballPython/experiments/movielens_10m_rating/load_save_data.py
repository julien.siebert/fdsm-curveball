from collections import defaultdict
from utilities.load_save_data import save_csv


def extract_adjacency_list_right(edge_list_filename, sep=' '):
    adj_list_right = defaultdict(set)
    with open(edge_list_filename, 'r') as f:
        for line in f:
            if line[0] == '%':
                continue
            neighbor, node, weight, timestampe = line.strip().split(sep)
            adj_list_right[int(node)].add(int(neighbor))
    return adj_list_right


if __name__ == '__main__':
    dataset = 'D:\\WorkSpace\\Datasets\\movielens_10m_rating\\out.movielens-10m_rating'
    adjacency_list_right = extract_adjacency_list_right(dataset)
    save_csv(adjacency_list=adjacency_list_right,
             filename='D:\\WorkSpace\\Datasets\\movielens_10m_rating\\adj_list_right.csv')



