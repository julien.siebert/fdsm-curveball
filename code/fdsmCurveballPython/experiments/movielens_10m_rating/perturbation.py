import cProfile
from utilities.plot.plot_perturbation_scores import perturbation_scores_plot_0, perturbation_scores_plot, perturbation_scores_plot_3d, compare_perturbation_scores_plot_0


def main():
    # perturbation_scores_plot_0('D:\\WorkSpace\\Datasets\\movielens_10m_rating\\adj_list_right.csv', 'D:\\WorkSpace\\Datasets\\movielens_10m_rating\\step_seed0_step{}.csv')
    perturbation_scores_plot('D:\\WorkSpace\\Datasets\\movielens_10m_rating\\adj_list_right.csv', 'D:\\WorkSpace\\Datasets\\movielens_10m_rating\\step_seed0_step{}.csv')
    # perturbation_scores_plot_3d('D:\\WorkSpace\\Datasets\\movielens_10m_rating\\adj_list_right.csv', 'D:\\WorkSpace\\Datasets\\movielens_10m_rating\\step_seed{}_step{}.csv')

    # perturbation_scores_plot_0('D:\\WorkSpace\\Datasets\\movielens_10m_rating\\adj_list_right.csv', 'D:\\WorkSpace\\Datasets\\movielens_10m_rating\\run_size10_seed0_step{}.csv')
    perturbation_scores_plot('D:\\WorkSpace\\Datasets\\movielens_10m_rating\\adj_list_right.csv', 'D:\\WorkSpace\\Datasets\\movielens_10m_rating\\run_size10_seed0_step{}.csv')
    # perturbation_scores_plot_3d('D:\\WorkSpace\\Datasets\\movielens_10m_rating\\adj_list_right.csv', 'D:\\WorkSpace\\Datasets\\movielens_10m_rating\\run_size10_seed0_step{}.csv')

    # compare_perturbation_scores_plot_0('D:\\WorkSpace\\Datasets\\movielens_10m_rating\\adj_list_right.csv', 'D:\\WorkSpace\\Datasets\\movielens_10m_rating\\step_seed{}_step{}.csv', seed=10)


if __name__ == '__main__':
    cProfile.run('main()')