from typing import Dict


def perturbation(adjacency_map_0: Dict[int, set], adjacency_map_1: Dict[int, set]):
    """
    compute number of different edges between two graphs G0 and G1,
    using Adjacency List: Dict[int, set]
    """
    perturbation_score = 0
    for i in adjacency_map_0.keys():
        perturbation_score += len(adjacency_map_0[i] - adjacency_map_1[i])
    return perturbation_score


def perturbation_normalized(adjacency_map_0: Dict[int, set], adjacency_map_1: Dict[int, set]):
    """
    compute the perturbation score between two graphs G0 and G1,
    using Adjacency List: Dict[int, set]
    """
    perturbation_score = 0
    number_of_edges = 0
    for i in adjacency_map_0.keys():
        perturbation_score += len(adjacency_map_0[i] - adjacency_map_1[i])
        number_of_edges += len(adjacency_map_0[i])
    return perturbation_score / number_of_edges

