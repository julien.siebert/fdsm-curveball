import unittest

from metrics.perturbation_score import perturbation, perturbation_normalized


class TestMemoryUsage(unittest.TestCase):
    def test_perturbation_zero(self):
        """
        Perturbation should return 0 when both adjacency matrices are identical
        """
        adjacency_map_0 = {1: {2, 3, 4}, 2: {5, 6, 7}}
        adjacency_map_1 = {1: {2, 3, 4}, 2: {5, 6, 7}}
        self.assertEqual(0, perturbation(adjacency_map_0=adjacency_map_0, adjacency_map_1=adjacency_map_1))

    def test_perturbation_normalized_zero(self):
        """
        Perturbation should return 0 when both adjacency matrices are identical
        """
        adjacency_map_0 = {1: {2, 3, 4}, 2: {5, 6, 7}}
        adjacency_map_1 = {1: {2, 3, 4}, 2: {5, 6, 7}}
        self.assertEqual(0, perturbation_normalized(adjacency_map_0=adjacency_map_0, adjacency_map_1=adjacency_map_1))

    def test_perturbation_max(self):
        """
        Perturbation should return the total number of edges when both adjacency matrices are totally different
        """
        adjacency_map_0 = {1: {2, 3, 4}, 2: {5, 6, 7}}
        adjacency_map_1 = {1: {5, 6, 7}, 2: {2, 3, 4}}
        self.assertEqual(6, perturbation(adjacency_map_0=adjacency_map_0, adjacency_map_1=adjacency_map_1))

    def test_perturbation_normalized_max(self):
        """
        Perturbation should return 1 when both adjacency matrices are totally different
        """
        adjacency_map_0 = {1: {2, 3, 4}, 2: {5, 6, 7}}
        adjacency_map_1 = {1: {5, 6, 7}, 2: {2, 3, 4}}
        self.assertEqual(1, perturbation_normalized(adjacency_map_0=adjacency_map_0, adjacency_map_1=adjacency_map_1))

    def test_perturbation_single(self):
        """
        Perturbation should return 1 when both adjacency matrices only differ by a single edge
        """
        adjacency_map_0 = {1: {2, 3, 4}, 2: {5, 6, 7}}
        adjacency_map_1 = {1: {2, 3, 7}, 2: {5, 6, 7}}
        self.assertEqual(1, perturbation(adjacency_map_0=adjacency_map_0, adjacency_map_1=adjacency_map_1))

    def test_perturbation_normalized_single(self):
        """
        Perturbation should return 1/nb_edges when both adjacency matrices only differ by a single edge
        """
        adjacency_map_0 = {1: {2, 3, 4}, 2: {5, 6, 7}}
        adjacency_map_1 = {1: {2, 3, 7}, 2: {5, 6, 7}}
        self.assertAlmostEqual(1./6., perturbation_normalized(adjacency_map_0=adjacency_map_0, adjacency_map_1=adjacency_map_1))

    def test_perturbation(self):
        """
        Perturbation should return 2 when both adjacency matrices differ by two edges
        """
        adjacency_map_0 = {1: {2, 3, 4}, 2: {5, 6, 7}}
        adjacency_map_1 = {1: {2, 3, 7}, 2: {5, 4, 7}}
        self.assertEqual(2, perturbation(adjacency_map_0=adjacency_map_0, adjacency_map_1=adjacency_map_1))

    def test_perturbation_normalized(self):
        """
        Perturbation should return 2/nb_edges when both adjacency matrices differ by two edges
        """
        adjacency_map_0 = {1: {2, 3, 4}, 2: {5, 6, 7}}
        adjacency_map_1 = {1: {2, 3, 7}, 2: {5, 4, 7}}
        self.assertAlmostEqual(2./6., perturbation_normalized(adjacency_map_0=adjacency_map_0, adjacency_map_1=adjacency_map_1))


if __name__ == '__main__':
    unittest.main()
