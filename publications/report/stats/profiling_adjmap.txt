mprof run experiments/benchmark_curveball/directed/profile_run.py --steps=10 --dataset=amazon_250000.txt
mprof: Sampling memory every 0.1s
Filename: /home/julien/Documents/SPPII/curveball/code/python/utilities/load_save_data.py

Line #    Mem usage    Increment   Line Contents
================================================
   101     25.0 MiB      0.0 MiB   @profile
   102                             def read_csv(filename):
   103                                 """
   104                                 Read an adjacency map from a file with the following format
   105
   106                                 node1:neighbor1,neighbor2,neighbor3...
   107                                 node2:neighbor1,neighbor2,neighbor3...
   108
   109                                 :param filename:
   110                                 :return: adjacency_list
   111                                 """
   112     25.0 MiB      0.0 MiB       adjacency_list = {}
   113     25.0 MiB      0.0 MiB       with open(filename, 'r') as f:
   114     80.4 MiB     55.4 MiB           for line in f:
   115     80.4 MiB      0.0 MiB               node, neighbors = line.strip().split(':')
   116     80.4 MiB      0.0 MiB               adjacency_list[int(node)] = set([int(x) for x in neighbors.split(',')])
   117     80.4 MiB      0.0 MiB       return adjacency_list



Filename: /home/julien/Documents/SPPII/curveball/code/python/curveball/directed/run.py

Line #    Mem usage    Increment   Line Contents
================================================
    35     80.4 MiB      0.0 MiB   @profile
    36                             def run(adjacency_list: dict, nb_steps: int) -> dict:
    37                                 """
    38                                 Runs a certain number of steps
    39                                 :param adjacency_list: the starting adjacency list
    40                                 :param nb_steps: the number of steps to run
    41                                 :return: a new adjacency list
    42                                 """
    43     80.5 MiB      0.1 MiB       for _ in range(nb_steps):
    44     80.5 MiB      0.0 MiB           adjacency_list = step(adjacency_list)
    45     80.5 MiB      0.0 MiB       return adjacency_list
